#include <df/log.h>
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h>


int main(int ac, char *av[])
{
    (void)ac;
    printf("start\n");
    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    printf("with NL:\n");
    dflog(LOG_INFO, "111\n");
    dflog(LOG_INFO, "222\n");
    dflog(LOG_INFO, "333\n");
    printf("without NL:\n");
    dflog(LOG_INFO, "111");
    dflog(LOG_INFO, "222");
    dflog(LOG_INFO, "333");
    printf("multiline:\n");
    dflog(LOG_INFO, "111\n222\n333");

    printf("append multiline:\n");
    dflog_("111\n");
    dflog_("222\n");
    dflog_("333");
    printf("flush:\n");
    dflog_flush(LOG_INFO);

    printf("append multiline untile close:\n");
    dflog_("111\n");
    dflog_("222\n");
    dflog_("333");
    printf("close:\n");

    dflog_close();
    printf("end\n");
    return EXIT_SUCCESS;
}
