#include <df/log.h>
#include <stdlib.h> // EXIT_SUCCESS


int main(int ac, char *av[])
{
    (void)ac;
    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    dflog(LOG_INFO, "ident av[0] with basename: %s", dflog_ident());

    dflog_close();
    return EXIT_SUCCESS;
}
