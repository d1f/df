#include <df/log.h>
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h>


int main(int ac, char *av[])
{
    (void)ac;
    (void)av;
    dflog_open(NULL, DFLOG_STDOUT | DFLOG_BASENAME);

    dflog(LOG_INFO, "ident av[0] with basename: %s", dflog_ident());
    printf("empty start:\n");
    dflog(LOG_INFO, " ");
    printf("empty end\n");

    printf("NL start:\n");
    dflog(LOG_INFO, "\n");
    printf("NL end\n");

    //dflog_close();
    return EXIT_SUCCESS;
}
