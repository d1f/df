# all df-*

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


ifneq ($(filter build bld install inst,$(MAKECMDGOALS)),)
xlist = df-msg% df-pthread-hooks-test
endif

NESTED_PKGES = $(filter df-% df,$(filter-out $(xlist) all% %all,$(ALL_PKGES)))


include $(RULES)
