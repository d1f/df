#include <df/log.h>
#include <df/x.h>   // df_freep
#include <stdio.h>  // fflush
#include <stddef.h> // NULL
#include <stdlib.h> // free
#include <string.h> // strdup


static int __flags = DFLOG_STDERR;

int dflog_flags(void)
{
    return __flags;
}


extern char *__progname; // glibc global variable
static char *__ident = NULL;

const char *dflog_ident(void)
{
    return __ident;
}

// set ident.
// return new strdup'ed ident.
// free old ident and use __progname if NULL supplied.
// no thread-safe.
static const char *dflog_set_ident(const char *ident)
{
    if (__ident != NULL && __ident != __progname)
	df_freep(&__ident);

    if (ident != NULL)
	__ident = strdup((dflog_flags() & DFLOG_BASENAME)
			 ? df_basename(ident)
			 :             ident);
    else
	__ident = __progname;

    return __ident;
}


// no thread-safe
void dflog_open(const char *_ident, int flags)
{
    dflog_close();

    __flags = flags;

    const char *ident = dflog_set_ident(_ident);

    if (flags & DFLOG_SYS)
	openlog(ident, flags & DFLOG_PID ? LOG_PID : 0, LOG_USER);

    if (flags & DFLOG_STDERR)
    {
	fflush(stderr);
	setlinebuf(stderr);
    }
    if (flags & DFLOG_STDOUT)
    {
	fflush(stdout);
	setlinebuf(stdout);
    }
}

void dflog_close(void)
{
    dflog_flush(LOG_ERR);

    if (__flags & DFLOG_SYS)
	closelog();
    if (__flags & DFLOG_STDERR)
	fflush(stderr);
    if (__flags & DFLOG_STDOUT)
	fflush(stdout);

    dflog_set_ident(NULL); // free ident
    dflog_free_buf();
}
