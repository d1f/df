#include <df/locale-date-names.h>


const char* df_locale_date_names[DF_LOCALE_LANG_NUM][DF_LOCALE_DATE_TYPE_NUM][12] =
{
    [DF_LOCALE_ENG] =
    {
	[DF_LOCALE_DATE_DAY_OF_WEEK_SHORT3] = {
	    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
	},

	[DF_LOCALE_DATE_DAY_OF_WEEK_SHORT2] = {
	    "Sn", "Mn", "Tu", "Wd", "Th", "Fr", "St"
	},

	[DF_LOCALE_DATE_DAY_OF_WEEK_LONG] = {
	    "Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"
	},

	[DF_LOCALE_DATE_MONTH_SHORT] = {
	    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
	},

	[DF_LOCALE_DATE_MONTH_LONG]  = {
	    "January", "February", "March"    , "April"  , "May"     , "June",
	    "July"   , "August"  , "September", "October", "November", "December"
	}
    },

    [DF_LOCALE_RUS] =
    {
	[DF_LOCALE_DATE_DAY_OF_WEEK_SHORT3] = {
	    "Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"
	},

	[DF_LOCALE_DATE_DAY_OF_WEEK_SHORT2] = {
	    "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"
	},

	[DF_LOCALE_DATE_DAY_OF_WEEK_LONG] = {
	    "Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"
	},

	[DF_LOCALE_DATE_MONTH_SHORT] = {
	    "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"
	},

	[DF_LOCALE_DATE_MONTH_LONG]  = {
	    "Январь" , "Февраль", "Март"    , "Апрель" , "Май"   , "Июнь",
	    "Июль"   , "Август" , "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
	}
    }
};
