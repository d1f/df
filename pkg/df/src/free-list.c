#include <int/free-list.h>
#include <df/x.h>
#include <df/bt.h>


void df_free_list_add(df_free_list_t **flp, void *mem)
{
    DF_BT;

    if (*flp != NULL)
	DF_CHECK_SIGNATURE(df_free_list, *flp);

    df_free_list_t *flnew = df_xmalloc(sizeof(df_free_list_t));
    flnew->signature = df_free_list_SIGNATURE;
    flnew->mem  = mem;
    flnew->next = *flp;
    *flp = flnew;
}

void df_free_list_free(df_free_list_t **flp)
{
    DF_BT;

    for (df_free_list_t *fl = *flp, *flnext=NULL; fl != NULL; fl = flnext)
    {
	DF_CHECK_SIGNATURE(df_free_list, fl);
	flnext = fl->next;
	free(fl->mem);
	free(fl);
    }

    *flp = NULL;
}
