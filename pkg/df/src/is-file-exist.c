#include <df/file-exist.h>
#include <df/error.h>
#include <df/log.h>
#include <df/bt.h>
#include <sys/stat.h>
#include <string.h> // strlen


// returns: >0: exist, 0: not exist, <0: error
// outputs messages via dferror(), no exits
int df_is_file_exist(const char *path)
{
    DF_BT;

    if (strlen(path) == 0)
    {
	DFLOGE("empty string in 'path' supplied");
	return -1;
    }

    struct stat statbuf;
    int rc = stat(path, &statbuf);
    if (rc < 0)
    {
	if (errno == ENOENT)
	    return 0; // path not exist

	dferror(EXIT_SUCCESS, errno, "%s(): stat(\"%s\") failed", __func__, path);
	return -1;
    }

    return 1; // path exist
}
