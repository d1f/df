#include <df/hex.h>
#include <df/bt.h>
#include <df/types.h>
#include <df/log.h>
#include <ctype.h>

static void df_char2hex(char str[2], char c)
{
    DF_BT;

    static const char hex_encode[16] =
    {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    str[0] = hex_encode[(c >> 4) & 0x0F];
    str[1] = hex_encode[ c       & 0x0F];
}

// convert single hex digit to number w/o checks
static char df_1hex2char(char hex_digit)
{
    DF_BT;

    static const char hex_decode[256] =
    {
	['0'] =  0,
	['1'] =  1,
	['2'] =  2,
	['3'] =  3,
	['4'] =  4,
	['5'] =  5,
	['6'] =  6,
	['7'] =  7,
	['8'] =  8,
	['9'] =  9,
	['a'] = 10,
	['b'] = 11,
	['c'] = 12,
	['d'] = 13,
	['e'] = 14,
	['f'] = 15,
	['A'] = 10,
	['B'] = 11,
	['C'] = 12,
	['D'] = 13,
	['E'] = 14,
	['F'] = 15,
    };

    return hex_decode[(uchar)hex_digit];
}

void df_bin2hex(char *dst, const void *src, size_t srclen)
{
    DF_BT;

    for (const char *s = src; srclen--; dst+=2, ++s)
	df_char2hex(dst, *s);
    *dst = '\0';
}

void df_str2hex(char *dst, const char *src)
{
    DF_BT;

    for ( ; *src != '\0'; dst+=2, ++src)
	df_char2hex(      dst,     *src);
    *dst = '\0';
}

// returns error flag
bool df_hex2bin(void *dst, size_t dstlen, const char *src)
{
    DF_BT;

    const char *src_orig = src;

    for (char c, *d = dst; dstlen-- && isxdigit(c = *src); ++src, ++d)
    {
	char hi = df_1hex2char(c);

	c = *++src;
	if (isxdigit(c))
	{
	    char lo = df_1hex2char(c);
	    *d = (hi << 4) | lo;
	}
	else
	{
	    DFLOGE("Odd number of HEX digits in %s", src_orig);
	    return true;
	}
    }

    if (dstlen > 0)
    {
	DFLOGE("Wrong number of HEX digits in %s", src_orig);
	return true;
    }

    return false;
}

// returns error flag
bool df_hex2str(char *dst, const char *src)
{
    DF_BT;

    const char *src_orig = src;

    for (char c; isxdigit(c = *src); ++src)
    {
	char hi = df_1hex2char(c);

	c = *++src;
	if (isxdigit(c))
	{
	    char lo = df_1hex2char(c);
	    *dst++ = (hi << 4) | lo;
	}
	else
	{
	    DFLOGE("Odd number of HEX digits in %s", src_orig);
	    return true;
	}
    }

    *dst = '\0';

    return false;
}
