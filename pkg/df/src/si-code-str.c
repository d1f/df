#define _GNU_SOURCE
#include <df/x.h>
#include <df/bt.h>
#include <sys/cdefs.h> // __STRING()

#define C(code) case code : return __STRING(code)

const char *df_si_code_str(int si_code, int signo)
{
    DF_BT;

    switch (signo)
    {
	case SIGILL:
	    switch (si_code)
	    {
		C(ILL_ILLOPC   );
		C(ILL_ILLOPN   );
		C(ILL_ILLADR   );
		C(ILL_ILLTRP   );
		C(ILL_PRVOPC   );
		C(ILL_PRVREG   );
		C(ILL_COPROC   );
		C(ILL_BADSTK   );
		default:
		    break;
	    }
	    break;
	case SIGFPE:
	    switch (si_code)
	    {
		C(FPE_INTDIV   );
		C(FPE_INTOVF   );
		C(FPE_FLTDIV   );
		C(FPE_FLTOVF   );
		C(FPE_FLTUND   );
		C(FPE_FLTRES   );
		C(FPE_FLTINV   );
		C(FPE_FLTSUB   );
		default:
		    break;
	    }
	case SIGSEGV:
	    switch (si_code)
	    {
		C(SEGV_MAPERR  );
		C(SEGV_ACCERR  );
		default:
		    break;
	    }
	case SIGBUS:
	    switch (si_code)
	    {
		C(BUS_ADRALN   );
		C(BUS_ADRERR   );
		C(BUS_OBJERR   );
#ifdef BUS_MCEERR_AR
		C(BUS_MCEERR_AR);
#endif
#ifdef BUS_MCEERR_AO
		C(BUS_MCEERR_AO);
#endif
		default:
		    break;
	    }
	case SIGTRAP:
	    switch (si_code)
	    {
#ifdef TRAP_BRKPT
		C(TRAP_BRKPT   );
#endif
#ifdef TRAP_TRACE
		C(TRAP_TRACE   );
#endif
#ifdef TRAP_BRANCH
		C(TRAP_BRANCH  );
#endif
#ifdef TRAP_HWBKPT
		C(TRAP_HWBKPT  );
#endif
		default:
		    break;
	    }
	case SIGCHLD:
	    switch (si_code)
	    {
		C(CLD_EXITED   );
		C(CLD_KILLED   );
		C(CLD_DUMPED   );
		C(CLD_TRAPPED  );
		C(CLD_STOPPED  );
#ifdef CLD_CONTINUE
		C(CLD_CONTINUE );
#endif
		default:
		    break;
	    }
	case SIGPOLL: // case SIGIO:
	    switch (si_code)
	    {
		C(POLL_IN      );
		C(POLL_OUT     );
		C(POLL_MSG     );
		C(POLL_ERR     );
		C(POLL_PRI     );
		C(POLL_HUP     );
		default:
		    break;
	    }
	default:
	    break;
    }

    switch (si_code)
    {
	C(SI_USER      );
	C(SI_KERNEL    );
	C(SI_QUEUE     );
	C(SI_TIMER     );
	C(SI_MESGQ     );
	C(SI_ASYNCIO   );
	C(SI_SIGIO     );
#ifdef SI_TKILL
	C(SI_TKILL     );
#endif

	default:
	    break;
    }

    return "Unknown";
}
