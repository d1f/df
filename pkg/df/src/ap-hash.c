#include <df/ap-hash.h>
#include <df/bt.h>
#include <df-signatures.h>

#define DF_AP_HASH_INIT_VALUE ((df_ap_hash_t) 0xAAAAAAAA)

// (C) Arash Partow http://www.partow.net/programming/hashfunctions/
#define df_ap_hash_add_char(hash, i, c)         \
    ({ hash ^= ((i & 1) == 0)                   \
        ? (  (hash <<  7) ^  c * (hash >> 3))   \
        : (~((hash << 11) + (c ^ (hash >> 5)))); hash; })

void df_ap_hash_init(void *out)
{
    static const df_ap_hash_t init = DF_AP_HASH_INIT_VALUE;
    memcpy(out, &init, sizeof(init));
}

void df_ap_hash(void *out, const void *src, size_t len)
{
    DF_BT;

    df_ap_hash_t hash = DF_AP_HASH_INIT_VALUE;
    const char *str = src;

    for(size_t i = 0; i < len; ++str, ++i)
	hash = df_ap_hash_add_char(hash, i, *str);

    memcpy(out, &hash, sizeof(hash));
}

void df_ap_hash_str(void *out, const char *str)
{
    DF_BT;

    df_ap_hash_t hash = DF_AP_HASH_INIT_VALUE;

    for(size_t i = 0; *str != 0; ++str, ++i)
	hash = df_ap_hash_add_char(hash, i, *str);

    memcpy(out, &hash, sizeof(hash));
}

const df_hash_alg_t df_ap_hash_alg =
{
    .signature   = df_hash_alg_SIGNATURE,
    .bits        = 32,
    .buckets_max = 0,
    .hash_init   = df_ap_hash_init,
    .hash_len    = df_ap_hash,
    .hash_str    = df_ap_hash_str
};
