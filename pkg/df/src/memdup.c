#include <df/memdup.h>
#include <df/bt.h>
#include <stdlib.h> // malloc
#include <string.h> // memcpy


void *df_memdup(const void *src, size_t size)
{
    DF_BT;

    void *p = malloc(size);
    if (p != NULL)
	memcpy(p, src, size);

    return p;
}
