#include <df/basename.h>
#include <df/bt.h>
#include <string.h> // strrchr


const char* df_basename(const char *filename)
{
    DF_BT;

    const char *slash = strrchr(filename, '/');
    return (slash != NULL) ? slash+1 : filename;
}
