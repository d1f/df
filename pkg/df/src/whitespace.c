/*
	<df/whitespace.c>

	(C) 1999-2015 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#include <df/whitespace.h>
#include <df/bt.h>
#include <string.h> // memmove strchr strlen
#include <ctype.h>  // isspace


void df_strip_lead_whitespace(char *str)
{
    DF_BT;

    char* cp = df_skip_whitespace(str);
    memmove(str, cp, strlen(cp) + 1);
}

void df_strip_tail_whitespace(char *str)
{
    DF_BT;

    char* cp = strchr(str, 0);
    if (cp == str) return;

    for (--cp; cp >= str && isspace(*cp); --cp)
	;
    if (cp >= str)
	* ++cp = '\0';
}

void df_strip_whitespace(char *str)
{
    DF_BT;

    df_strip_lead_whitespace(str);
    df_strip_tail_whitespace(str);
}

// returns pointer to first non-whitespace character.
const char* df_skip_whitespace_const(const char *str)
{
    DF_BT;

    while ( *str!='\0' && isspace(*str) )
	str++;

    return str;
}

// returns pointer to line's character after non-whitespace word.
const char* df_skip_word(const char *str)
{
    DF_BT;

    while ('\0' != *str && !isspace(*str))
	++str;

    return str;
}
