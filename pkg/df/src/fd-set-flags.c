#include <df/fd.h>
#include <df/error.h>
#include <df/bt.h>
#include <fcntl.h>

int df_fd_set_flags(int fd, long flags) // returns -1 on error with error message
{
    DF_BT;

    int err = fcntl(fd, F_SETFL, flags);
    if (err < 0)
	dferror(EXIT_SUCCESS, errno, "%s(): fcntl F_SETFL failed", __func__);

    return err;
}
