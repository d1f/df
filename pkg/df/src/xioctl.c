#define _GNU_SOURCE

#include <df/x.h>
#include <df/error.h>
#include <df/bt.h>

#include <errno.h>  // errno
#include <stdlib.h> // EXIT_FAILURE
#include <sys/ioctl.h>


static int xioctl_loop(int fd, int request, void *arg)
{
    DF_BT;

    int rc, err;

    do
    {
	rc = ioctl(fd, request, arg);
	err = errno;
    } while ( rc < 0 && (err == EINTR || err == EAGAIN || err == EWOULDBLOCK) );

    return rc;
}

void df_xioctl(const char *pfx, int fd, int request, void *arg)
{
    DF_BT;

    if (xioctl_loop(fd, request, arg) < 0)
	dferror(EXIT_FAILURE, errno, "%s", pfx);
}
