#include <df/x.h>
#include <stdlib.h> // free

void df_freep(void *vpp)
{
    if (vpp != NULL)
    {
	void **_vpp = vpp;
	void *vp = *_vpp;

	if (vp != NULL)
	{
	    free(vp);
	    *_vpp = NULL;
	}
    }
}
