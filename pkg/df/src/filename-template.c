#include <df/filename-template.h>
#include <df/date-format.h>
#include <df/bt.h>
#include <stdio.h>    // sprintf
#include <string.h>   // strncat
#include <time.h>     // localtime


void df_filename_template(char out[NAME_MAX],
			  const char *prefix,   // NULL allowed
			  const char *suffix,   // NULL allowed
			  const size_t *seqnum, // NULL allowed
			  char left_delim, char right_delim,
			  const char *template,
			  df_locale_lang_t lang,
			  const struct timeval *tv // calls gettimeofday() if NULL
		      )
{
    DF_BT;

    size_t out_limit = NAME_MAX;

    out[0] = 0;

    if (prefix != NULL)
    {
	strncpy(out, prefix, out_limit-1);
	out[out_limit-1] = 0;
	size_t prefix_len = strlen(out);
	out       += prefix_len;
	out_limit -= prefix_len;
    }

    if (seqnum != NULL)
    {
	int sn_len = sprintf(out, "%010zu_", *seqnum);
	if (sn_len < 0)
	    sn_len = 0;
	out       += sn_len;
	out_limit -= sn_len;
    }

    df_date_format(out, out_limit,
		   template, left_delim, right_delim, lang, tv);

    if (suffix != NULL)
	strncat(out, suffix, out_limit - strlen(out) - 1);
}
