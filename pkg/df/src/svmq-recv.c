#include <df/svmq.h>
#include <df/log.h>
#include <df/bt.h>

#include <sys/msg.h>
#include <unistd.h> // getpid
#include <string.h> // memcpy


// returns received message src on succes or -1 on error
long df_svmq_recv(int mqid, long dst, void *data, size_t *data_size)
{
    DF_BT;

    char mdata[sizeof(df_svmq_t) + *data_size];
    memset(mdata, 0, sizeof(mdata));
    df_svmq_t *msg = (void*) &mdata[0];
    msg->dst = dst;

    ssize_t rc = msgrcv(mqid, msg, DF_SVMQ_MSG_SIZE(*data_size), dst, 0);
    if (rc < 0)
    {
	DFLOGE("msgrcv(dst: %ld) failed", dst);
	return -1;
    }
#if 0
    else
    {
	if (rc != SVMQ_MSG_SIZE)
	{
	    DFLOGE("%d bytes received instead of %d, ignore",
		   rc, DF_SVMQ_MSG_SIZE(*data_size));
	    return -1;
	}
    }
#endif

    ssize_t rsize = rc - sizeof(long);
    if (rsize < 0)
    {
	DFLOGE("only %zd bytes received, ignore", rc);
	return -1;
    }

    *data_size = rsize;

    memcpy(data, &msg->data[0], rsize);

    return msg->src;
}
