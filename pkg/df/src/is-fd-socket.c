#include <df/fd.h>    // df_is_fd_socket
#include <df/error.h> // dferror
#include <df/bt.h>
#include <unistd.h>   // fstat
#include <sys/stat.h> // struct stat

bool df_is_fd_socket(int fd)
{
    DF_BT;

    struct stat sbuf;
    int rc = fstat(fd, &sbuf);
    if (rc < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): fstat(%d) failed", __func__, fd);
	return false;
    }

    return  !!(sbuf.st_mode & S_IFSOCK);
}
