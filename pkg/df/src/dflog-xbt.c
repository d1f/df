#include <df/log.h>
#include <df/bt.h>


// out mesage with "file:line func(): " prefix via dflog(LOG_ERR) and calls df_bt_exit(EXIT_FAILURE)
void vdflog_xbt(const df_flf_t *flf, const char *format, va_list ap)
{
    dflog_("%s:%zu %s(): ", flf->file, flf->line, flf->func);
    vdflog_(format, ap);
    dflog_flush(LOG_ERR);
    df_bt_exit(EXIT_FAILURE);
}

// out mesage with "file:line func(): " prefix via dflog(LOG_ERR) and calls df_bt_exit(EXIT_FAILURE)
void dflog_xbt(const df_flf_t *flf, const char *format, ...)
{
    va_list ap;

    va_start(ap, format);
    vdflog_xbt(flf, format, ap);
    va_end  (ap);
}
