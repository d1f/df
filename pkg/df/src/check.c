#include <df/check.h>
#include <df/log.h>
#include <df/bt.h>
#include <stdlib.h> // EXIT_FAILURE


// out message via dflog_xbt() if ptr == NULL
void df_check_null(const df_flf_t *flf,
		   const char *what, const void * volatile ptr)
{
    DF_BT;

    if (ptr == NULL)
	dflog_xbt(flf, "%s is NULL", what);
}

// out message via dflog_xbt() if signature mismatched
void df_check_signature(const df_flf_t *flf,
			const char *struct_name,
			df_signature_t signature_value, df_signature_t signature_const)
{
    DF_BT;

    if (signature_value != signature_const)
	dflog_xbt(flf, "struct %s signature mismatched", struct_name);
}

// decrement unsigned counter if > 0 or dflog_xbt().
// returns decremented value.
size_t df_dec(df_flf_t const *flf, const char *what, size_t *count_p)
{
    DF_BT;

    if (*count_p > 0)
	--(*count_p);
    else
	dflog_xbt(flf, "decrement of %s == 0", what);

    return *count_p;
}

