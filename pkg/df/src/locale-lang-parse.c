#include <df/locale-lang.h>
#include <df/log.h>
#include <df/bt.h>
#include <string.h> //strcmp


// accepts lang string: ru[s]|en[g]
// out to *lang
// returns error flag, error message out via dferror()
bool df_locale_lang_parse(df_locale_lang_t *lang, const char *lang_str)
{
    DF_BT;

    if (strcmp(lang_str, "en") == 0 || strcmp(lang_str, "eng") == 0)
	*lang = DF_LOCALE_ENG;
    else if (strcmp(lang_str, "ru") == 0 || strcmp(lang_str, "rus") == 0)
	*lang = DF_LOCALE_RUS;
    else
    {
	*lang = DF_LOCALE_ENG;
	DFLOGEBT("Unknown lang: %s", lang_str);
	return true;
    }

    return false;
}
