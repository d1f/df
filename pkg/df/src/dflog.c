#define _GNU_SOURCE /* for stdio.h dprintf() */
#include <df/log.h>
#include "dflog-buf.h"
#include <errno.h>
#include <stdio.h>  // vsnprintf
#include <string.h> // strrchr
#include <unistd.h> // getpid
#include <sys/types.h> // ssize_t


static void dflog_out(int outfd, const char *str)
{
	int flags = dflog_flags();
	const char *ident = dflog_ident();

	size_t count = 0;

	if (ident != NULL && ident[0] != '\0')
	{
	    dprintf(outfd, "%s", ident);
	    ++count;
	}

	if (flags & DFLOG_PID)
	{
	    dprintf(outfd, "[%d]", getpid());
	    ++count;
	}

	if (count > 0)
	    dprintf(outfd, ": ");

	dprintf(outfd, "%s\n", str);
}

static void dflog_flush_(int level, const char *str)
{
    int flags = dflog_flags();

    if (flags & DFLOG_SYS)
	syslog(level, str);
    if (flags & DFLOG_STDERR)
	dflog_out(STDERR_FILENO, str);
    if (flags & DFLOG_STDOUT)
	dflog_out(STDOUT_FILENO, str);
}

// output log buffer
void dflog_flush(int level)
{
    dflog_buf_t *b = dflog_buf_get();

    if (dflog_buf_check(b))
	return;

    char *buf = dflog_buf_buf(b);
    if (buf[0] == '\0') // nothing to out
	return;

    for (char *bp = buf; ; )
    {
	char *np = strchr(bp, '\n');
	if (np != NULL)
	{
	    *np = '\0';
	    dflog_flush_(level, bp);
	    bp = np + 1;
	}
	else
	{
	    if (bp[0] != '\0')
		dflog_flush_(level, bp);
	    break;
	}
    }

    buf[0] = '\0';
    b->ptr = buf;
}


// return negative required space if truncated
// or number of chars w/o nul termination
static ssize_t dflog_buf_vprintf(dflog_buf_t *b, const char *format, va_list ap)
{
    size_t space = dflog_buf_space(b);

    /*
     If the output was truncated due to the size arg limit,
     then the return value is the number of characters
     (excluding the terminating null byte)
     which would have been written to the final string
     if enough space had been available.
     So, if return value of size arg or more means that the output was truncated.
     */
    ssize_t rc = vsnprintf(b->ptr, space, format, ap);
    if (rc < 0)
	return 0;

    //printf("%s() %d: , space: %zu, rc: %zu\n", __func__, __LINE__, space, rc);
    return (size_t)rc >= space ? -rc : rc;
}

// append log buffer
void vdflog_(const char *format, va_list ap)
{
    dflog_buf_t *b = dflog_buf_get();

    if (dflog_buf_check(b))
	return;

    va_list ap_save;
    va_copy(ap_save, ap);

    ssize_t rc = dflog_buf_vprintf(b, format, ap);
    if (rc < 0) // truncated
    {
	size_t space = dflog_buf_space(b);
	size_t new_size = b->size + (-rc + 1 - space);
	dflog_buf_t *nb = dflog_buf_realloc_set(b, new_size);
	if (nb == NULL) // realloc failed
	    return; //FIXME: out of memory failure
	b = nb;

	va_copy(ap, ap_save);
	rc = dflog_buf_vprintf(b, format, ap);
    }

    b->ptr += rc;
    *b->ptr = '\0';
}


// append log buffer
void dflog_(const char *format, ...)
{
    int err = errno;
    va_list ap;

    va_start(ap, format);
    vdflog_(format, ap);
    va_end  (ap);
    errno = err;
}


// append and flush log buffer
void dflog(int level, const char *format, ...)
{
    int err = errno;
    va_list ap;

    va_start(ap, format);
    vdflog_(format, ap);
    va_end  (ap);

    dflog_flush(level);
    errno = err;
}
