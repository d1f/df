#include <df/cron.h>
#include <df/error.h>
#include <df/whitespace.h>
#include <df/defs.h> // DF_ARRAY_LENGTH, DF_MEMZERO
#include <df/x.h>    // df_xstrdup
#include <df/bt.h>
#include <df/check.h>
#include <string.h>  // strsep
#include <stdio.h>

typedef struct range_str_s
{
    const char *left, *right;
    const char *div;
} range_str_t;

typedef struct range_s
{
    size_t left, right, div;
} range_t;

// src is list of ranges: M[-N][/D],M[-N][/D]...
void df_cron_parse_field(bool out[], size_t out_len, const char *_src, int inc)
{
    DF_BT;

    char *src = df_xstrdup(_src);

    memset(out, false, sizeof(out[0]) * out_len);

    char *token_list = src;

    while ((token_list = strsep(&src, ",")) != NULL)
    {
	df_strip_whitespace(token_list);

	range_str_t range_str; DF_MEMZERO(range_str);

	char *token_range = token_list;
	for (bool range_side = false;
	     (token_range = strsep(&token_list, "-")) != NULL;
	     range_side = !range_side
	    )
	{
	    df_strip_whitespace(token_range);

	    char *token_div = token_range;
	    for (bool div_side = false;
		 (token_div = strsep(&token_range, "/")) != NULL;
		 div_side = !div_side
		)
	    {
		df_strip_whitespace(token_div);

		if      (!range_side && !div_side)
		    range_str.left = token_div;
		else if (!range_side &&  div_side)
		    range_str.div  = token_div;
		else if ( range_side && !div_side)
		    range_str.right = token_div;
		else if ( range_side &&  div_side)
		    range_str.div = token_div;
	    } // slash
	} // minus

#if 0 // FIXME: debug only
	printf("str: left: %s, right: %s, div: %s\n",
	       range_str.left, range_str.right, range_str.div);
#endif

	range_t range = { .left = -1, .right = -1, .div = 1 };

	size_t out_max = out_len - 1;

	if (range_str.div == NULL)
	    range.div   = 1;
	else
	    range.div   = strcmp(range_str.div  , "*") == 0 ?  1 : (size_t)atoi(range_str.div);

	if (range.div == 0)
	    range.div  = 1;


	if (strcmp(range_str.left, "*") == 0)
	{
	    range.left  = 0;

	    if (range_str.right == NULL) // *
		range.right = out_max;
	    else                         // * - R
		range.right = strcmp(range_str.right, "*") == 0
		    ? out_max : (size_t)(atoi(range_str.right) + inc);
	}
	else
	{
	    range.left  = atoi(range_str.left);

	    if (range_str.right == NULL) // L
		range.right = range.left;
	    else                         // L - R
		range.right = strcmp(range_str.right, "*") == 0
		    ? out_max : (size_t)(atoi(range_str.right) + inc);
	}

	if (range.left  >= out_max)
	    range.left   = out_max;

	if (range.right >= out_max)
	    range.right  = out_max;

#if 0 // FIXME: debug only
	printf("num: left: %zu, right: %zu, div: %zu\n",
	       range.left, range.right, range.div);
#endif

	for (size_t i = range.left; i <= range.right; ++i)
	    if ((i % range.div) == 0)
		out[i] = true;
    } // comma

    free(src);
}

void df_cron_parse_fields(df_cron_t *cron,
			  const char *mins,
			  const char *hrs ,
			  const char *days,
			  const char *mons,
			  const char *dows)
{
    DF_BT;

    df_cron_parse_field(cron->mins, DF_ARRAY_LENGTH(cron->mins), mins,  0);
    df_cron_parse_field(cron->hrs , DF_ARRAY_LENGTH(cron->hrs ), hrs ,  0);
    df_cron_parse_field(cron->days, DF_ARRAY_LENGTH(cron->days), days,  0);
    df_cron_parse_field(cron->mons, DF_ARRAY_LENGTH(cron->mons), mons, -1);
    df_cron_parse_field(cron->dows, DF_ARRAY_LENGTH(cron->dows), dows, -1);
}
