#include <df/netaddr.h>
#include <df/bt.h>
#include <stdio.h>

void df_ip4addr_fprintf(FILE* out, df_ip4addr_t ip4addr, enum DF_IP4ADDR_ALIGN align)
{
    DF_BT;

    char str[DF_IP4ADDR_OUT_SIZE];
    df_ip4addr_sprintf(str, ip4addr, align);
    fputs(str, out);
}
