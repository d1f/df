#include <df/memtrue.h>
#include <df/bt.h>
#include <stdint.h> // uint32_t uintptr_t


/*
 * Like memchr.
 * Scans size bytes of the memory area pointed to by p for the non-zero byte.
 * Returns true if found, false if not.
 */
bool df_memtrue(const void *p, size_t size)
{
    DF_BT;

    // a few first unaligned bytes
    size_t first = sizeof(long) - (uintptr_t)p % sizeof(long);
    size -= first;

    const char *cp = p;

    while (first--)
	if (*cp++)
	    return true;

    // number of int's
    size_t longs = size / sizeof(long);
    size %= sizeof(long);

    const unsigned int *up = (const void *)cp;

    while (longs--)
	if (*up++)
	    return true;

    cp = (const void *)up;

    // a few last unaligned bytes
    while (size--)
	if (*cp++)
	    return true;

    return false;
}
