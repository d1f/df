#include <df/error.h>
#include <df/log.h> // dflog*
#include <df/bt.h>  // df_bt_exit
#include <df/defs.h> // ATTR
#include <stdarg.h> // va_list
#include <string.h> // strerror
#include <stdlib.h> // exit EXIT_SUCCESS


static void ATTR(nonnull(1,4))
vdferror(void (*exit_f)(int), int status, int errnum, const char *format, va_list ap)
{
    vdflog_(format, ap);

    if (errnum)
	dflog_(": %s", strerror(errnum));

    dflog_flush(LOG_ERR);

    if (status != EXIT_SUCCESS)
	exit_f(status);
}

// if ERRNUM is nonzero, follow it with ": " and strerror (ERRNUM).
// If STATUS is nonzero, terminate the program with `exit (STATUS)'.
void dferror(int status, int errnum, const char *format, ...)
{
    va_list ap;

    va_start(ap, format);
    vdferror(exit, status, errnum, format, ap);
    va_end  (ap);
}

// like dferror but with df_bt if the program will be terminated.
void dferror_bt(int status, int errnum, const char *format, ...)
{
    va_list ap;

    va_start(ap, format);
    vdferror(df_bt_exit, status, errnum, format, ap);
    va_end  (ap);
}
