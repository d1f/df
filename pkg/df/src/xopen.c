#define _GNU_SOURCE

#include <df/x.h>
#include <df/error.h>
#include <df/bt.h>

#include <errno.h>  // errno
#include <stdlib.h> // EXIT_FAILURE
#include <fcntl.h>  // open

int df_xopen(const char *path, int flags, mode_t mode)
{
    DF_BT;

    int fd = open(path, flags, mode);
    if (fd < 0)
	dferror(EXIT_FAILURE, errno, "open failed for %s", path);

    return fd;
}
