#include <stdio.h>    // printf
#include <stddef.h>   // size_t offsetof
#include <strings.h>  // strcasecmp
#include <string.h>   // strlen strcat
#include <sys/time.h> // gettimeofday
#include <time.h>     // localtime
#include <df/date-format.h>
#include <df/parse-string.h>
#include <df/locale-date-names.h>
#include <df/bt.h>


typedef struct datefmt_entry_s
{
    const char *key;
    const char *descr[DF_LOCALE_LANG_NUM];
    const char *fmt; // printf format
    size_t tm_offs;  // struct tm member offset
    size_t tv_offs;  // struct tv member offset
    df_locale_date_type_t lo_type;  // df_locale_date_names[lang][lo_type] index
    int   val_offs;  // struct tm member value offset
    int   val_rem;   // value reminder
    int   val_div;   // value divider
} datefmt_entry_t;

#define TO(m) offsetof(struct tm, m)
#define TV(m) offsetof(struct timeval, m)

#define EN DF_LOCALE_ENG
#define RU DF_LOCALE_RUS

static datefmt_entry_t datefmt_table[] =
{
    { .key = "year"   , .fmt = "%04d", .tm_offs = TO(tm_year), .val_offs = 1900,
                        .descr = { [EN] = "year (four digits, 2015)",
                                   [RU] = "год (четыре цифры, 2015)" }
    },
    { .key = "yy"     , .fmt = "%02d", .tm_offs = TO(tm_year), .val_rem  =  100,
                        .descr = { [EN] = "last two digits of year (00..99)",
			           [RU] = "две последние цифры года (00..99)" }
    },
    { .key = "month"  , .fmt = "%s"  , .tm_offs = TO(tm_mon) , .lo_type = DF_LOCALE_DATE_MONTH_LONG,
                        .descr = { [EN] = "full month name (January)",
                                   [RU] = "месяц (Январь)" }
    },
    { .key = "mon"    , .fmt = "%s"  , .tm_offs = TO(tm_mon) , .lo_type = DF_LOCALE_DATE_MONTH_SHORT,
                        .descr = { [EN] = "abbreviated month name (Jan)",
                                   [RU] = "месяц сокращенно (Янв)" }
    },
    { .key = "mn"     , .fmt = "%02d", .tm_offs = TO(tm_mon) , .val_offs =    1,
                        .descr = { [EN] = "month number (01..12)",
                                   [RU] = "номер месяца (01..12)" }
    },
    { .key = "yday"   , .fmt = "%03d", .tm_offs = TO(tm_yday), .val_offs =    1,
                        .descr = { [EN] = "day of year (001..366)",
                                   [RU] = "день года (001..366)" }
    },
    { .key = "weekday", .fmt = "%s"  , .tm_offs = TO(tm_wday), .lo_type = DF_LOCALE_DATE_DAY_OF_WEEK_LONG,
                        .descr = { [EN] = "full weekday name (Sunday)",
                                   [RU] = "день недели (Воскресенье)" }
    },
    { .key = "wday"   , .fmt = "%s"  , .tm_offs = TO(tm_wday), .lo_type = DF_LOCALE_DATE_DAY_OF_WEEK_SHORT3,
                        .descr = { [EN] = "abbreviated weekday name (three letters, Sun)",
                                   [RU] = "день недели сокращенно (три буквы, Вск)" }
    },
    { .key = "wd"     , .fmt = "%s"  , .tm_offs = TO(tm_wday), .lo_type = DF_LOCALE_DATE_DAY_OF_WEEK_SHORT2,
                        .descr = { [EN] = "abbreviated weekday name (two letters, Sn)",
                                   [RU] = "день недели сокращенно (две буквы, Вс" }
    },
    { .key = "dm"     , .fmt = "%02d", .tm_offs = TO(tm_mday),
                        .descr = { [EN] = "day of month (01..31)",
                                   [RU] = "день месяца (01..31)" }
    },
    { .key = "hour"   , .fmt = "%02d", .tm_offs = TO(tm_hour),
                        .descr = { [EN] = "hour (00..23)",
                                   [RU] = "часы (00..23)" }
    },
    { .key = "min"    , .fmt = "%02d", .tm_offs = TO(tm_min),
                        .descr = { [EN] = "minute (00..59)",
                                   [RU] = "минуты (00..59)" }
    },
    { .key = "sec"    , .fmt = "%02d", .tm_offs = TO(tm_sec),
                        .descr = { [EN] = "second (00..59)",
                                   [RU] = "секунды (00..59)" }
    },
    { .key = "msec"   , .fmt = "%03d", .tv_offs = TV(tv_usec), .val_div = 1000,
                        .descr = { [EN] = "millisecond (000..999)",
                                   [RU] = "миллисекунды (000..999)" }
    },
    { .key = "usec"   , .fmt = "%06d", .tv_offs = TV(tv_usec),
                        .descr = { [EN] = "microsecond (000000..999999)",
                                   [RU] = "микросекунды (000000..999999)" }
    },
    { .key = NULL }
};

static const datefmt_entry_t *table_search(const char *key)
{
    DF_BT;

    for (size_t i=0; datefmt_table[i].key != NULL; ++i)
    {
	if (strcasecmp(key, datefmt_table[i].key) == 0)
	    return  &datefmt_table[i];
    }

    return NULL;
}

// output format description list to stdout
// *_delim parameters may be 0
// col_sep, line_end parameters may be NULL
void df_date_format_list(const char *left_delim, const char *right_delim,
			 const char *col_sep, const char *line_end,
			 df_locale_lang_t lang)
{
    DF_BT;

    if (left_delim == NULL)
	left_delim = "";
    if (right_delim == NULL)
	right_delim = "";

    if (col_sep == NULL)
	col_sep = " - ";
    if (line_end == NULL)
	line_end = "\n";

    for (size_t i=0; datefmt_table[i].key != NULL; ++i)
    {
	const char *key = datefmt_table[i].key;
	char k[strlen(key)+strlen(left_delim)+strlen(right_delim)+1]; k[0] = 0;

	strcpy(k, left_delim);
	strcat(k, key);
	strcat(k, right_delim);

	printf("%20s%s%s%s",
	       k, col_sep, datefmt_table[i].descr[lang], line_end);
    }
}

typedef struct mytime_s
{
    struct timeval tv;
    struct tm      tm;
    df_locale_lang_t lang; // DF_LOCALE_ENG | DF_LOCALE_RUS
} mytime_t;

static int tm_val(const struct tm *tm, size_t offset)
{
    DF_BT;

    return * (const int*) (const void*) ( ((const char*)(const void*)tm) + offset );
}

static int val_proc(int val, const datefmt_entry_t *e)
{
    DF_BT;

    val += e->val_offs;

    if (e->val_div)
	val = (val + (e->val_div / 2)) / e->val_div;

    if (e->val_rem)
	val %= e->val_rem;

    return val;
}

static void df_date_format_entry(char *out, const datefmt_entry_t *e,
				 const mytime_t *mytime)
{
    DF_BT;

    if (e->tv_offs)
    {
	sprintf(out, e->fmt, val_proc(mytime->tv.tv_usec, e));
    }
    else if (e->lo_type != DF_LOCALE_DATE_NO)
    {
	sprintf(out, e->fmt,
		df_locale_date_names[mytime->lang][e->lo_type][tm_val(&mytime->tm, e->tm_offs)]);
    }
    else // e->tm_offs
    {
	sprintf(out, e->fmt, val_proc(tm_val(&mytime->tm, e->tm_offs), e));
    }
}

// returns error flag 'key not recognized'
static bool cb(char *out, size_t out_limit, void *user_data, const char *key)
{
    DF_BT;

    (void)out_limit;

    const mytime_t *mytime = user_data;

    const datefmt_entry_t *e = table_search(key);
    if (e == NULL)
	return true;

    df_date_format_entry(out, e, mytime);
    return false;
}

// returns error flag
bool df_date_format(char *out, size_t out_limit,
		    const char *fmt, char left_delim, char right_delim,
		    df_locale_lang_t lang,
		    const struct timeval *tv)
{
    DF_BT;

    mytime_t mytime;
    mytime.lang = lang;

    if (tv == NULL)
	gettimeofday(&mytime.tv, NULL);
    else
	mytime.tv = *tv;

    localtime_r(&mytime.tv.tv_sec, &mytime.tm);

    return df_parse_string(out, out_limit, cb, &mytime,
			   fmt, left_delim, right_delim);
}
