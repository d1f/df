#include <df/pidfile.h>
#include <df/error.h>
#include <df/log.h>
#include <df/bt.h>

#include <stdlib.h>  // EXIT_SUCCESS
#include <stdio.h>   // fopen fprintf
#include <string.h>  // strlen strdup
#include <unistd.h>  // getpid
#include <errno.h>   // errno


static void rm_pidfile(int rc, void *arg)
{
    DF_BT;

    (void)rc;
    unlink(arg);
}

// returns 0 or -1 on error.
// dferror() used for messages.
int df_write_pidfile(const char *pidfile)
{
    DF_BT;

    if (strlen(pidfile) == 0)
    {
	DFLOGE("empty pidfile supplied");
	return -1;
    }

    FILE *fpid = fopen(pidfile, "w");
    if (fpid == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "Can't open %s", pidfile);
	return -1;
    }

    char *pidfile_dup = strdup(pidfile);
    if (pidfile_dup == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): No memory for pidfile name copy %s",
		__func__, pidfile);
	fclose(fpid);
	return -1;
    }

    if (on_exit(rm_pidfile, pidfile_dup) != 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): on_exit call failed", __func__);
	free(pidfile_dup);
	fclose(fpid);
	return -1;
    }

    int rc = fprintf(fpid, "%d\n", getpid());
    if (rc < 0)
    {
	dferror(EXIT_SUCCESS, errno, "Can't write to %s", pidfile);
	fclose(fpid);
	return -1;
    }

    rc = fclose(fpid);
    if (rc < 0)
    {
	dferror(EXIT_SUCCESS, errno, "Can't close %s", pidfile);
	return -1;
    }

    return 0;
}
