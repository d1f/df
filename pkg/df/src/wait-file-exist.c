#include <df/file-exist.h>
#include <df/poll-loop.h>
#include <df/bt.h>


// returns: >0: exist, 0: not exist, <0: error
// outputs messages via dferror(), no exits
int df_wait_file_exist(const char *path, size_t interval_msec, size_t timeout_msec)
{
    DF_BT;

    return df_poll_loop((df_poll_f)df_is_file_exist, path,
			interval_msec, timeout_msec);
}
