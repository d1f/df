#define _GNU_SOURCE
#include <df/parse-string.h>
#include <df/log.h>
#include <df/bt.h>
#include <string.h> // strchrnul


typedef enum
{
    ST_USER_CHAR,
    ST_KEY_CHAR,
} states_t;


static char *str_init(char *dst)
{
    DF_BT;

    *dst = '\0';
    return dst;
}

static char *str_append(char *dst, char c)
{
    DF_BT;

    *dst++ = c;
    *dst   = '\0';
    return dst;
}

// returns error flag
bool df_parse_string(char *out, size_t out_limit, df_parse_string_cb cb, void *user_data,
		     const char *src, char left_delim, char right_delim)
{
    DF_BT;

    states_t state = ST_USER_CHAR;
    char *out_ptr  = out;

    char  key[strlen(src)+1];
    char *key_ptr = str_init(key);

    for (const char *cp = src; *cp != '\0'; ++cp)
    {
	char c = *cp;

	switch (state)
	{
	    case ST_USER_CHAR:
		if (c == left_delim)
		{
		    state = ST_KEY_CHAR;
		}
		else if (c == right_delim)
		{
		    DFLOGE("Syntax error: right delimiter in user context");
		    return true;
		}
		else // user char
		{
		    if ((size_t)(out_ptr - out) > out_limit-1)
		    {
			DFLOGE("out limit %zu exhausted", out_limit);
			return true;
		    }
		    out_ptr = str_append(out_ptr, c);
		    out_limit--;
		}
		break;
	    case ST_KEY_CHAR:
		if (c == right_delim)
		{
		    if (cb(out_ptr, out_limit, user_data, key))
		    {
			DFLOGE("key %s is not recognized", key);
			return true;
		    }

		    char *nul = strchrnul(out_ptr, '\0');
		    if (nul == NULL)
		    {
			DFLOGE("strchrnul() returned NULL");
			return true;
		    }
		    out_limit -= (nul - out_ptr);
		    out_ptr = nul;

		    key_ptr = str_init(key);

		    state = ST_USER_CHAR;
		}
		else if (c == left_delim)
		{
		    DFLOGE("Syntax error: nested left delimiter");
		    return true;
		}
		else // key char
		{
		    if ((size_t)(key_ptr - key) > sizeof(key)-1)
		    {
			DFLOGE("key is too long");
			return true;
		    }

		    key_ptr = str_append(key_ptr, c);
		}
		break;
	    default:
		DFLOGE("Unknown state %d", state);
		return true;
	}
    }

    return false;
}
