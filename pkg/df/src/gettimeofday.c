#include <df/time.h>
#include <df/bt.h>
#include <stdlib.h> // NULL

// returns time in microseconds
long long df_gettimeofday(void)
{
    DF_BT;

    struct timeval tv;
    gettimeofday( &tv, NULL );

    long long ret = df_timeval2longlong( &tv );

    return ret;
}
