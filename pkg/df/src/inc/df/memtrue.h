#ifndef  DF_MEMTRUE_H
# define DF_MEMTRUE_H

# include <stddef.h> // size_t
# include <stdbool.h>
# include <df/defs.h>  // ATTR


/*
 * Like memchr.
 * Scans size bytes of the memory area pointed to by p for the non-zero byte.
 * Returns true if found, false if not.
 */
bool df_memtrue(const void *p, size_t size) ATTR(nonnull);


#endif //DF_MEMTRUE_H
