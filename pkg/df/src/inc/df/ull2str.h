#ifndef DF_ULL2STR_H
#define DF_ULL2STR_H

#include <df/types.h>
#include <df/free-list.h>


const char *df_ull2str(ullong num, char sep, df_free_list_t **flp) ATTR(nonnull);


#endif //DF_ULL2STR_H
