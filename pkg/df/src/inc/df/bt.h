#ifndef  DF_BT_H
# define DF_BT_H

# include <signal.h> // siginfo_t
# include <stddef.h> // NULL
# include <df/defs.h> // ATTR


# if 0 // backtrace usage example

#include <df/bt.h>

ret_t function(args)
{
    DF_BT;

    // body

    return val;
}

# endif // usage example



        struct df_bt_s;
typedef struct df_bt_s df_bt_t;

struct df_bt_s
{
    const char * const func;
    df_bt_t *prev;
};


df_bt_t *df_bt_last_get(void);
void     df_bt_last_set(df_bt_t *last);

extern __inline__ ATTR(always_inline, nonnull)
void df_bt_leave(df_bt_t *bt)
{
    df_bt_last_set(bt->prev);
    bt->prev = NULL;
}


#ifdef NO_DF_BT
# define DF_BT do { } while(0)
#else
# define DF_BT \
    df_bt_t ATTR( __cleanup__(df_bt_leave) ) \
        df_bt_frame = { __func__, df_bt_last_get() }; \
    df_bt_last_set(&df_bt_frame)
#endif

#define DF_BT_ENTRY DF_BT


typedef void (*df_bt_f)(const char * const func);

void df_bt_foreach(df_bt_f f) ATTR(nonnull);


// log backtrace via dflog
void df_bt(void);

// df_bt with exit
void df_bt_exit(int status) ATTR(noreturn);

// df_bt with abort and signal name
void df_bt_abort(int sig) ATTR(noreturn);

// signal handler with backtrace and abort
void df_bt_siginfo_handler(int sig, siginfo_t *si, void *ucontext);

void df_bt_xset_signals(void (*sih)(int sig, siginfo_t *si, void *ucontext)) ATTR(nonnull);


#endif //DF_BT_H
