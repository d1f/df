#ifndef  DF_TYPES_H
# define DF_TYPES_H

# include <stdint.h> // uint32_t
# include <stdbool.h>
# include <df/defs.h>  // ATTR


#ifndef ON
#define ON  true
#endif

#ifndef OFF
#define OFF false
#endif


typedef unsigned      char  uchar;
typedef unsigned     short ushort;
typedef unsigned       int   uint;
typedef unsigned      long  ulong;
typedef          long long  llong;
typedef unsigned long long ullong;


typedef       void*  df_vptr_t;
typedef const void* df_cvptr_t;

typedef uint32_t df_signature_t; // ap-hash.h

typedef void (*df_item_dtor_f)(void *) ATTR(nonnull);

// returns: <0 if left < right, =0 if left == right, >0 if left > right
// for strings use: return strcmp( *(const df_cvptr_t*)left, *(const df_cvptr_t*)right );
typedef int  (*df_item_cmp_f)(const void *left, const void *right) ATTR(nonnull);

typedef void (*df_item_check_f)(void const *) ATTR(nonnull);


typedef union df_item_u
{
    df_vptr_t    vPtr;
    char       * cPtr;
    const char *ccPtr;
     long        Long;
    ulong       ULong;
} df_item_t;


typedef struct
{
    const char *file;
    size_t      line;
    const char *func;
} df_flf_t;

#define DF_FLF ({ static const df_flf_t flf = { __FILE__, __LINE__, __func__ }; &flf; })


#endif //DF_TYPES_H
