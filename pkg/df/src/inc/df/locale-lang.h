#ifndef  DF_LOCALE_LANG_H
# define DF_LOCALE_LANG_H

# include <stdbool.h>
# include <df/defs.h> // ATTR


typedef enum DF_LOCALE
{
    DF_LOCALE_ENG,
    DF_LOCALE_RUS,
} df_locale_lang_t;

enum { DF_LOCALE_LANG_NUM = DF_LOCALE_RUS + 1 };


// accepts lang string: ru[s]|en[g]
// out to *lang
// returns error flag, error message out via dferror()
bool df_locale_lang_parse(df_locale_lang_t *lang, const char *lang_str)
    ATTR(nonnull);


#endif //DF_LOCALE_LANG_H
