#ifndef  DF_TIME_H
# define DF_TIME_H

# include <time.h>     // struct timespec
# include <sys/time.h> // gettimeofday
# include <df/defs.h> // ATTR


// converts timeval to microseconds
static inline ATTR(nonnull)
long long df_timeval2longlong(const struct timeval* tv)
{
    return (long long)tv->tv_sec * 1000000 + tv->tv_usec;
}

// converts timespec to nanoseconds
static inline ATTR(nonnull)
long long df_timespec2longlong(const struct timespec* ts)
{
    return (long long)ts->tv_sec * 1000000000LL + ts->tv_nsec;
}


// returns time in microseconds
long long df_gettimeofday(void);


static inline ATTR(nonnull)
struct timeval df_timeval_diff(const struct timeval *minuend,
			       const struct timeval *subtrahend)
{
    struct timeval tv =
    {
	.tv_sec  = minuend->tv_sec  - subtrahend->tv_sec,
	.tv_usec = minuend->tv_usec - subtrahend->tv_usec
    };

    return tv;
}

static inline ATTR(nonnull)
struct timespec df_timespec_diff(const struct timespec *minuend,
				 const struct timespec *subtrahend)
{
    struct timespec ts =
    {
	.tv_sec  = minuend->tv_sec  - subtrahend->tv_sec,
	.tv_nsec = minuend->tv_nsec - subtrahend->tv_nsec
    };

    return ts;
}


#endif //DF_TIME_H
