#ifndef  DF_READ_ALL_H
# define DF_READ_ALL_H

# include <sys/types.h> // ssize_t size_t
# include <df/defs.h> // ATTR


// returns -1 on error, 0 on EOF, >0 on success
ssize_t df_read_all(int fd, void *buf, size_t count) ATTR(nonnull);


#endif //DF_READ_ALL_H
