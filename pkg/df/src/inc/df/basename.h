#ifndef DF_BASENAME_H
#define DF_BASENAME_H

#include <df/defs.h>


const char* df_basename(const char *filename)
    ATTR(nonnull, returns_nonnull);


#endif //DF_BASENAME_H
