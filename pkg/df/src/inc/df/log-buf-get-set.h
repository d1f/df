#ifndef  DF_LOG_BUF_GET_SET_H
# define DF_LOG_BUF_GET_SET_H

# include <stddef.h> // NULL


// interface for thread-specific dflog libs:

        struct dflog_buf_s;
typedef struct dflog_buf_s dflog_buf_t;

// init once, set thread-specific buffer
void         dflog_buf_set_(dflog_buf_t *b);

// init once, return thread-specific buffer
dflog_buf_t *dflog_buf_get_(void);


void         dflog_buf_delete(dflog_buf_t *b);


#endif //DF_LOG_BUF_GET_SET_H
