#ifndef  DF_DEFS_H
# define DF_DEFS_H

# ifndef _GNU_SOURCE
# define _GNU_SOURCE
# endif
# include <string.h> // memset
# include <stddef.h> // offsetof


# define DF_MEMZERO(obj)	memset(&(obj), 0, sizeof(obj))


# define DF_ARRAY_LENGTH(arr) ( sizeof((arr)) / sizeof((arr)[0]) )


// minimal alignment required by a type
# ifndef  ALIGNOF
#  define ALIGNOF(type) offsetof(struct { char c; (type) (member); }, (member))
# endif


# if __GNUC__ >= 4 && __GNUC_MINOR__ >= 9
#    define returns_nonnull returns_nonnull
# else
#    define returns_nonnull
# endif


# if __GNUC__ >= 4 && __GNUC_MINOR__ >= 4
#  define gnu_printf __gnu_printf__
# else
#  define gnu_printf     __printf__
#endif


# define ATTR(...) __attribute__ (( __VA_ARGS__ ))


# if defined  __cplusplus || defined c_plusplus /* for C++ V2.0 */
#  define DF_C_BEGIN extern "C" { /* do not leave open across includes */
#  define DF_C_END }
# else
#  define DF_C_BEGIN
#  define DF_C_END
# endif


#endif //DF_DEFS_H
