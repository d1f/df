#ifndef  DF_POLL_LOOP_H
#define  DF_POLL_LOOP_H

#include <stddef.h> // size_t
#include <df/defs.h> // ATTR


// returns: <0: fatal, end loop; 0: fail; >0: ok
typedef int (*df_poll_f)(const void *data) ATTR(nonnull);

// returns: <0: fatal, end loop; 0: fail, timeout; >0: ok
int df_poll_loop(df_poll_f pf, const void *data,
		 size_t interval_msec, size_t timeout_msec)
     ATTR(nonnull);


#endif //DF_POLL_LOOP_H
