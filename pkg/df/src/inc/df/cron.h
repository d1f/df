#ifndef  DF_CRON_H
# define DF_CRON_H

# include <stdbool.h>
# include <stddef.h>  // size_t
# include <df/defs.h> // ATTR


typedef struct df_cron_s
{
    int  id; // for user
    bool mins[60]; // 0-59
    bool hrs [24]; // 0-23
    bool days[32]; // 1-31
    bool mons[12]; // 0-11
    bool dows[ 7]; // 0-6, beginning sunday
    int  flags; // for user
} df_cron_t;


// src is list of ranges: M[-N][/D],M[-N][/D]...
void df_cron_parse_field(bool out[], size_t out_len, const char *src, int inc) ATTR(nonnull);

void df_cron_parse_fields(df_cron_t *cron,
			  const char *mins,
			  const char *hrs ,
			  const char *days,
			  const char *mons,
			  const char *dows) ATTR(nonnull);


#endif //DF_CRON_H
