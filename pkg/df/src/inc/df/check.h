#ifndef  DF_CHECK_H
# define DF_CHECK_H

# include <df/types.h>
# include <df/log.h>


// out message via dflog_xbt() if ptr == NULL
void df_check_null(const df_flf_t *flf, const char *what,
		   const void * volatile ptr) ATTR(nonnull(1,2));

// out message via dflog_xbt() if ptr == NULL
# define DF_CHECK_NULL(what, ptr) \
    if (ptr == NULL) \
	dflog_xbt(DF_FLF, "%s is NULL", what)


// out message via dflog_xbt() if signature mismatched
void df_check_signature(const df_flf_t *flf, const char *struct_name,
			df_signature_t signature_value, df_signature_t signature_const) ATTR(nonnull(1,2));

// <PKG-signatures.h> must be included before
// out message via dflog_xbt() if signature mismatched
# define DF_CHECK_SIGNATURE(struct_name, ptr) \
    if ((ptr)->signature != struct_name##_SIGNATURE) \
	dflog_xbt(DF_FLF, "struct %s signature mismatched", #struct_name)


// decrement unsigned counter if > 0 or dflog_xbt().
// returns decremented value.
size_t df_dec(df_flf_t const *flf, const char *what, size_t *count_p) ATTR(nonnull);


#endif //DF_CHECK_H
