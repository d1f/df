#ifndef  DF_FILENAME_TEMPLATE_H
# define DF_FILENAME_TEMPLATE_H

# include <limits.h>    // NAME_MAX
# include <stddef.h>    // size_t NULL
# include <sys/time.h>  // struct timeval
# include <df/locale-lang.h> // df_locale_lang_t
# include <df/defs.h> // ATTR


void df_filename_template(char out[NAME_MAX],
			  const char *prefix,   // NULL allowed
			  const char *suffix,   // NULL allowed
			  const size_t *seqnum, // NULL allowed
			  char left_delim, char right_delim,
			  const char *template,
			  df_locale_lang_t lang,
			  const struct timeval *tv // calls gettimeofday() if NULL
		      ) ATTR(nonnull(1,7));


#endif //DF_FILENAME_TEMPLATE_H
