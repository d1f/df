/*
	<df/x.h>

	Wrappers with exit on failure, never returns eror.

	(C) 1999-2015,2017 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_X_H
# define DF_X_H

# include <stddef.h>	// size_t
# include <signal.h>	// struct sigaction
# include <fcntl.h>	// open, flags, mode_t
# include <df/defs.h>	// returns_nonnull


void* df_xmalloc (size_t size)                                     ATTR(malloc, returns_nonnull);
void* df_xzmalloc(size_t size)                                     ATTR(malloc, returns_nonnull);
void* df_xrealloc(void* ptr, size_t size)                          ATTR(        returns_nonnull);
void* df_xmemdup (const void* src, size_t len)                     ATTR(nonnull,returns_nonnull);
char* df_xstrdup (const char* src)                                 ATTR(nonnull,returns_nonnull);
int   df_xopen   (const char *path, int flags, mode_t mode)        ATTR(nonnull);
void  df_xioctl  (const char *pfx, int fd, int request, void *arg) ATTR(nonnull(1));
void  df_xsigaction(int signum, const struct sigaction *act)       ATTR(nonnull);
void  df_xsigact   (int signum, int flags, void (*sa_h)(int))      ATTR(nonnull);

const char *df_si_code_str(int si_code, int signo);

void df_freep(void *vpp);


#endif	// DF_X_H
