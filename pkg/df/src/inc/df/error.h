/*
	error(3) via dflog

	(C) 2012 Dmitry Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_ERROR_H
# define DF_ERROR_H


# include <stdlib.h>   // EXIT_SUCCESS, EXIT_FAILURE
# include <errno.h>    // errno
# include <df/defs.h>  // ATTR


// if ERRNUM is nonzero, follow it with ": " and strerror (ERRNUM).
// If STATUS is nonzero, terminate the program with `exit (STATUS)'.
void dferror(int status, int errnum, const char *format, ...)
     ATTR(format(gnu_printf, 3, 4), nonnull(3));

// like dferror but with df_bt if the program will be terminated.
void dferror_bt(int status, int errnum, const char *format, ...)
     ATTR(format(gnu_printf, 3, 4), nonnull(3));


#endif //DF_ERROR_H
