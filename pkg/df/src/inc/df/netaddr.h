#ifndef  DF_NETADDR_H
# define DF_NETADDR_H

# include <stddef.h>     // size_t
# include <stdint.h>     // [u]intXX_t
# include <stdio.h>      // FILE
# include <netinet/in.h> // INADDR_ANY, INADDR_BROADCAST
# include <df/defs.h>    // ATTR


# ifndef  ETHADDR_ANY
#  define ETHADDR_ANY       0ull
# endif

# ifndef  ETHADDR_MASK
#  define ETHADDR_MASK      0xffffffull
# endif

# ifndef  ETHADDR_BROADCAST
#  define ETHADDR_BROADCAST 0xffffffull
# endif


typedef uint32_t df_ip4addr_t;
typedef uint64_t df_ethaddr_t;


enum DF_IP4ADDR_ALIGN { DF_IP4ADDR_NO_ALIGN, DF_IP4ADDR_ALIGN_OCTETS, DF_IP4ADDR_ALIGN_WHOLE };

#define DF_IP4ADDR_OUT_SIZE 16
#define DF_ETHADDR_OUT_SIZE 18

void   df_ip4addr_sprintf(char  out[DF_IP4ADDR_OUT_SIZE], df_ip4addr_t ipaddr, enum DF_IP4ADDR_ALIGN align)
    ATTR(nonnull);
void   df_ip4addr_fprintf(FILE* out                     , df_ip4addr_t ipaddr, enum DF_IP4ADDR_ALIGN align)
    ATTR(nonnull);

// returns prefix length from subnet mask
size_t df_ip4addr_mask2pfxlen(df_ip4addr_t netmask);

void   df_ethaddr_sprintf(char  out[DF_ETHADDR_OUT_SIZE], df_ethaddr_t ethaddr)
    ATTR(nonnull);
void   df_ethaddr_fprintf(FILE* out                     , df_ethaddr_t ethaddr)
    ATTR(nonnull);

int    df_ip4addr_sscanf(const char *in, df_ip4addr_t *ip4addr) ATTR(nonnull);
int    df_ethaddr_sscanf(const char *in, df_ethaddr_t *ethaddr) ATTR(nonnull);


#endif	//DF_NETADDR_H
