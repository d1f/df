#ifndef  DF_CAT_H
# define DF_CAT_H

# include <df/defs.h>  // ATTR


int df_cat(int outfd, const char *file) ATTR(nonnull);


#endif //DF_CAT_H
