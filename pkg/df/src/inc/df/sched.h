#ifndef  DF_SCHED_H
# define DF_SCHED_H

# include <sched.h>
# include <sys/types.h> // pid_t
# include <df/defs.h> // ATTR


typedef struct df_sched_param_s
{
    struct sched_param param;
    int policy;
    pid_t pid;
} df_sched_param_t;


// returns 0 or -errno on error, message via dferror.

int df_sched_set(pid_t pid, int policy, int priority, df_sched_param_t *save /* may be NULL*/);

int df_sched_restore(const df_sched_param_t *save) ATTR(nonnull);


#endif //DF_SCHED_H
