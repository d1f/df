#ifndef  DF_PIPE_FORK_H
# define DF_PIPE_FORK_H

# include <df/defs.h> // ATTR


// fork new process assigning descriptors of two communicating pipes
// for parent and child.
// returns pid from fork(2) or -1 on error.
int df_pipe2_fork(int *pipe_rd, int *pipe_wr) ATTR(nonnull);

// USED in rs232-tcpdmx
// fork new process assigning descriptors of four communicating pipes
// for parent and child.
// returns pid from fork(2) or -1 on error.
int df_pipe4_fork(int *pipe1_rd, int *pipe1_wr, int *pipe2_rd, int *pipe2_wr);


// fork new process assigning one of communicating sockets from pair
// to each of processes for parent and child.
// parent and child pointers may be NULL.
// returns pid from fork(2) or -1 on error.
int df_socketpair_fork(int *parent, int *child);


#endif	//DF_PIPE_FORK_H
