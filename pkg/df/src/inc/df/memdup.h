#ifndef  DF_MEMDUP_H
# define DF_MEMDUP_H

# include <stddef.h>   // size_t NULL
# include <df/defs.h>  // ATTR


void *df_memdup(const void *src, size_t size) ATTR(nonnull);


#endif //DF_MEMDUP_H
