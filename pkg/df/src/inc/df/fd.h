/*
	(C) 2012 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_FD_H
# define DF_FD_H

# include <stdbool.h>


bool df_is_fd_opened(int fd);
bool df_is_fd_socket(int fd);

long df_fd_get_flags    (int fd); // returns flags or -1 on error with error message
int  df_fd_set_flags    (int fd, long flags); // returns -1 on error with error message
int  df_fd_restore_flags(int fd, long flags); // returns df_fd_set_flags return if flags >= 0

long df_fd_set_block    (int fd); // returns old flags or -1 on error
long df_fd_set_nonblock (int fd); // returns old flags or -1 on error


#endif //DF_FD_H
