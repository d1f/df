#ifndef  DF_SYSTEM_STATUS_H
# define DF_SYSTEM_STATUS_H


// process system(3) status and complain about errors via dferror()
// returns: <0 on error, 0 on success
int df_system_status(const char *cmd, int sysret);


#endif //DF_SYSTEM_STATUS_H
