#ifndef  DF_EMERGENCY_LOG_H
# define DF_EMERGENCY_LOG_H

# include <stdarg.h>  // va_list
# include <df/defs.h> // ATTR, gnu_printf


/*
 Output log message about unrecoverable condition (out of memory ...)
 to STDERR_FILENO if opened, console, system log.
 Don't allocate memory for buffers.
 */
void df_velog(const char *format, va_list ap) ATTR(nonnull);
void df_elog (const char *format, ...)        ATTR(format(gnu_printf, 1, 2), nonnull(1));


#endif //DF_EMERGENCY_LOG_H
