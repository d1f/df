#ifndef  DF_FILENAME_TIMESTAMP_H
# define DF_FILENAME_TIMESTAMP_H

# include <limits.h>   // NAME_MAX
# include <sys/time.h> // struct timeval
# include <stddef.h>   // size_t NULL
# include <df/defs.h>  // ATTR


void df_filename_timestamp(char out[NAME_MAX],
			   const char *prefix,      // NULL allowed
			   const char *suffix,      // NULL allowed
			   const size_t *seqnum,    // NULL allowed
			   const struct timeval *tv // calls gettimeofday() if NULL
			  ) ATTR(nonnull(1));


#endif //DF_FILENAME_TIMESTAMP_H
