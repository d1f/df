/*
	<df/whitespace.h>

	string white space manipulation routines

	(C) 2000-2015 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_WHITESPACE_H
# define DF_WHITESPACE_H

# include <df/defs.h> // ATTR


void        df_strip_lead_whitespace(char *str) ATTR(nonnull);
void        df_strip_tail_whitespace(char *str) ATTR(nonnull);
void        df_strip_whitespace     (char *str) ATTR(nonnull);

// returns pointer to first non-whitespace character.
char const* df_skip_whitespace_const(char const *str) ATTR(nonnull);

// returns pointer to line's character after non-whitespace word.
const char* df_skip_word            (char const *str) ATTR(nonnull);


static inline ATTR(nonnull)
char* df_skip_whitespace(char *str)
{
    return str + ( df_skip_whitespace_const(str) - str );
}


#endif //DF_WHITESPACE_H
