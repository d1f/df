#ifndef  DF_ENDIAN_H
# define DF_ENDIAN_H

# include <endian.h>


#ifdef __UCLIBC__
# if __BYTE_ORDER == __BIG_ENDIAN
#  define be64toh(x) (x)
#  define be32toh(x) (x)
#  define htobe64(x) (x)
#  define htobe32(x) (x)
# else
#  if __BYTE_ORDER == __LITTLE_ENDIAN
#   include <byteswap.h>
#   define be64toh(x)   bswap_64(x)
#   define be32toh(x)   bswap_32(x)
#   define htobe64(x)   bswap_64(x)
#   define htobe32(x)   bswap_32(x)
#  endif
# endif
#endif

#ifdef __GLIBC__
# if   __GLIBC__ > 2 || (__GLIBC__ == 2 && __GLIBC_MINOR__ < 9)
#  if __BYTE_ORDER == __BIG_ENDIAN
#   define be64toh(x) (x)
#   define be32toh(x) (x)
#   define htobe64(x) (x)
#   define htobe32(x) (x)
#  else
#   if __BYTE_ORDER == __LITTLE_ENDIAN
#    include <byteswap.h>
#    define be64toh(x)   bswap_64(x)
#    define be32toh(x)   bswap_32(x)
#    define htobe64(x)   bswap_64(x)
#    define htobe32(x)   bswap_32(x)
#   endif
#  endif
# endif
#endif


#endif //DF_ENDIAN_H
