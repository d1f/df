#ifndef DF_BITSET_H
#define DF_BITSET_H

#include <limits.h> // CHAR_BIT


#define DF_BITSET_CHAR(charmap, bitnum) \
    (charmap[bitnum / CHAR_BIT])

#define DF_BITSET_MASK(bitnum) \
    (1 << (bitnum % CHAR_BIT))

#define DF_BITSET_GET(charmap, bitnum) \
    (DF_BITSET_CHAR(charmap, bitnum) & DF_BITSET_MASK(bitnum))

#define DF_BITSET_SET(charmap, bitnum) \
    (DF_BITSET_CHAR(charmap, bitnum) |= DF_BITSET_MASK(bitnum))

#define DF_BITSET_CLR(charmap, bitnum) \
    (DF_BITSET_CHAR(charmap, bitnum) &= ~DF_BITSET_MASK(bitnum))


#endif //DF_BITSET_H