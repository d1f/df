#ifndef  DF_PID_FILE_H
# define DF_PID_FILE_H

# include <sys/types.h> // pid_t
# include <df/defs.h> // ATTR


// returns pid from file or -1 on error.
// dferror() used for messages.
pid_t df_read_pidfile(const char *pidfile) ATTR(nonnull);

// returns 0 or -1 on error.
// dferror() used for messages.
int  df_write_pidfile(const char *pidfile) ATTR(nonnull);


#endif //DF_PID_FILE_H
