#ifndef  DF_AP_HASH_H
# define DF_AP_HASH_H

// (C) Arash Partow http://www.partow.net/programming/hashfunctions/

# include <stddef.h> // size_t
# include <stdint.h> // uint32_t
# include <df/defs.h> // ATTR


typedef uint32_t df_ap_hash_t;

void df_ap_hash    (void *out, const void *src, size_t len)  ATTR(nonnull);
void df_ap_hash_str(void *out, const char *str)              ATTR(nonnull);


# include <df/hash.h>
extern const df_hash_alg_t df_ap_hash_alg;


#endif //DF_AP_HASH_H
