#ifndef  DF_KILLALL_H
# define DF_KILLALL_H

# include <df/defs.h> // ATTR


/*
 Got from
 http://jakash3.wordpress.com/2011/06/17/linux-kill-all-processes-with-name/
*/

// returns: -1 on error with errno or number of processes to be signaled
int killall(const char* name, int sig) ATTR(nonnull);


#endif //DF_KILLALL_H
