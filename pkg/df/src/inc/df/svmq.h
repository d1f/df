#ifndef  DF_SVMQ_H
# define DF_SVMQ_H

# include <stddef.h> // size_t
# include <df/defs.h>  // ATTR


typedef struct df_svmq_s
{
    long dst;
    long src;
    char data[0];
} df_svmq_t;


# define DF_SVMQ_MSG_SIZE(data_size) \
    (sizeof(df_svmq_t) - sizeof(long) + data_size)


// returns message queue id or -1 on error
int df_svmq_get_qid(const char *prog_path, int project_id)           ATTR(nonnull);


// server always has dst == 1

// returns 0 on succes or -1 on error
int  df_svmq_send(int mqid, long dst, void *data, size_t data_size)  ATTR(nonnull);

// returns received message src on succes or -1 on error
long df_svmq_recv(int mqid, long dst, void *data, size_t *data_size) ATTR(nonnull);


#endif //DF_SVMQ_H
