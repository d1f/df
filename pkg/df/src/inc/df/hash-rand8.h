#ifndef  DF_HASH_RAND8_H
# define DF_HASH_RAND8_H

// hashing by XOR string characters with random value.
// http://algolist.manual.ru/ds/s_has.php

# include <df/hash.h>


extern const df_hash_alg_t df_hash_rand8_alg;

void df_hash_rand8_len(void *out, const void *src, size_t len) ATTR(nonnull);
void df_hash_rand8_str(void *out, const char *str)             ATTR(nonnull);


#endif //DF_HASH_RAND8_H
