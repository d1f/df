#ifndef  DF_C_ESCAPE_H
# define DF_C_ESCAPE_H

# include <stddef.h> // size_t
# include <df/defs.h>  // ATTR


/*
Escape		Hex value	Character represented
sequence	in ASCII
--------	---------	-----------------------------------------
\a		07		Alarm (Beep, Bell)
\b		08		Backspace
\f		0C		Formfeed
\n		0A		Newline (Line Feed); see notes below
\r		0D		Carriage Return
\t		09		Horizontal Tab
\v		0B		Vertical Tab
\\		5C		Backslash
\'		27		Single quotation mark
\"		22		Double quotation mark
\?		3F		Question mark
\e		1B		Escape character
\nnn		any		The character whose numerical value is given by nnn interpreted as an octal number
\xhh		any		The character whose numerical value is given by hh interpreted as a hexadecimal number
*/


// escape ASCII characters to C escape sequences.
// returns required dst string size including terminating NUL.
// if returned size > dst_limit, dst was truncated.
size_t df_c_escape(char *dst, size_t dst_limit, const char *src) ATTR(nonnull);

// convert C escape sequences \C from sr to unescaped dst.
// dst size is not greater then src size.
void df_c_unescape(char *dst, const char *src) ATTR(nonnull);


#endif //DF_C_ESCAPE_H
