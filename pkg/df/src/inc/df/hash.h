#ifndef  DF_HASH_H
#define  DF_HASH_H

#include <stddef.h>   // size_t
#include <stdint.h>   // uint32_t
#include <df/defs.h>  // ATTR
#include <df/types.h> // df_signature_t df_flf_t
#include <df/check.h>


        struct df_hash_alg;
typedef struct df_hash_alg
{
    df_signature_t signature; // df_hash_alg_SIGNATURE

    size_t       const bits; // bit width of hash value
    size_t       const buckets_max; // 0 - unlimited

    void (* const hash_init)(void *out)                               ATTR(nonnull);
    void (* const hash_len )(void *out, const void *src, size_t len)  ATTR(nonnull);
    void (* const hash_str )(void *out, const char *str)              ATTR(nonnull);
} df_hash_alg_t;


// out message via dflog_xbt() if check failed
void df_hash_alg_check(const df_flf_t *flf, const df_hash_alg_t *alg) ATTR(nonnull);


#endif //DF_HASH_H
