#ifndef  DF_KV_H
# define DF_KV_H

# include <df/types.h>
# include <df/defs.h> // ATTR, returns_nonnull


typedef struct df_kv_s
{
    df_item_t key;
    df_item_t val;
} df_kv_t;

typedef void (*df_kv_dtor_f)(df_kv_t *kv);


void     df_kv_init0 (df_kv_t *kv) ATTR(nonnull);
void     df_kv_init  (df_kv_t *kv, const df_item_t key, const df_item_t val) ATTR(nonnull);
df_kv_t *df_kv_create(const df_item_t key, const df_item_t val) ATTR(returns_nonnull);
void     df_kv_delete(df_kv_t **kvp, df_kv_dtor_f dtor);

// for [lb]search, qsort...
int      df_kv_key_strcmp(const void *l, const void *r) ATTR(nonnull);


// key, val are strings and strdup'ed. val maybe NULL
df_kv_t *df_kv_str_create(const char *key, const char *val) ATTR(returns_nonnull,nonnull(1));
void     df_kv_str_delete(df_kv_t **kvp);
void     df_kv_str_dtor  (df_kv_t  *kv);


#endif //DF_KV_H
