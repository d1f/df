#ifndef DF_HEX_H
#define DF_HEX_H

#include <df/defs.h> // ATRR
#include <stdbool.h>


void df_bin2hex(char *dst, const void *src, size_t srclen)      ATTR(nonnull);
void df_str2hex(char *dst, const char *src)                     ATTR(nonnull);

// returns error flag
bool df_hex2bin(void *dst, size_t dstlen, const char *src)      ATTR(nonnull);

// returns error flag
bool df_hex2str(char *dst, const char *src)                     ATTR(nonnull);


#endif //DF_HEX_H
