/*
	Trace macros via dflog

	(C) 2012 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_TRACE_H
# define DF_TRACE_H

# include <df/log.h>


# define DFTRACE() dflog(LOG_INFO, "%s:%3d %s()", df_basename(__FILE__), __LINE__, __func__)

# define DFTRACEF(fmt, args...)	\
    do { dflog_("%s:%3d %s(): ", df_basename(__FILE__), __LINE__, __func__); \
         dflog (LOG_INFO, fmt , ##args); } while(0)


#endif //DF_TRACE_H
