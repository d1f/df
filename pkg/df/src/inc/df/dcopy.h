#ifndef  DF_DCOPY_H
# define DF_DCOPY_H

# include <stddef.h>


int df_dcopy(int infd, int outfd, size_t buf_size);


#endif //DF_DCOPY_H
