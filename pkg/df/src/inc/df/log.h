/*
	logging to stderr, stdout and syslog

	(C) 2012-2013 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_LOG_H
# define DF_LOG_H

# include <stdarg.h>  // va_list
# include <syslog.h>  // LOG_* level constants
# include <stddef.h>  // size_t
# include <stdlib.h>  // EXIT_FAILURE
# include <df/defs.h> // ATTR gnu_printf
# include <df/types.h> // df_flf_t
# include <df/basename.h>

# ifdef __cplusplus
   extern "C" {
# endif

// dflog_open flags
enum
{
    DFLOG_STDERR   =  1,	// output to stderr
    DFLOG_SYS      =  2,	// output to syslog
    DFLOG_PID      =  4,	// Prepend PID to each of message
    DFLOG_STDOUT   =  8,	// output to stdout
    DFLOG_BASENAME = 16,	// use basename of ident
};


// no thread-safe
void dflog_open (const char *ident, int flags);
void dflog_close(void);

int  dflog_flags(void);

const char *dflog_ident(void);


// use level LOG_NO to not change

void vdflog_(const char *format, va_list ap) // append log buffer
    ATTR (format(gnu_printf, 1, 0), nonnull(1));
void  dflog_(const char *format, ...)         // append log buffer
    ATTR(format(gnu_printf, 1, 2), nonnull(1));

void dflog(int level, const char *format, ...) // append and flush log buffer
    ATTR(format(gnu_printf, 2, 3), nonnull(2));

void dflog_flush(int level); // output log buffer
void dflog_free_buf(void);

extern inline const char* ATTR(nonnull, returns_nonnull, always_inline)
    dflog_basename(const char *filename)
{
    return df_basename(filename);
}

void dflog_stdio_hook(void);
void dflog_stdio_unhook(void);


// out mesage with "file:line func(): " prefix via dflog(LOG_ERR) and calls df_bt_exit(EXIT_FAILURE)
void vdflog_xbt(const df_flf_t *flf, const char *format, va_list ap)
    ATTR(format(gnu_printf, 2, 0), nonnull(1,2), noreturn);

// out mesage with "file:line func(): " prefix via dflog(LOG_ERR) and calls df_bt_exit(EXIT_FAILURE)
void dflog_xbt(const df_flf_t *flf, const char *format, ...)
    ATTR(format(gnu_printf, 2, 3), nonnull(1,2), noreturn);


# define DFLOG_(fmt, args...)	\
    do { dflog_("%s:%3d %s(): ", df_basename(__FILE__), __LINE__, __func__); \
         dflog_(fmt , ##args); } while(0)

# define DFLOGI(fmt, args...)	\
    do { DFLOG_(fmt, ##args); dflog_flush(LOG_INFO); } while(0)

# define DFLOGW(fmt, args...)	\
    do { DFLOG_(fmt, ##args); dflog_flush(LOG_WARNING); } while(0)

# define DFLOGE(fmt, args...)	\
    do { DFLOG_(fmt, ##args); dflog_flush(LOG_ERR); } while(0)

# define DFLOGEBT(fmt, args...) \
    do { DFLOGE(fmt, ##args); df_bt(); } while(0)

# define DFLOGEX(fmt, args...) \
    do { DFLOGE(fmt, ##args); exit(EXIT_FAILURE); } while(0)

# define DFLOGEXBT(fmt, args...) \
    do { DFLOGE(fmt, ##args); df_bt_exit(EXIT_FAILURE); } while(0)


# ifdef __cplusplus
   }
# endif

#endif //DF_LOG_H
