#ifndef  DF_KEY_VAL_H
# define DF_KEY_VAL_H

# include <stdint.h> // intptr_t
# include <df/defs.h> // ATTR

typedef struct df_key_val_s
{
    const char *key;
    intptr_t val;
} df_key_val_t;


// Linear case-insensitive search of key in keyvals table.
// Last entry in the table must have key == NULL.
// Returns found df_key_val_t pointer or NULL.
const df_key_val_t *df_key_val_lsearch(const df_key_val_t keyvals[], const char *key)
        ATTR(nonnull);


#endif //DF_KEY_VAL_H
