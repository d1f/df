/*
	<df/tty.h>

	getch(), kbhit() etc.

	(C) 1999-2016 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#ifndef  DF_TTY_H
# define DF_TTY_H

# include <df/defs.h>

DF_C_BEGIN



struct termios;


// set noncanonical input tty mode (w/o echo and <cr> waiting)
// returns 0 / -1 with errno from tcgetattr(), tcsetattr()
int df_tty_set_char_input_mode(int fd, int wait_flag,
			       struct termios* saved_tattr);

// restore saved tty modes
// returns 0 / -1 with errno from tcsetattr()
int df_tty_restore_attr(int fd, struct termios* saved_tattr) ATTR(nonnull);


// returns:
//	 0 - there are no characters in input queue;
//	>0 - there are characters in input queue;
//	<0 - error, see errno
int df_tty_kbhit(int fd);



int df_tty_fcheck_key(int fd, int wait_flag);	// main routine
int df_tty_check_key(int wait_flag);

extern inline int ATTR(always_inline)
    df_tty_fwait_key(int fd) { return df_tty_fcheck_key(fd, (~0)); }
extern inline int ATTR(always_inline)
    df_tty_wait_key(void)    { return df_tty_check_key(~0); }


DF_C_END

#endif //DF_TTY_H
