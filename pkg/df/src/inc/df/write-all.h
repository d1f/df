#ifndef  DF_WRITE_ALL_H
# define DF_WRITE_ALL_H

# include <sys/types.h> // size_t, ssize_t
# include <df/defs.h> // ATTR


ssize_t df_write_all(int fd, const void *buf, size_t count) ATTR(nonnull);


#endif	//DF_WRITE_ALL_H
