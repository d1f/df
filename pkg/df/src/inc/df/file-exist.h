#ifndef  DF_FILE_EXIST_H
# define DF_FILE_EXIST_H

# define _GNU_SOURCE
# include <stddef.h> // size_t
# include <unistd.h> // useconds_t
# include <df/defs.h> // ATTR


// returns: >0: exist, 0: not exist, <0: error
// outputs messages via dferror(), no exits
int   df_is_file_exist(const char *path) ATTR(nonnull);
int df_wait_file_exist(const char *path, size_t interval_msec, size_t timeout_msec)
    ATTR(nonnull);


#endif //DF_FILE_EXIST_H
