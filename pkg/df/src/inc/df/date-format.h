#ifndef  DF_DATE_FORMAT_H
# define DF_DATE_FORMAT_H

# include <stddef.h>   // size_t
# include <stdbool.h>
# include <time.h>     // struct tm
# include <sys/time.h> // struct timeval
# include <df/locale-lang.h> // df_locale_lang_t
# include <df/defs.h> // ATTR


// output format description list to stdout
// *_delim parameters may be NULL
// col_sep, line_end parameters may be NULL
void df_date_format_list(const char *left_delim, const char *right_delim,
			 const char *col_sep, const char *line_end,
			 df_locale_lang_t lang);


// returns error flag
bool df_date_format(char *out, size_t out_limit,
		    const char *fmt, char left_delim, char right_delim,
		    df_locale_lang_t lang,
		    const struct timeval *tv) // tm maybe NULL
    ATTR(nonnull(1,3));


#endif //DF_DATE_FORMAT_H
