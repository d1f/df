#ifndef DF_PARSE_STRING_H
#define DF_PARSE_STRING_H

#include <stddef.h> // size_t
#include <stdbool.h>
#include <df/defs.h> // ATTR


// returns error flag 'key not recognized'
typedef bool (*df_parse_string_cb)(char *out, size_t out_limit, void *user_data, const char *key)
    ATTR(nonnull(1,4));


// returns error flag
bool df_parse_string(char *out, size_t out_limit, df_parse_string_cb cb, void *user_data,
		     const char *src, char left_delim, char right_delim)
     ATTR(nonnull(1,3,5));


#endif //DF_PARSE_STRING_H
