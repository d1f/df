#ifndef DF_FREE_LIST_H
#define DF_FREE_LIST_H

#include <df/defs.h>


        struct df_free_list;
typedef struct df_free_list df_free_list_t;

void df_free_list_add (df_free_list_t **flp, void *mem) ATTR(nonnull);
void df_free_list_free(df_free_list_t **flp)            ATTR(nonnull);


#endif //DF_FREE_LIST_H
