#include <df/log.h>
#include <df/error.h>
#include <df/backtrace.h>
#include <features.h>

#ifdef __UCLIBC__
void df_backtrace(void)
{
    dflog(LOG_INFO, "%s(): No backtrace supported in uClibc", __func__);
}
#else

#include <execinfo.h>

#define BUF_LEN 1024

void df_backtrace(void)
{
    void *buf[BUF_LEN];
    ssize_t len = backtrace (buf, BUF_LEN);

    if (len < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): backtrace(BUF_LEN: %d)", __func__, BUF_LEN);
	return;
    }

    if (len >= BUF_LEN)
	dflog(LOG_INFO, "%s(): stack trace truncated", __func__);

    if (len == 0)
    {
	dflog(LOG_INFO, "%s(): No stack trace", __func__);
	return;
    }

    char ** syms = backtrace_symbols (buf, len);
    if (syms == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): backtrace_symbols()", __func__);
	return;
    }

    dflog(LOG_INFO, "backtrace:");

    for (size_t j = 0; j < (size_t)len; ++j)
	dflog(LOG_INFO, "\t%s", syms[j]);

    free(syms);
}
#endif
