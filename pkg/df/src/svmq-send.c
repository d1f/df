#include <df/svmq.h>
#include <df/error.h>
#include <df/bt.h>

#include <sys/msg.h>
#include <unistd.h> // getpid
#include <string.h> // memcpy


// returns 0 on succes or -1 on error
int df_svmq_send(int mqid, long dst, void *data, size_t data_size)
{
    DF_BT;

    char mdata[sizeof(df_svmq_t) + data_size];
    df_svmq_t *msg = (void*) &mdata[0];

    msg->dst = dst;
    msg->src = getpid();
    memcpy(&msg->data[0], data, data_size);

    ssize_t rc = msgsnd(mqid, msg, DF_SVMQ_MSG_SIZE(data_size), IPC_NOWAIT);
    if (rc < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): msgsnd() failed", __func__);
	return -1;
    }

    return 0;
}
