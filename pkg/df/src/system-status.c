#define _GNU_SOURCE

#include <df/system-status.h>
#include <df/error.h>
#include <sys/wait.h> // WEXITSTATUS
#include <string.h>   // strsignmal


// process system(3) status and complain about errors via dferror()
// returns: <0 on error, 0 on success
int df_system_status(const char *cmd, int sysret)
{
    if (sysret < 0)
    {
	dferror(EXIT_SUCCESS, errno, "Can't execute command '%s'", cmd);
	return -1;
    }

    if (WIFSIGNALED(sysret))
    {
	int sig = WTERMSIG(sysret);
	dferror(EXIT_SUCCESS, 0, "Command '%s' terminated by %s", cmd, strsignal(sig));
	return -1;
    }

    if (WIFSTOPPED(sysret))
    {
	int sig = WSTOPSIG(sysret);
	dferror(EXIT_SUCCESS, 0, "Command '%s' stopped by %s", cmd, strsignal(sig));
	return -1;
    }

    if (WIFEXITED(sysret))
    {
	int status = WEXITSTATUS(sysret);
	if (status != 0)
	{
	    dferror(EXIT_SUCCESS, 0, "Command '%s' completed with status %d", cmd, status);
	    return -1;
	}
    }

    return 0;
}
