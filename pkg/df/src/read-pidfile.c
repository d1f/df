#include <df/pidfile.h>
#include <df/error.h>
#include <df/bt.h>

#include <stdio.h>  // fopen fscanf
#include <stdlib.h> // EXIT_SUCCESS
#include <errno.h>  // errno


// returns pid from file or -1 on error.
// dferror() used for messages.
pid_t df_read_pidfile(const char *pidfile)
{
    DF_BT;

    FILE *fpid = fopen(pidfile, "r");
    if (fpid == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "Can't open %s", pidfile);
	return -1;
    }

    pid_t pid = -1;
    int rc = fscanf(fpid, "%d", &pid);
    if (rc < 0)
    {
	if (ferror(fpid))
	{
	    dferror(EXIT_SUCCESS, errno, "Can't read %s", pidfile);
	    pid = -1;
	}
	else
	{
	    dferror(EXIT_SUCCESS, 0, "Unexpected EOF on %s", pidfile);
	    pid = -1;
	}
    }
    else if (rc != 1)
    {
	dferror(EXIT_SUCCESS, 0, "Error parsing pid in %s", pidfile);
	pid = -1;
    }

    fclose(fpid);
    return pid;
}
