#include <df/svmq.h>
#include <df/error.h>
#include <df/bt.h>
#include <sys/msg.h>


// returns message queue id or -1 on error
int df_svmq_get_qid(const char *prog_path, int project_id)
{
    DF_BT;

    key_t mkey = ftok(prog_path, project_id);
    if (mkey < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): ftok(%s, %d) failed",
		__func__, prog_path, project_id);
	return -1;
    }

    int mqid = msgget(mkey, IPC_CREAT|0666);
    if (mqid < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): msgget() failed", __func__);
	return -1;
    }

    return mqid;
}
