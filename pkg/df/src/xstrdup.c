/*
	<dfc/xstrdup.c>

	df_xstrdup - strdup with xmalloc, never returns NULL.

	(C) 1999-2015 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#include <df/x.h>
#include <df/bt.h>
#include <string.h>		/* strlen, memcpy */


char* df_xstrdup(const char* src)
{
    DF_BT;

    return df_xmemdup(src, strlen(src) + 1);
}
