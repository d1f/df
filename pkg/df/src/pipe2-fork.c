#include <df/pipe-fork.h>
#include <df/bt.h>
#include <errno.h>
#include <unistd.h>    // pipe
#include <sys/types.h> // pid_t


// fork new process assigning descriptors of two communicating pipes
// for parent and child.
// returns pid from fork(2) or -1 on error.
int df_pipe2_fork(int *pipe_rd, int *pipe_wr)
{
    DF_BT;

    int err = 0;

    *pipe_wr  = -1;
    *pipe_rd  = -1;

    int parent_child[2] = { -1, -1 }; // parent -> child
    if (pipe(parent_child) < 0)
	return -1;

    int child_parent[2] = { -1, -1 }; // parent <- child
    if (pipe(child_parent) < 0)
	goto fail;

    pid_t pid = fork();
    if (pid < 0)
	goto fail;

    if (pid != 0)   // parent
    {
	close(parent_child[0]);     // read  end
	*pipe_wr = parent_child[1]; // write end

	close(child_parent[1]);     // write end
	*pipe_rd = child_parent[0]; // read  end
    }
    else            // child
    {
	close(child_parent[0]);     // read  end
	*pipe_wr = child_parent[1]; // write end

	close(parent_child[1]);     // write end
	*pipe_rd = parent_child[0]; // read  end
    }

    return pid;

fail:
    err = errno;

    close(parent_child[0]);
    close(parent_child[1]);
    close(child_parent[0]);
    close(child_parent[1]);

    errno = err;
    return -1;
}
