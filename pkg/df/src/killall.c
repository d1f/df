#include <df/killall.h>
#include <df/bt.h>

/*
 Got from
 http://jakash3.wordpress.com/2011/06/17/linux-kill-all-processes-with-name/
 */

#include <sys/types.h>
#include <signal.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// returns: -1 on error with errno or number of processes to be signaled
int killall(const char* name, int sig)
{
    DF_BT;

    int signaled = 0;

    pid_t p;
    char* s = malloc(264);
    if (s == NULL)
	return -1;

    char buf[128];
    DIR* d = opendir("/proc");
    if (d == NULL)
	goto out_free;

    FILE* st = NULL;
    struct dirent* f = NULL;

    while ((f = readdir(d)) != NULL)
    {
	if (f->d_name[0] == '.') continue;

	size_t i;
	for (i = 0; isdigit(f->d_name[i]); ++i)
	    ;
	if (i < strlen(f->d_name))
	    continue;

	strcpy(s, "/proc/");
	strcat(s, f->d_name);
	strcat(s, "/status");

	st = fopen(s, "r");
	if (st == NULL)
	    goto out_closedir;

	do
	{
	    if (fgets(buf, 128, st) == NULL)
		goto out_fclose;
	} while (strncmp(buf, "Name:", 5));

	fclose(st); st = NULL;

	for (i = 5; isspace(buf[i]); ++i)
	    ;

	*strchr(buf, '\n') = 0;
	if (!strcmp(&(buf[i]), name))
	{
	    sscanf(&(s[6]), "%d", &p);
	    int rc = kill(p, sig);
	    if (rc < 0)
		break;
	    ++signaled;
	}
    }

out_fclose:
    if (st != NULL)
	fclose(st);
out_closedir:
    if (d != NULL)
	closedir(d);
out_free:
    free(s);

    if (signaled > 0)
	return signaled;

    return -1;
}
