#define _GNU_SOURCE
#include <df/log.h>
#include <df/error.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h> // strrchr
#include <unistd.h> // STDOUT_FILENO


typedef struct cookie_s
{
    FILE *old_stream;
} cookie_t;

static ssize_t my_write(void *c, const char *buf, size_t size)
{
    //FIXME: dflog__() w/o nul-terminated string
    char b[size+1];
    memcpy(b, buf, size);
    b[size] = 0;

    dflog_(b); // append buf to dflog buffer

    if (strrchr(b, '\n') != NULL)
    {
	cookie_t *cookie = c;
	int level = fileno(cookie->old_stream) == STDERR_FILENO ? LOG_ERR : LOG_INFO;
	dflog_flush(level);
    }

    return size;
}

static cookie_io_functions_t funcs = { .write = my_write };
static cookie_t out_cookie,  err_cookie;
static FILE    *out_stream, *err_stream;

void dflog_stdio_hook(void)
{
    setvbuf(stdout, NULL, _IOLBF, 0);
    setvbuf(stderr, NULL, _IOLBF, 0);

    if (out_cookie.old_stream == NULL)
	out_cookie.old_stream = stdout;
    if (err_cookie.old_stream == NULL)
	err_cookie.old_stream = stderr;

    dflog_stdio_unhook();

    out_stream = fopencookie(&out_cookie, "a", funcs);
    if (out_stream == NULL)
	dferror(EXIT_FAILURE, errno, "fopencookie() for stdout failed");
    setvbuf(out_stream, NULL, _IOLBF, 0);

    err_stream = fopencookie(&err_cookie, "a", funcs);
    if (err_stream == NULL)
	dferror(EXIT_FAILURE, errno, "fopencookie() for stderr failed");
    setvbuf(err_stream, NULL, _IOLBF, 0);

    stdout = out_stream;
    stderr = err_stream;
}

void dflog_stdio_unhook(void)
{
    if (out_cookie.old_stream != NULL)
	stdout = out_cookie.old_stream;

    if (err_cookie.old_stream == NULL)
	stderr = err_cookie.old_stream;

    if (out_stream != NULL)
    {
	fclose(out_stream);
	out_stream = NULL;
    }

    if (err_stream != NULL)
    {
	fclose(err_stream);
	err_stream = NULL;
    }
}
