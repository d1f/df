#include <df/log.h>
#include "dflog-buf.h"
#include "df/log-buf-get-set.h" // dflog_buf_get_ dflog_buf_set_
#include <stdlib.h> // malloc free realloc
#include <string.h> // memset


#define DFLOG_BUF_INIT_SIZE (sizeof(dflog_buf_t))

dflog_buf_t *dflog_buf_create(void)
{
    size_t mem_size = sizeof(dflog_buf_t) + DFLOG_BUF_INIT_SIZE;

    dflog_buf_t *b = malloc(mem_size);
    if (b == NULL)
	return NULL;

    memset(b, 0, mem_size);

    char *buf = dflog_buf_buf(b);
    b->ptr  = buf;
    b->size = DFLOG_BUF_INIT_SIZE;

    return b;
}

void dflog_buf_delete(dflog_buf_t *b)
{
    free(b);
}

void dflog_free_buf(void)
{
    dflog_buf_t *b = dflog_buf_get_();
    dflog_buf_delete(b);
    dflog_buf_set_(NULL);
}


dflog_buf_t *dflog_buf_get(void)
{
    dflog_buf_t *b = dflog_buf_get_();

    if (b == NULL)
    {
	b = dflog_buf_create();
	dflog_buf_set_(b);
    }

    return b;
}

dflog_buf_t *dflog_buf_realloc_set(dflog_buf_t *b, size_t new_size)
{
    dflog_buf_t *nb = dflog_buf_realloc(b, new_size);
    if (nb != NULL)
	dflog_buf_set_(nb);
    return nb;
}

dflog_buf_t *dflog_buf_realloc(dflog_buf_t *b, size_t new_size)
{
    if (b == NULL)
    {
	b = dflog_buf_create();
	if (b == NULL)
	    return NULL;
    }

    if (new_size == 0)
	return b;

    if (dflog_buf_check(b))
	return NULL;

    if (b->size >= new_size)
	return b; // buf size does not shrinked

    size_t save_offset = b->ptr - dflog_buf_buf(b);

    dflog_buf_t *new_buf = realloc(b, sizeof(*new_buf) + new_size);
    if (new_buf == NULL)
	return NULL; // realloc failed, old buf is not touched or moved

    char *new_buf_buf = dflog_buf_buf(new_buf);
    new_buf->ptr  = new_buf_buf + save_offset;
    new_buf->size = new_size;

    return new_buf;
}

// return free space
size_t dflog_buf_space(dflog_buf_t *b)
{
    if (dflog_buf_check(b))
	return 0;

    return b->size - (b->ptr - dflog_buf_buf(b));
}

bool dflog_buf_check(dflog_buf_t *b)
{
    if (b == NULL)
	return true;

    if (b->ptr == NULL)
	return true;

    char *buf = dflog_buf_buf(b);
    if (buf == NULL)
	return true;

    if (b->ptr < buf)
	return true;

    if (b->ptr >= buf + b->size)
	return true;

    return false;
}
