#define _GNU_SOURCE

#include <df/x.h>
#include <df/error.h>
#include <df/bt.h>
#include <df/defs.h> // DF_MEMZERO

#include <signal.h>  // sigaction
#include <errno.h>   // errno
#include <string.h>  // strsignal
#include <stdlib.h>  // EXIT_FAILURE


void df_xsigact(int signum, int flags, void (*sa_h)(int))
{
    DF_BT;

    if (flags & SA_SIGINFO)
	dferror(EXIT_FAILURE, errno, "%s(): %s: SA_SIGINFO is not supported here",
		__func__, strsignal(signum));

    struct sigaction sigact;
    DF_MEMZERO(sigact);
    sigact.sa_handler = sa_h;
    sigact.sa_flags   = flags;

    int rc = sigaction(signum, &sigact, NULL);
    if (rc < 0)
	dferror(EXIT_FAILURE, errno, "sigaction failed for %s", strsignal(signum));
}
