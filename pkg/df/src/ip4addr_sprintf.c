#include <df/netaddr.h>
#include <df/endian.h>
#include <df/bt.h>
#include <stdio.h>
#include <string.h> // strlen, strcat

void df_ip4addr_sprintf(char out[DF_IP4ADDR_OUT_SIZE], df_ip4addr_t ip4addr, enum DF_IP4ADDR_ALIGN align)
{
    DF_BT;

    ip4addr = be32toh(ip4addr);
    switch (align)
    {
	case DF_IP4ADDR_NO_ALIGN:
	case DF_IP4ADDR_ALIGN_WHOLE:
	    sprintf(out, "%d.%d.%d.%d",
		    (ip4addr >> 24) & 0xff,
		    (ip4addr >> 16) & 0xff,
		    (ip4addr >>  8) & 0xff,
		    (ip4addr >>  0) & 0xff
		   );
	    break;
	case DF_IP4ADDR_ALIGN_OCTETS:
	    sprintf(out, "%3d.%3d.%3d.%3d",
		    (ip4addr >> 24) & 0xff,
		    (ip4addr >> 16) & 0xff,
		    (ip4addr >>  8) & 0xff,
		    (ip4addr >>  0) & 0xff
		   );
	    break;
    }

    if (align == DF_IP4ADDR_ALIGN_WHOLE)
    {
	size_t len = strlen(out);
	size_t i;
	for (i = len; i < DF_IP4ADDR_OUT_SIZE-1; ++i)
	    strcat(out+len, " ");
    }
}
