#include <df/key-val.h>
#include <df/bt.h>
#include <stdlib.h>  // NULL
#include <strings.h> // strcasecmp


// Linear case-insensitive search of key in keyvals table.
// Last entry in the table must have key == NULL.
// Returns found df_kv_t pointer or NULL.
const df_key_val_t *df_key_val_lsearch(const df_key_val_t keyvals[], const char *key)
{
    DF_BT;

    const df_key_val_t *kv = NULL;

    for (size_t i = 0; keyvals[i].key != NULL; ++i)
    {
	if (strcasecmp(key, keyvals[i].key) == 0)
	{
	    kv = &keyvals[i];
	    break;
	}
    }

    return kv;
}
