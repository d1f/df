#ifndef DF_FREE_LIST_INT_H
#define DF_FREE_LIST_INT_H

#include <df/free-list.h>
#include <df/types.h> // df_signature_t
#include <df-signatures.h>
#include <df/check.h>


struct df_free_list
{
    df_signature_t signature;
    void           *mem;
    df_free_list_t *next;
};


#endif //DF_FREE_LIST_INT_H
