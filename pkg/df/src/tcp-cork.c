#include <df/tcp-cork.h>
#include <df/bt.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <sys/socket.h> // setsockopt

int df_tcp_cork(int fd, int onoff)
{
    DF_BT;

    return setsockopt(fd, IPPROTO_TCP, TCP_CORK, &onoff, sizeof(onoff));
}
