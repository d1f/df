#include <df/netaddr.h>
#include <df/endian.h>
#include <df/bt.h>
#include <string.h> // ffs

// returns prefix length from subnet mask
size_t df_ip4addr_mask2pfxlen(df_ip4addr_t netmask)
{
    DF_BT;

    netmask = be32toh(netmask);
    int n = ffs(netmask);
    return n > 0 ? 32-(n-1) : 0;
}
