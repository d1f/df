#include <df/fd.h>
#include <df/bt.h>
#include <fcntl.h>


int df_fd_restore_flags(int fd, long flags) // returns df_fd_set_flags return if flags >= 0
{
    DF_BT;

    return flags >= 0 ? df_fd_set_flags(fd, flags) : flags;
}
