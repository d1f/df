#include <df/c-escape.h>
#include <df/bt.h>

/*
Escape		Hex value	Character represented
sequence	in ASCII
--------	---------	-----------------------------------------
\a		07		Alarm (Beep, Bell)
\b		08		Backspace
\f		0C		Formfeed
\n		0A		Newline (Line Feed); see notes below
\r		0D		Carriage Return
\t		09		Horizontal Tab
\v		0B		Vertical Tab
\\		5C		Backslash
\'		27		Single quotation mark
\"		22		Double quotation mark
\?		3F		Question mark
\e		1B		Escape character
\nnn		any		The character whose numerical value is given by nnn interpreted as an octal number
\xhh		any		The character whose numerical value is given by hh interpreted as a hexadecimal number
*/

// convert C escape sequences \C from sr to unescaped dst.
// dst size is not greater then src size.
void df_c_unescape(char *dst, const char *src)
{
    DF_BT;

    char c;
    while ( (c = *src++) != '\0' )
    {
	if (c == '\\')
	{
	    c = *src++;
	    if (c != '\0')
	    {
		char o = c;
		switch (c)
		{
		    case 'a' : o = '\a'; break;
		    case 'b' : o = '\b'; break;
		    case 'f' : o = '\f'; break;
		    case 'n' : o = '\n'; break;
		    case 'r' : o = '\r'; break;
		    case 't' : o = '\t'; break;
		    case 'v' : o = '\v'; break;
		    case '\\': o = '\\'; break;
		    case '\'': o = '\''; break;
		    case '\"': o = '\"'; break;
		    case '\?': o = '\?'; break;
		    case 'e' : o = 0x1B; break;
		    //FIXME: \nnn \xhh
		    default:   o = c;
		}
		*dst++ = o;
	    }
	    else
	    {
		break;
	    }
	} // c == '\\'
	else
	{
	    *dst++ = c;
	}
    } // loop

    *dst = 0;
}
