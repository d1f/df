#include <df/kv.h>
#include <df/bt.h>
#include <df/defs.h> // DF_MEMZERO
#include <df/x.h>

void df_kv_init0(df_kv_t *kv)
{
    DF_BT;

    DF_MEMZERO(*kv);
}

void df_kv_init(df_kv_t *kv, const df_item_t key, const df_item_t val)
{
    DF_BT;

    kv->key = key;
    kv->val = val;
}

df_kv_t *df_kv_create(const df_item_t key, const df_item_t val)
{
    DF_BT;

    df_kv_t *kv = df_xmalloc(sizeof(*kv));
    df_kv_init(kv, key, val);
    return kv;
}

void df_kv_delete(df_kv_t **kvp, df_kv_dtor_f dtor)
{
    DF_BT;

    if (kvp != NULL)
    {
	df_kv_t *kv = *kvp;
	if (kv != NULL && dtor != NULL)
	    dtor(kv);
	df_freep(kvp);
    }
}

// for [lb]search, qsort...
int df_kv_key_strcmp(const void *l, const void *r)
{
    DF_BT;

    return strcmp(((const df_kv_t*)l)->key.ccPtr, ((const df_kv_t*)r)->key.ccPtr);
}


// key, val are strings and strdup'ed. val maybe NULL
df_kv_t *df_kv_str_create(const char *key, const char *val)
{
    DF_BT;

    return df_kv_create(
			(df_item_t){ .cPtr = df_xstrdup(key) },
			(df_item_t){ .cPtr = val != NULL ? df_xstrdup(val) : NULL }
		       );
}

void df_kv_str_delete(df_kv_t **kvp)
{
    DF_BT;

    df_kv_delete(kvp, df_kv_str_dtor);
}

void df_kv_str_dtor(df_kv_t *kv)
{
    DF_BT;

    if (kv != NULL)
    {
	df_freep(&kv->val.cPtr);
	df_freep(&kv->key.cPtr);
    }
}
