#include <df/hash.h>
#include <df/hash-rand8.h>
#include <df/error.h>
#include <df/bt.h>
#include <df/x.h>        // df_xopen
#include <df/read-all.h> // df_read_all
#include <unistd.h>      // close
#include <df-signatures.h>


// XOR characters with random value.
// http://algolist.manual.ru/ds/s_has.php

static uint8_t rand8[UINT8_MAX+1];
static bool    rand8_inited = false;


static void rand8_fill_once(void)
{
    if (!rand8_inited)
    {
	static const char urandem_dev[] = "/dev/urandom";

	int fd = df_xopen(urandem_dev, O_RDONLY, 0);

	// returns -1 on error, 0 on EOF, >0 on success
	ssize_t rc = df_read_all(fd, rand8, sizeof(rand8));
	if (rc < 0)
	    dferror(EXIT_FAILURE, errno, "Can't read %zu bytes from %s",
		    sizeof(rand8), urandem_dev);
	if (rc == 0)
	    dferror(EXIT_FAILURE, errno, "Unexpected EOF on %s", urandem_dev);

	close(fd);

	rand8_inited = true;
    }
}

#define HASH() hash = rand8[ hash ^ *str++ ]

void df_hash_rand8_init(void *out)
{
    *(uint8_t*)out = 0;
}

void df_hash_rand8_len(void *out, const void *src, size_t len)
{
    DF_BT;

    rand8_fill_once();

    uint8_t hash = 0;

    for (const unsigned char *str = src; len > 0; --len)
	HASH();

    *(uint8_t*)out = hash;
}

void df_hash_rand8_str(void *out, const char *str)
{
    DF_BT;

    rand8_fill_once();

    uint8_t hash = 0;

    while (*str != 0)
	HASH();

    *(uint8_t*)out = hash;
}

const df_hash_alg_t df_hash_rand8_alg =
{
    .signature   = df_hash_alg_SIGNATURE,
    .bits        = 8,
    .buckets_max = UINT8_MAX+1,
    .hash_init   = df_hash_rand8_init,
    .hash_len    = df_hash_rand8_len,
    .hash_str    = df_hash_rand8_str
};
