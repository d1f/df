#define _GNU_SOURCE

#include <df/x.h>
#include <df/error.h>
#include <df/bt.h>

#include <signal.h> // sigaction
#include <errno.h>  // errno
#include <string.h> // strsignal
#include <stdlib.h> // EXIT_FAILURE

void df_xsigaction(int signum, const struct sigaction *act)
{
    DF_BT;

    int rc = sigaction(signum, act, NULL);
    if (rc < 0)
	dferror(EXIT_FAILURE, errno, "sigaction failed for %s", strsignal(signum));
}
