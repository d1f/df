/*
	<xmemdup.c>

	df_xmemdup - memcpy with xmalloc, never returns NULL.

	(C) 2013-2015 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#include <df/x.h>
#include <df/bt.h>
#include <string.h>		/* memcpy */


void* df_xmemdup(const void* src, size_t len)
{
    DF_BT;

    return memcpy(df_xmalloc(len), src, len);
}
