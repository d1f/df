#include <inttypes.h> // PRIu32
#include <df/hash.h>
#include <df/log.h>
#include <df/bt.h>
#include <df-signatures.h>
#include <df/check.h>


// out message via dflog_xbt() if check failed
void df_hash_alg_check(const df_flf_t *flf, const df_hash_alg_t *alg)
{
    DF_BT;

    df_check_null     (flf, "hash alg pointer", alg);
    df_check_signature(flf, "df_hash_alg", alg->signature, df_hash_alg_SIGNATURE);

    if (alg->bits < 1)
	dflog_xbt(flf, "alg->bits %zu < 1", alg->bits);

    df_check_null     (flf, "hash alg hash_init function pointer", alg->hash_init);
    df_check_null     (flf, "hash alg hash_len  function pointer", alg->hash_len);
    df_check_null     (flf, "hash alg hash_str  function pointer", alg->hash_str);
}
