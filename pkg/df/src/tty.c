/*
	<df/tty.h>

	getch(), kbhit() etc.

	(C) 1999-2016 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#include <df/tty.h>
#include <df/bt.h>
#include <unistd.h>	// isatty(), STDIN_FILENO
#include <stdio.h>	// fflush(stdout), EOF
#include <errno.h>	// errno
#include <termios.h>	// struct termios, tc?etattr()
#include <sys/time.h>	// struct timeval, select()
#include <string.h>	// memcpy


// set noncanonical input tty mode (w/o echo and <cr> waiting)
// returns 0 / -1 with errno from tcgetattr(), tcsetattr()
int df_tty_set_char_input_mode(int fd, int wait_flag,
			       struct termios* saved_tattr)
{
    DF_BT;

    struct termios tattr;
    int rc = 0;

    if (isatty(fd))
    {
	rc = tcgetattr(fd, &tattr);
	if (rc != 0) return rc;

	if (saved_tattr != NULL)
	    memcpy(saved_tattr, &tattr, sizeof(tattr));

	tattr.c_lflag    &= ~(ICANON|ECHO); // Clear ICANON and ECHO
	tattr.c_cc[VMIN ] = wait_flag?1:0;
	tattr.c_cc[VTIME] = 0;

	rc = tcsetattr(fd, TCSANOW, &tattr);
    }

    return rc;
}


// restore saved tty modes
// returns 0 / -1 with errno from tcsetattr()
int df_tty_restore_attr(int fd, struct termios* saved_tattr)
{
    DF_BT;

    return isatty(fd) ? tcsetattr(fd, TCSANOW, saved_tattr) : 0;
}


// returns:
//	 0 - there are no characters in input queue;
//	>0 - there are characters in input queue;
//	<0 - error, see errno
int df_tty_kbhit(int fd)
{
    DF_BT;

    struct termios saved_tattr;
    fd_set fdset;
    struct timeval tv  = { 0, 0 };
    int rc;

    rc = df_tty_set_char_input_mode(fd, 0, &saved_tattr);
    if (rc != 0) return rc;

    FD_ZERO(&fdset); FD_SET(fd, &fdset);

    rc = select(fd+1, &fdset, NULL, NULL, &tv);

    df_tty_restore_attr(fd, &saved_tattr);
    return rc;
}


// main routine
int df_tty_fcheck_key(int fd, int wait_flag)
{
    DF_BT;

    struct termios saved_tattr;
    char buf[16];

    int rc = df_tty_set_char_input_mode(fd, wait_flag, &saved_tattr);
    if (rc != 0) return rc;

    ssize_t l = read(fd, &buf, isatty(fd) ? sizeof(buf) : 1);
    if (l == -1)	{ rc = -1; goto restore; } // error

    if (l == 0)
    {
	if (wait_flag)	{ errno=0; rc = -1; goto restore; }
	else		{          rc =  0; goto restore; } // no wait && no key
    }
    else
	rc = buf[0] & 0xFF;

restore:
    df_tty_restore_attr(fd, &saved_tattr);
    return rc;
}

int df_tty_check_key(int wait_flag)
{
    DF_BT;

    fflush(stdout);
    return df_tty_fcheck_key(STDIN_FILENO, wait_flag);
}
