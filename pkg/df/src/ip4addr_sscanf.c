#include <df/netaddr.h>
#include <df/endian.h>
#include <df/bt.h>
#include <stdio.h>

int df_ip4addr_sscanf(const char *in, df_ip4addr_t *ip4addr)
{
    DF_BT;

    unsigned char xx[4];
    int rc = sscanf(in, "%3hhd.%3hhd.%3hhd.%3hhd",
		    &xx[0], &xx[1], &xx[2], &xx[3] );
    if (rc != 4)
    {
	*ip4addr = INADDR_ANY;
	return -1;
    }

    *ip4addr =
	((df_ip4addr_t)xx[0] << 24) |
	((df_ip4addr_t)xx[1] << 16) |
	((df_ip4addr_t)xx[2] <<  8) |
	((df_ip4addr_t)xx[3]);

    *ip4addr = htobe32(*ip4addr);

    return 0;
}
