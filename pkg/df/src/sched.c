#include <df/sched.h>
#include <df/error.h>
#include <df/bt.h>


int df_sched_set(pid_t pid, int policy, int priority, df_sched_param_t *save)
{
    DF_BT;

    int err = 0;

    if (save != NULL)
    {
	save->pid = pid;

	save->policy = sched_getscheduler(pid); // get policy
	if (save->policy < 0)
	{
	    err = errno;
	    dferror(EXIT_SUCCESS, err, "Can't get current scheduler policy");
	    return -err;
	}

	if (sched_getparam(pid, &save->param) < 0)
	{
	    err = errno;
	    dferror(EXIT_SUCCESS, err, "Can't get current scheduler priority");
	    return -err;
	}
    }

    struct sched_param param = { .sched_priority = priority };
    if (sched_setscheduler(pid, policy, &param) < 0)
    {
	err = errno;
	dferror(EXIT_SUCCESS, err, "Can't set scheduler policy/priority");
	return -err;
    }

    return 0;
}

int df_sched_restore(const df_sched_param_t *save)
{
    DF_BT;

    if (sched_setscheduler(save->pid, save->policy, &save->param) < 0)
    {
	int err = errno;
	dferror(EXIT_SUCCESS, err, "Can't set scheduler policy/priority");
	return -err;
    }

    return 0;
}
