#include <df/netaddr.h>
#include <df/bt.h>
#include <stdio.h>

void df_ethaddr_fprintf(FILE* out, df_ethaddr_t ethaddr)
{
    DF_BT;

    char str[DF_ETHADDR_OUT_SIZE];
    df_ethaddr_sprintf(str, ethaddr);
    fputs(str, out);
}
