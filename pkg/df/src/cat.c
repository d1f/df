#define _GNU_SOURCE

#include <df/cat.h>
#include <df/error.h>
#include <df/write-all.h>
#include <df/bt.h>
#include <sys/mman.h> // mmap
#include <sys/stat.h> // fstat
#include <fcntl.h>    // open
#include <unistd.h>   // close


#ifndef  O_NOATIME
# define O_NOATIME 0
#endif

int df_cat(int outfd, const char *file)
{
    DF_BT;

    int fd = open(file, O_RDONLY | O_NOATIME | O_NOCTTY | O_LARGEFILE);
    if (fd < 0)
    {
	dferror(EXIT_SUCCESS, errno, "Can't open %s", file);
	return fd;
    }

    int ret = 0;
    void *map = MAP_FAILED;

    struct stat sb;
    if (fstat(fd, &sb) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): fstat on %s failed", __func__, file);
	ret = -1;
	goto out;
    }

    map = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (map == MAP_FAILED)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): mmap on %s failed", __func__, file);
	ret = -1;
	goto out;
    }

    ssize_t wrc = df_write_all(outfd, map, sb.st_size);
    if (wrc < 0)
    {
	dferror(EXIT_SUCCESS, errno, "Can't write to stdout");
	ret = -1;
    }

out:
    if (map != MAP_FAILED)
	munmap(map, sb.st_size);
    close(fd);
    return ret;
}
