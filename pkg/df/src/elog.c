#include <df/elog.h>
#include <df/fd.h>
#include <df/whitespace.h> // df_strip_tail_whitespace
#include <stdio.h>      // STDERR_FILENO
#include <stdbool.h>
#include <string.h>     // strchr strerror
#include <errno.h>
#include <fcntl.h>      // open
#include <paths.h>      // _PATH_CONSOLE
#include <unistd.h>     // isatty
#include <time.h>       // time strftime localtime
#include <sys/syslog.h> // LOG_*
#include <sys/socket.h>
#include <sys/un.h>     // struct sockaddr_un


enum { ELOG_STDERR = 1, ELOG_SYS = 2, ELOG_CONS = 4 };

static void df_velog_(int channels, const char *format, va_list ap);


void df_elog(const char *format, ...)
{
    va_list ap;

    va_start(ap, format);
    df_velog_(~0, format, ap);
    va_end  (ap);
}

void df_velog(const char *format, va_list ap)
{
    df_velog_(~0, format, ap);
}

static void df_elog_(int channels, const char *format, ...)
{
    va_list ap;

    va_start(ap, format);
    df_velog_(channels, format, ap);
    va_end  (ap);
}


static void norn(char *s) // replace \r, \n by space
{
    for ( ; *s != 0; ++s)
	if (*s == '\n' || *s == '\r')
	    *s = ' ';
}

extern char *__progname; // glibc global variable
static bool stderr_checked = false, stderr_opened = false;
static int cons_fd = -1, syslog_fd = -1;
static struct sockaddr_un syslog_addr;	// AF_UNIX address of local logger

static void df_elog_sys_close(void)
{
    if (syslog_fd >= 0)
    {
	close(syslog_fd);
	syslog_fd = -1;
    }
}

static void df_elog_sys_open(void)
{
    if (syslog_fd < 0)
    {
	syslog_addr.sun_family = AF_UNIX;
	strncpy(syslog_addr.sun_path, _PATH_LOG, sizeof(syslog_addr.sun_path));

	syslog_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (syslog_fd < 0)
	{
	    df_elog_(ELOG_STDERR | ELOG_CONS,
		     "%s(): Can't create socket: %m\n", __func__);
	    return;
	}

	fcntl(syslog_fd, F_SETFD, FD_CLOEXEC);

	if (connect(syslog_fd, (const struct sockaddr*)&syslog_addr, sizeof(syslog_addr)) < 0)
	{
	    df_elog_(ELOG_STDERR | ELOG_CONS,
		     "%s(): Can't connect socket to %s: %m\n", __func__, _PATH_LOG);
	    df_elog_sys_close();
	    return;
	}
    }
}

typedef struct buf_s
{
    char  buf[256 + 128];
    char *end;
    char *syslog;
    char *misc;
} buf_t;

static void buf_init(buf_t *buf)
{
    memset(buf, 0, sizeof(*buf));
    buf->end = buf->buf;
}

extern inline size_t buf_size(const buf_t *buf)
{
    return sizeof(buf->buf) - 1/* NUL */ - 2/* \r\n */;
}

static inline size_t buf_rest(const buf_t *buf)
{
    return buf_size(buf) - (buf->end - buf->buf);
}

static void df_velog_(int channels, const char *format, va_list ap)
{
    buf_t buf;
    buf_init(&buf);

    buf.syslog = buf.buf;
    buf.end += sprintf(buf.syslog, "<%d>", LOG_USER | LOG_ERR);

    time_t now = time(NULL);
    struct tm tm;
    buf.end +=  strftime(buf.end, buf_rest(&buf),
			 "%h %e %T ", localtime_r(&now, &tm));

    buf.misc = buf.end;

    buf.end += snprintf(buf.misc, buf_rest(&buf),
			"%s[%d]: ", __progname, getpid());

    vsnprintf(buf.end, buf_rest(&buf), format, ap);

    norn(buf.buf); // replace \r, \n by space
    df_strip_tail_whitespace(buf.buf); // chop
    buf.end = strchr(buf.buf, 0); // new NUL terminator

    if ( ! stderr_checked )
    {
	stderr_opened  = df_is_fd_opened(STDERR_FILENO);
	stderr_checked = true;
    }

    if (stderr_opened && (channels & ELOG_STDERR))
    {
	buf.end[0] = '\n';
	buf.end[1] = 0;

	if (write(STDERR_FILENO, buf.misc, buf.end - buf.misc + 1) < 0)
	    df_elog_(ELOG_SYS | ELOG_CONS,
		     "%s(): Can't write to stderr: %m\n", __func__);
    }

    if (channels & ELOG_SYS)
    {
	df_elog_sys_open();

	if (syslog_fd >= 0)
	{
	    if (send(syslog_fd, buf.syslog, buf.end - buf.syslog, MSG_NOSIGNAL) < 0)
	    {
		df_elog_(ELOG_STDERR | ELOG_CONS,
			 "%s(): Can't send to syslog: %m\n", __func__);
		df_elog_sys_close();	// attempt re-open next time
	    }
	}
    }

    if (channels & ELOG_CONS)
    {
	if (cons_fd < 0)
	{
	    cons_fd = open(_PATH_CONSOLE, O_WRONLY|O_NOCTTY, 0);

	    if (cons_fd < 0)
	    {
		if (errno != EPERM && errno != EACCES)
		    df_elog_(ELOG_STDERR | ELOG_SYS,
			     "%s(): Can't open %s: %m\n", __func__, _PATH_CONSOLE);
	    }
	}

	if (cons_fd >= 0)
	{
	    buf.end[0] = '\r';
	    buf.end[1] = '\n';
	    buf.end[2] = 0;

	    if (write(STDERR_FILENO, buf.misc, buf.end - buf.misc + 2) < 0)
	    {
		df_elog_(ELOG_SYS | ELOG_STDERR,
			 "%s(): Can't write to %s: %m\n", __func__, _PATH_CONSOLE);

		close(cons_fd);
		cons_fd = -1; // re-open next time
	    }
	}
    }
}
