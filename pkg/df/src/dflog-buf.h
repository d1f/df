#ifndef  DF_LOG_BUF_H
# define DF_LOG_BUF_H

#include <stddef.h> // size_t
#include <stdbool.h>

#include "df/log-buf-get-set.h" // dflog_buf_t dflog_buf_get_ dflog_buf_set_


struct dflog_buf_s
{
    char *ptr;   // moving pointer inside buf
    size_t size; // buf size without this header
};


dflog_buf_t *dflog_buf_create(void);
dflog_buf_t *dflog_buf_realloc(dflog_buf_t *b, size_t new_size);
void         dflog_buf_delete (dflog_buf_t *b);

size_t       dflog_buf_space  (dflog_buf_t *b);

dflog_buf_t *dflog_buf_get(void);

static inline
char *dflog_buf_buf(dflog_buf_t *b)
{
    return b==NULL ? NULL : (char*)(b + 1);
}

dflog_buf_t *dflog_buf_realloc_set(dflog_buf_t *b, size_t new_size);

bool dflog_buf_check(dflog_buf_t *b);


#endif //DF_LOG_BUF_H
