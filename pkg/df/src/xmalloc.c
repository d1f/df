/*
	<df/x.h>

	Wrappers with exit on failure, never returns eror.

	(C) 1999-2015 Dmitry A. Fedorov <dm.fedorov@gmail.com>
	Copying policy: GNU LGPL
*/

#include <df/x.h>
#include <df/elog.h>
#include <df/bt.h>
#include <stdlib.h> // exit EXIT_FAILURE
#include <string.h> // memset


void* df_xmalloc(size_t size)
{
    DF_BT;

    if (size == 0)
	size =  1;

    void* ptr = malloc(size);
    if (ptr == NULL)
    {
	df_elog("%s(): %m, exiting", __func__);
	df_bt_exit(EXIT_FAILURE);
    }

    return ptr;
}

void* df_xrealloc(void* oldptr, size_t size)
{
    DF_BT;

    if (size == 0)
	size =  1;

    void* ptr = realloc(oldptr, size);
    if (ptr == NULL)
    {
	df_elog("%s(): %m, exiting", __func__);
	df_bt_exit(EXIT_FAILURE);
    }

    return ptr;
}

void *df_xzmalloc(size_t size)
{
    DF_BT;

    void *ret = df_xmalloc(size);
    memset(ret, 0, size);

    return ret;
}
