#define _GNU_SOURCE
#include <df/bt.h>
#include <df/log.h>
#include <df/x.h>    // df_si_code_str
#include <df/defs.h> // DF_MEMZERO
#include <stddef.h>  // NULL
#include <stdlib.h>  // EXIT_SUCCESS
#include <string.h>  // strsignal


void df_bt_foreach(df_bt_f f)
{
    const df_bt_t *btp = NULL;
    for (btp = df_bt_last_get(); btp != NULL; btp = btp->prev)
    {
	f(btp->func);
    }
}


static void log_func(const char * const func)
{
    dflog(LOG_ERR, "\t%s()", func);
}

void df_bt(void)
{
    dflog(LOG_ERR, "Backtrace:");
    df_bt_foreach(log_func);
}

// df_bt with exit
void df_bt_exit(int status)
{
    df_bt();
    exit(status);
}

// df_bt with abort and signal name
void df_bt_abort(int sig)
{
    dflog(LOG_ERR, "Signal %d (%s) catched, backtrace:", sig, strsignal(sig));
    df_bt_foreach(log_func);

    abort();
}

void df_bt_siginfo_handler(int sig, siginfo_t *si, void *ucontext)
{
    DF_BT;

    (void)ucontext; // see getcontext(2)

    dflog(LOG_ERR, "Signal %d (%s) catched. errno: %d (%s), code: %d (%s)",
	  sig, strsignal(sig),
	  si->si_errno, strerror(si->si_errno),
	  si->si_code, df_si_code_str(si->si_code, sig));

    switch (sig)
    {
	case SIGILL: case SIGFPE: case SIGSEGV: case SIGBUS: case SIGTRAP:
#ifdef si_trapno
	    dflog(LOG_ERR, "\taddr: %p, trap: %d", si->si_addr, si->si_trapno);
#else
	    dflog(LOG_ERR, "\taddr: %p", si->si_addr);
#endif
	    break;
    }

    df_bt();
    signal(SIGABRT, SIG_DFL);
    abort();
}

void df_bt_xset_signals(void (*sih)(int sig, siginfo_t *si, void *ucontext))
{
    DF_BT;

    struct sigaction sa; DF_MEMZERO(sa);

    sa.sa_sigaction = sih;
    sa.sa_flags     = SA_SIGINFO | SA_NODEFER | SA_RESETHAND;

    df_xsigaction(SIGBUS   , &sa); // Bus error (bad memory access)
#ifdef SIGEMT
    df_xsigaction(SIGEMT   , &sa);
#endif
    df_xsigaction(SIGFPE   , &sa); // Floating point exception
    df_xsigaction(SIGILL   , &sa); // Illegal Instruction
#ifdef SIGINFO
    df_xsigaction(SIGINFO  , &sa); // A synonym for SIGPWR
#endif
#ifdef SIGIOT
    df_xsigaction(SIGIOT   , &sa); // IOT trap. A synonym for SIGABRT
#endif
    df_xsigaction(SIGPWR   , &sa); // Power failure (System V)
    df_xsigaction(SIGSEGV  , &sa); // Invalid memory reference
#ifdef SIGSTKFLT
    df_xsigaction(SIGSTKFLT, &sa); // Stack fault on coprocessor (unused)
#endif
    df_xsigaction(SIGSYS   , &sa); // Bad argument to routine (SVr4)
    df_xsigaction(SIGTRAP  , &sa); // Trace/breakpoint trap
#ifdef SIGUNUSED
    df_xsigaction(SIGUNUSED, &sa); // Synonymous with SIGSYS
#endif
    df_xsigaction(SIGXCPU  , &sa); // CPU time limit exceeded (4.2BSD)
    df_xsigaction(SIGXFSZ  , &sa); // File size limit exceeded (4.2BSD)
}
