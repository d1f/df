#include <df/fd.h>  // df_is_fd_opened
#include <df/bt.h>
#include <errno.h>
#include <unistd.h> // isatty

bool df_is_fd_opened(int fd)
{
    DF_BT;

    errno = 0;
    isatty(fd);

    return !(errno == EBADF);
}
