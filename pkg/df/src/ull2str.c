#include <df/ull2str.h>
#include <df/x.h>     // df_xmalloc
#include <df/bt.h>
#include <stdio.h>    // sprintf


const char *df_ull2str(ullong num, char sep, df_free_list_t **flp)
{
    DF_BT;

    unsigned short rem[8];

    int i = 0;

    for (i = 0; num != 0ull; ++i, num /= 1000)
	rem[i] = num % 1000;

    char *str = df_xmalloc(i*4+1);
    df_free_list_add(flp, str);

    char *s   = str;
    --i;
    size_t len = sprintf(s, "%hu", rem[i--]);
    s += len;
    for  (; i >= 0; --i, s += len)
	len = sprintf(s, "%c%03hu", sep, rem[i]);

    return str;
}
