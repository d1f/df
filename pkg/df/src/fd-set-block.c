#include <df/fd.h>
#include <df/error.h>
#include <df/bt.h>
#include <fcntl.h>


long df_fd_set_block(int fd) // returns old flags or -1 on error
{
    DF_BT;

    long flags = df_fd_get_flags(fd);
    if  (flags >= 0)
    {
	int rc = df_fd_set_flags(fd, flags & ~O_NONBLOCK);
	if (rc < 0)
	    flags = rc;
    }

    return flags;
}
