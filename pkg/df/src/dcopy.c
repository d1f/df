#include <df/dcopy.h>
#include <df/error.h>
#include <df/write-all.h>
#include <df/bt.h>
#include <unistd.h> // read


int df_dcopy(int infd, int outfd, size_t buf_size)
{
    DF_BT;

    if (buf_size == 0)
	buf_size = 4096;

    char buf[buf_size];

    do
    {
	ssize_t rdc = read(infd, buf, buf_size);
	if (rdc < 0)
	{
	    dferror(EXIT_SUCCESS, errno, "%s(): read error", __func__);
	    return -1;
	}
	else if (rdc == 0) // EOF
	{
	    return 0;
	}

	// rdc > 0

	ssize_t wrc = df_write_all(outfd, buf, rdc);
	if (wrc < 0)
	{
	    dferror(EXIT_SUCCESS, errno, "%s(): write error", __func__);
	    return -1;
	}
    } while(1);

    return 0;
}
