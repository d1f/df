#include <df/netaddr.h>
#include <df/endian.h>
#include <df/bt.h>
#include <stdio.h>

void df_ethaddr_sprintf(char out[DF_ETHADDR_OUT_SIZE], df_ethaddr_t ethaddr)
{
    DF_BT;

    ethaddr = be64toh(ethaddr);
    sprintf(out, "%02x:%02x:%02x:%02x:%02x:%02x",
	    (int) ((ethaddr >> 40) & 0xff),
	    (int) ((ethaddr >> 32) & 0xff),
	    (int) ((ethaddr >> 24) & 0xff),
	    (int) ((ethaddr >> 16) & 0xff),
	    (int) ((ethaddr >>  8) & 0xff),
	    (int) ((ethaddr >>  0) & 0xff)
	   );
}
