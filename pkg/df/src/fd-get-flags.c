#include <df/fd.h>
#include <df/error.h>
#include <df/bt.h>
#include <fcntl.h>

long df_fd_get_flags(int fd) // returns flags or -1 on error with error message
{
    DF_BT;

    long flags = fcntl(fd, F_GETFL);
    if (flags < 0)
	dferror(EXIT_SUCCESS, errno, "%s(): fcntl F_GETFL failed", __func__);

    return flags;
}
