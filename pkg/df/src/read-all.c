#include <df/read-all.h>
#include <df/bt.h>
#include <errno.h>
#include <sched.h> // sched_yield
#include <unistd.h> // read


// returns -1 on error, 0 on EOF, >0 on success
ssize_t df_read_all(int fd, void *_buf, size_t count)
{
    DF_BT;

    if (count == 0)
    {
	errno = EINVAL;
	return -1;
    }

    char* buf = _buf;
    int first_run = 1;
    size_t rcount = 0;

    do
    {
	ssize_t rc = read(fd, buf, count);
	if (rc < 0)
	{
#if defined EWOULDBLOCK && EWOULDBLOCK != EAGAIN
	    if ( errno == EAGAIN || errno == EWOULDBLOCK )
#else
	    if ( errno == EAGAIN )
#endif
		continue;
	    else
		return rc;
	}

	if (rc == 0)
	    return 0; //EOF

	 count -= rc;
	rcount += rc;
	buf    += rc;

	if (!first_run) sched_yield();
	first_run = 0;

    } while (count > 0);

    return rcount;
}
