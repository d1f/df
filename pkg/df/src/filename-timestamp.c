#include <df/filename-timestamp.h>
#include <df/bt.h>
#include <stdio.h>    // snprintf
#include <time.h>     // localtime_r


void df_filename_timestamp(char out[NAME_MAX],
			   const char *prefix,      // NULL allowed
			   const char *suffix,      // NULL allowed
			   const size_t *seqnum,    // NULL allowed
			   const struct timeval *tv // calls gettimeofday() if NULL
			  )
{
    DF_BT;

    struct timeval mytv;
    if (tv == NULL)
    {
	gettimeofday(&mytv, NULL);
	tv = &mytv;
    }

    struct tm tm;
    localtime_r(&tv->tv_sec, &tm);

    char num[20];
    if (seqnum != NULL)
	sprintf(num, "%010zu_", *seqnum);
    else
	num[0] = 0;

    snprintf(out, NAME_MAX,
	     "%s%s%04d-%02d-%02d_%02d-%02d-%02d-%03ld%s",
	     prefix?prefix:"",
	     num,
	     tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday,
	     tm.tm_hour, tm.tm_min, tm.tm_sec,
	     (tv->tv_usec+500)/1000,
	     suffix?suffix:"");
}
