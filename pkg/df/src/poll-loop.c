#include <df/poll-loop.h>
#include <df/time.h> // df_gettimeofday
#include <df/bt.h>
#include <unistd.h>  // usleep


// returns: <0: fatal, end loop; 0: fail, timeout; >0: ok
int df_poll_loop(df_poll_f pf, const void *data,
		 size_t interval_msec, size_t timeout_msec)
{
    DF_BT;

    suseconds_t interval_usec = interval_msec * 1000;

    long long end_time = df_gettimeofday() + timeout_msec * 1000;

    int rc = 0;
    do
    {
	rc = pf(data);
	if (rc != 0)
	    return rc;

	usleep(interval_usec);

	rc = pf(data);
	if (rc != 0)
	    return rc;

    } while (df_gettimeofday() < end_time);

    return 0; // timeout
}
