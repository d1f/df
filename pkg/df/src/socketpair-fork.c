#include <df/pipe-fork.h>
#include <df/bt.h>
#include <unistd.h>     // pipe
#include <sys/types.h>  // pid_t
#include <sys/socket.h> // socketpair


// fork new process assigning one of communicating sockets from pair
// to each of processes for parent and child.
// parent and child pointers may be NULL.
// returns pid from fork(2) or -1 on error.
int df_socketpair_fork(int *parent, int *child)
{
    DF_BT;

    pid_t pid  = -1;
    if  (parent != NULL)
	*parent  = -1;
    if  (child  != NULL)
	*child   = -1;

    int pair[2] = { -1, -1 };
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, pair) < 0)
	return -1;

    pid = fork();
    if (pid < 0)
	;
    else if (pid != 0) // parent
    {
	close(pair[1]);
	if  (parent != NULL)
	    *parent =  pair[0];
    }
    else               // child
    {
	close(pair[0]);
	if  (child != NULL)
	    *child =  pair[1];
    }

    return pid;
}
