#include <df/netaddr.h>
#include <df/endian.h>
#include <df/bt.h>
#include <stdio.h>

int df_ethaddr_sscanf(const char *in, df_ethaddr_t *ethaddr)
{
    DF_BT;

    unsigned char xx[6];
    int rc = sscanf(in, "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx",
		    &xx[0], &xx[1], &xx[2], &xx[3], &xx[4], &xx[5] );
    if (rc != 6)
	return -1;

    *ethaddr =
	((df_ethaddr_t)xx[0] << 40) |
	((df_ethaddr_t)xx[1] << 32) |
	((df_ethaddr_t)xx[2] << 24) |
	((df_ethaddr_t)xx[3] << 16) |
	((df_ethaddr_t)xx[4] <<  8) |
	((df_ethaddr_t)xx[5]);

    *ethaddr = htobe64(*ethaddr);

    return 0;
}
