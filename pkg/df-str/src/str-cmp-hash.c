#include <int/str.h>

int df_str_cmp_hash(const df_str_t *l, const df_str_t *r)
{
    DF_BT;

    df_ap_hash_t lhash = df_str_hash(l);
    df_ap_hash_t rhash = df_str_hash(r);

    if (lhash == rhash)
	return 0;
    else if (lhash < rhash)
	return -1;

    return 1;
}
