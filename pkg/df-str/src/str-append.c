#include <int/str.h>
#include <string.h> // strlen

void df_str_append(df_str_t *s, const char *src) // src may be NULL
{
    DF_BT;

    CHECK_SIGNATURE(s);

    if (src == NULL)
	return;

    df_mem_chop_nul(s->m);
    df_mem_append_data(s->m, src, strlen(src) + 1);

    s->hashp->hashed = false;
}
