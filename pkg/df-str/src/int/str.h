#ifndef  DF_STR_INT_H
# define DF_STR_INT_H

# include <df/str.h>
# include <df/mem.h>
# include <df/x.h>
# include <df/bt.h>
# include <df/check.h>
# include <df-str-signatures.h>


typedef struct
{
    df_ap_hash_t hash;
    bool hashed;
} df_str_hash_t;

struct df_str
{
    df_signature_t signature;

    df_mem_t *m;
    df_str_hash_t *hashp; // is pointer to hash on const df_str_t*
};


#define CHECK_NULL(s) DF_CHECK_NULL("df_str_t pointer", s)
#define CHECK_SIGNATURE(s) DF_CHECK_SIGNATURE(df_str, s)


#endif //DF_STR_INT_H
