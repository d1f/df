#include <int/str.h>

const char *df_str_get(df_str_t const  *s)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    return df_mem_data(s->m);
}
