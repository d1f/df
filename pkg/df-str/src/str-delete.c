#include <int/str.h>

void df_str_delete(df_str_t **sp) // sp, *sp may be NULL
{
    DF_BT;

    if (sp == NULL)
	return;

    df_str_t *s = *sp;
    if (s == NULL)
	return;

    CHECK_SIGNATURE(s);

    df_mem_free(s->m);
    free(s->hashp);

    memset(s, ~0, sizeof(*s));
    df_freep(sp);
}
