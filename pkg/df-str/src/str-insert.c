#include <int/str.h>

void df_str_insert_at(df_str_t *s, size_t offset, const char *src)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    if (src != NULL)
    {
	df_mem_insert_at(s->m, offset, src, strlen(src));
	df_mem_chop_nul(s->m);
	df_mem_append_char(s->m, '\0');
	s->hashp->hashed = false;
    }
}
