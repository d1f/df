#include <int/str.h>
#include <df/memdup.h>

char *df_str_dup(df_str_t const *s)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    return df_mem_dup(s->m);
}
