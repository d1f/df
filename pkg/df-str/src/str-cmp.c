#include <int/str.h>

int df_str_cmp(const df_str_t *l, const df_str_t *r)
{
    DF_BT;

    if (df_str_hash(l) == df_str_hash(r))
	return 0;

    return strcmp(df_mem_data(l->m), df_mem_data(r->m));
}
