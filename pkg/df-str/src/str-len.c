#include <int/str.h>

size_t df_str_len(df_str_t const  *s)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    size_t size = df_mem_size(s->m);
    return size <= 1 ? 0 : size - 1;
}
