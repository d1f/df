#ifndef  DF_STR_H
# define DF_STR_H

# include <stddef.h>  // size_t NULL
# include <df/defs.h> // ATTR, returns_nonnull
# include <df/ap-hash.h>

        struct df_str;
typedef struct df_str df_str_t;


df_str_t    *df_str_create     (char const *src)  /* src  may be NULL */    ATTR(returns_nonnull);
df_str_t    *df_str_create_take(char      **srcp) /* srcp may be NULL */    ATTR(returns_nonnull);

void         df_str_delete     (df_str_t **sp)    /* sp, *sp may be NULL */;
void         df_str_free       (df_str_t  *s)                                ATTR(nonnull);

size_t       df_str_len        (df_str_t const *s)                           ATTR(nonnull);
df_ap_hash_t df_str_hash       (df_str_t const *s)                           ATTR(nonnull);
const char  *df_str_get        (df_str_t const *s)                           ATTR(nonnull,returns_nonnull);
void         df_str_set        (df_str_t       *s, const char *src)          ATTR(nonnull(1));

void         df_str_append_char(df_str_t *s, char c)                         ATTR(nonnull);
void         df_str_append     (df_str_t *s, const char *src)                ATTR(nonnull(1));
void         df_str_insert_at  (df_str_t *s, size_t offset, const char *src) ATTR(nonnull(1));
void         df_str_cut_at     (df_str_t *s, size_t offset, size_t size)     ATTR(nonnull);

char        *df_str_dup        (df_str_t const *s)                           ATTR(nonnull,returns_nonnull);
char        *df_str_take       (df_str_t      **sp)                          ATTR(nonnull,returns_nonnull);
int          df_str_cmp        (df_str_t const *l, df_str_t const *r)        ATTR(nonnull);
int          df_str_cmp_hash   (df_str_t const *l, df_str_t const *r)        ATTR(nonnull);


#endif //DF_STR_H
