#include <int/str.h>


df_str_t *df_str_create(char const *src) /* src may be NULL */
{
    DF_BT;

    df_str_t *str  = df_xmalloc(sizeof(df_str_t));
    str->signature = df_str_SIGNATURE;
    if (src == NULL)
    {
	str->m   = df_mem_create(1, NULL);
	char *cp = df_mem_data(str->m);
	*cp = 0;
    }
    else
    {
	str->m   = df_mem_create(strlen(src)+1, src);
    }

    str->hashp = df_xzmalloc(sizeof(*str->hashp));

    return str;
}

df_str_t *df_str_create_take(char **srcp) /* srcp may be NULL */
{
    DF_BT;

    df_str_t *str  = df_str_create(NULL); // create empty string

    if (srcp != NULL)
    {
	if (*srcp != NULL)
	{
	    df_mem_delete(&str->m);
	    str->m = df_mem_create_take(strlen(*srcp)+1, (void**)srcp);
	    df_str_hash(str);
	}
    }

    return str;
}
