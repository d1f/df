#include <int/str.h>

void df_str_set(df_str_t *s, const char *src) // src may be NULL
{
    DF_BT;

    CHECK_SIGNATURE(s);

    size_t len = src == NULL ? 0 : strlen(src);
    df_mem_realloc(s->m, len + 1);

    memcpy(df_mem_data(s->m), src, len + 1);

    s->hashp->hashed = false;
}
