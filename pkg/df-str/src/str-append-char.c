#include <int/str.h>

void df_str_append_char(df_str_t *s, char c)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    df_mem_chop_nul(s->m);
    df_mem_append_char(s->m, c);
    if (c != '\0')
	df_mem_append_char(s->m, '\0');
    s->hashp->hashed = false;
}
