#include <int/str.h>

void df_str_free(df_str_t *s)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    df_mem_free(s->m);

    s->hashp->hashed = false;
}
