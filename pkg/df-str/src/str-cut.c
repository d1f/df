#include <int/str.h>

void df_str_cut_at(df_str_t *s, size_t offset, size_t size)
{
    DF_BT;

    CHECK_SIGNATURE(s);

    df_mem_cut_at(s->m, offset, size);

    df_mem_chop_nul(s->m);
    df_mem_append_char(s->m, '\0');
    s->hashp->hashed = false;
}
