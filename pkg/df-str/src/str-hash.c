#include <int/str.h>

df_ap_hash_t df_str_hash(df_str_t const *s)
{
    CHECK_SIGNATURE(s);

    if (s->hashp->hashed)
	return s->hashp->hash;

    df_ap_hash(&s->hashp->hash, df_mem_data(s->m), df_str_len(s));

    return s->hashp->hash;
}
