#include <int/str.h>

char *df_str_take(df_str_t **sp)
{
    DF_BT;

    df_str_t *s = *sp;
    CHECK_NULL(s);
    CHECK_SIGNATURE(s);

    char *str = df_mem_take(&s->m);
    df_str_delete(&s);
    return str;
}
