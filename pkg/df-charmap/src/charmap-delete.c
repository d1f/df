#include <df/charmap.h>
#include <df/x.h>   // df_freep
#include <df/bt.h>


void df_charmap_delete(df_charmap_t **charmap)
{
    DF_BT;

    df_freep(charmap);
}
