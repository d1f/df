#ifndef  DF_CHARMAP_H
# define DF_CHARMAP_H

# include <stddef.h> // size_t
# include <df/defs.h>


typedef struct df_charmap_s
{
    size_t height;
    size_t width;
    size_t first_row;
    size_t  last_row;
    char   map[0];
} df_charmap_t;


// returns malloced and zeroed struct with data.
// NULL on error, message issued.
df_charmap_t *df_charmap_create(size_t height, size_t width);
void          df_charmap_delete(df_charmap_t **charmap);

// returns error flag, error message issued
int           df_charmap_check(const df_charmap_t *charmap) ATTR(nonnull);

void          df_charmap_contour(df_charmap_t *charmap) ATTR(nonnull);

void          df_charmap_squeeze(df_charmap_t *charmap) ATTR(nonnull);

void          df_charmap_squeeze_standalone(size_t height, size_t width, const char map[height][width],
					    size_t *first_row, size_t *last_row) ATTR(nonnull);


#endif //DF_CHARMAP_H
