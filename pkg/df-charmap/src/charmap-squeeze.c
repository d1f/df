#include <df/charmap.h>
#include <df/bt.h>

void df_charmap_squeeze(df_charmap_t *cm)
{
    DF_BT;

    if (df_charmap_check(cm))
	return;

    df_charmap_squeeze_standalone(cm->height, cm->width, (void*)cm->map,
				  &cm->first_row, &cm->last_row);

    df_charmap_check(cm);
}
