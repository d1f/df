#include <df/charmap.h>
#include <df/memtrue.h>
#include <df/bt.h>

void df_charmap_squeeze_standalone(size_t height, size_t width, const char map[height][width],
				   size_t *first_row, size_t *last_row)
{
    DF_BT;

    if (height == 0 || width == 0)
	return;

    *first_row = 0;
    *last_row  = height-1;

    size_t row;

    // find first scanline
    for (row=0; row < height; ++row)
	if (df_memtrue(map[row], width))
	    break;

    if (row < height)
	*first_row = row;

    // find last scanline
    for (row = height-1; row > 0; --row)
	if (df_memtrue(map[row], width))
	    break;

    if (row > 0)
	*last_row = row;
}
