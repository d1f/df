#include <df/charmap.h>
#include <df/error.h>
#include <df/x.h>
#include <df/bt.h>


// returns malloced and zeroed struct with data.
// NULL on error, message issued.
df_charmap_t *df_charmap_create(size_t height, size_t width)
{
    DF_BT;

    if (height == 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): height parameter is 0", __func__);
	return NULL;
    }

    if (width == 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): width parameter is 0", __func__);
	return NULL;
    }

    df_charmap_t *charmap = df_xzmalloc(sizeof(df_charmap_t) + width * height);
    if (charmap == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): memory for charmap", __func__);
	return NULL;
    }

    charmap->height = height;
    charmap->width  = width;

    charmap->first_row = 0;
    charmap->last_row  = height-1;

    return charmap;
}
