#include <df/charmap.h>
#include <df/log.h>
#include <df/bt.h>

// returns error flag, error message issued
int df_charmap_check(const df_charmap_t *cm)
{
    DF_BT;

    if (cm->height == 0)
    {
	DFLOGE("height is 0");
	return -1;
    }

    if (cm->width == 0)
    {
	DFLOGE("width is 0");
	return -1;
    }

    if (cm->first_row >= cm->height)
    {
	DFLOGE("first row %zu >= height %zu", cm->first_row, cm->height);
	return -1;
    }

    if (cm->last_row >= cm->height)
    {
	DFLOGE("last row %zu >= height %zu", cm->last_row, cm->height);
	return -1;
    }

    if (cm->first_row > cm->last_row)
    {
	DFLOGE("first row %zu > last row %zu", cm->first_row, cm->last_row);
	return -1;
    }

    return 0;
}
