#include <df/charmap.h>
#include <df/bt.h>

static void contour(size_t first_row, size_t last_row,
		    size_t height, size_t width, char data[height][width])
{
    DF_BT;

    size_t x, y;

    for (y=first_row; y <= last_row; ++y)
    {
	for (x=0; x < width; ++x)
	{
	    if (data[y][x] > 1)
	    {
		if (x > 0)
		    if (data[y][x-1] == 0)
			data[y][x-1] =  1;

		if (x < width - 1)
		    if (data[y][x+1] == 0)
			data[y][x+1] =  1;

		if (y > 0)
		    if (data[y-1][x] == 0)
			data[y-1][x] =  1;

		if (y < height - 1)
		    if (data[y+1][x] == 0)
			data[y+1][x] =  1;
	    }
	}
    }
}

void df_charmap_contour(df_charmap_t *cm)
{
    DF_BT;

    if (df_charmap_check(cm))
	return;

    void *m = cm->map;

    contour(cm->first_row, cm->last_row, cm->height, cm->width, m);

    // unsqueeze by 1 row
    if (cm->first_row > 0)
	cm->first_row--;
    if (cm->last_row < cm->height-1)
	cm->last_row++;
}
