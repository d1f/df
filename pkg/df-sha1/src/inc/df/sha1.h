#ifndef DF_SHA1_H
#define DF_SHA1_H

#include <df/defs.h>  // ATTR
#include <df/types.h> // uschar


#define DF_SHA1_HASH_LEN 20
#define DF_SHA1_HEX_LEN (DF_SHA1_HASH_LEN * 2)


        struct df_sha1;
typedef struct df_sha1 df_sha1_t;


df_sha1_t *df_sha1_create(void)                                 ATTR(returns_nonnull);
void       df_sha1_delete(df_sha1_t **sha1_p)                   ATTR(nonnull);

void       df_sha1_update(df_sha1_t  *sha1,
			  const void *data, size_t len)         ATTR(nonnull);
void       df_sha1_finish(df_sha1_t *sha1,
			  uchar hashout[DF_SHA1_HASH_LEN])      ATTR(nonnull);

// combine all the above
void df_sha1(uchar hashout[DF_SHA1_HASH_LEN],
	     const void *data, size_t len)                      ATTR(nonnull);


void df_sha1_hex(char hash_str[(DF_SHA1_HASH_LEN*2)+1],
		 const void *data, size_t len)                  ATTR(nonnull);


#endif //DF_SHA1_H
