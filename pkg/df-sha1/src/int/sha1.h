#ifndef DF_SHA1_INT_H
#define DF_SHA1_INT_H

#include <df/sha1.h>
#include <df/types.h> // df_signature_t

#include "int/git-sha1.h"


struct df_sha1
{
    df_signature_t signature;

    blk_SHA_CTX ctx;
};


#endif //DF_SHA1_INT_H
