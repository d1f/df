#ifndef GIT_SHA1_H
#define GIT_SHA1_H

// shamelessly stolen from git source

#include <stdlib.h> // size_t

/*
 * SHA1 routine optimized to do word accesses rather than byte accesses,
 * and to avoid unnecessary copies into the context array.
 *
 * This was initially based on the Mozilla SHA1 implementation, although
 * none of the original Mozilla code remains.
 */

typedef struct {
	unsigned long long size;
	unsigned int H[5];
	unsigned int W[16];
} blk_SHA_CTX;

void blk_SHA1_Init  (blk_SHA_CTX *ctx);
void blk_SHA1_Update(blk_SHA_CTX *ctx, const void *dataIn, unsigned long len);
void blk_SHA1_Final (blk_SHA_CTX *ctx, unsigned char hashout[20]);


#endif // GIT_SHA1_H
