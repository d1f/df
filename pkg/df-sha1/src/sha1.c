#include "int/sha1.h"
#include <df/x.h>
#include <df/bt.h>
#include <df/hex.h>
#include <df/check.h>
#include "df-sha1-signatures.h"


df_sha1_t *df_sha1_create(void)
{
    DF_BT;

    df_sha1_t *sha1 = df_xzmalloc(sizeof(df_sha1_t));
    sha1->signature = df_sha1_SIGNATURE;

    blk_SHA1_Init(&sha1->ctx);

    return sha1;
}

void df_sha1_delete(df_sha1_t **sha1_p)
{
    DF_BT;

    df_sha1_t *sha1 = *sha1_p;
    if (sha1 == NULL)
	return;

    DF_CHECK_SIGNATURE(df_sha1, sha1);

    df_freep(sha1_p);
}

void df_sha1_update(df_sha1_t *sha1,
		    const void *data, size_t len)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_sha1, sha1);

    blk_SHA1_Update(&sha1->ctx, data, len);
}

void df_sha1_finish(df_sha1_t *sha1, uchar hashout[DF_SHA1_HASH_LEN])
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_sha1, sha1);

    blk_SHA1_Final(&sha1->ctx, hashout);
}


// combine all the above
void df_sha1(uchar hashout[DF_SHA1_HASH_LEN], const void *data, size_t len)
{
    DF_BT;

    blk_SHA_CTX ctx;
    blk_SHA1_Init  (&ctx);
    blk_SHA1_Update(&ctx, data, len);
    blk_SHA1_Final (&ctx, hashout);
}

void df_sha1_hex(char hash_str[(DF_SHA1_HASH_LEN*2)+1],
		 const void *data, size_t len)
{
    uchar hash[DF_SHA1_HASH_LEN];
    df_sha1(hash, data, len);
    df_bin2hex(hash_str, hash, DF_SHA1_HASH_LEN);
}
