#include <df/bt.h>

static df_bt_t *df_bt_last = NULL;

df_bt_t *df_bt_last_get(void)
{
    return df_bt_last;
}

void df_bt_last_set(df_bt_t *last)
{
    df_bt_last = last;
}
