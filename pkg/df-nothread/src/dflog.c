#include <df/log-buf-get-set.h>

static dflog_buf_t *logbuf = NULL;


// init once, set thread-specific buffer
void dflog_buf_set_(dflog_buf_t *b)
{
    logbuf = b;
}

// init once, return thread-specific buffer
dflog_buf_t *dflog_buf_get_(void)
{
    return logbuf;
}
