#include <int/da.h>
#include <df/error.h>

void df_da_insert_item(df_da_t* da, size_t index, const void *item_p)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    size_t newsize = df_mem_insert_at(da->m, index * da->item_size,
				      item_p, da->item_size);

    da->length = newsize / da->item_size;
    da->sorted = false;
}
