#include <int/da.h>
#include <df/error.h>
#include <df/log.h>


void df_da_del_item_nocheck(df_da_t *da, size_t index)
{
    DF_BT;

    void *item_p  = df_da_item_nocheck(da, index);

    if (item_p != NULL)
    {
	if (da->item_dtor != NULL)
	    da->item_dtor(item_p);
	else
	    memset(item_p, 0, da->item_size);

	da->sorted  = false;
    }
}

// destructs item
void df_da_del_item(df_da_t *da, size_t index)
{
    DF_BT;

    df_da_check_base_NULL (da       , __func__, DF_ERR);
    DF_CHECK_SIGNATURE(df_da, da);
    df_da_check_index     (da, index, __func__, DF_ERR);
    df_da_del_item_nocheck(da, index);
}

void df_da_cut_item(df_da_t* da, size_t index)
{
    DF_BT;

    df_da_check_base_NULL (da       , __func__, DF_ERR);
    DF_CHECK_SIGNATURE(df_da, da);
    df_da_check_index     (da, index, __func__, DF_ERR);
    df_da_del_item_nocheck(da, index);

    size_t newsize = df_mem_cut_at(da->m, index * da->item_size, da->item_size);

    da->length = newsize / da->item_size;
    da->sorted = false;
}
