#include <int/da.h>
#include <df/x.h> // df_freep
#include <df/error.h>


void df_da_delete(df_da_t **dap)
{
    DF_BT;

    DF_CHECK_NULL("Object pointer pointer", dap);

    df_da_t *da = *dap;
    if (da != NULL)
    {
	DF_CHECK_SIGNATURE(df_da, da);
	df_da_del_items(da);
	df_mem_delete(&da->m);
	da->length = 0;

	da->signature = 0;
	df_freep(dap);
    }
}
