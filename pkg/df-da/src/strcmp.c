#include <int/da.h>
#include <df/error.h>
#include <string.h> // strcmp

// comparator for zero-terminated string, comdatible with with all of qsort, bsearch, ...
int df_da_strcmp(const void *l_ptr_ptr, const void *r_ptr_ptr)
{
    DF_BT;

    const char *l_ptr = *(const df_cvptr_t*)l_ptr_ptr;
    const char *r_ptr = *(const df_cvptr_t*)r_ptr_ptr;

    if (l_ptr == NULL)
    {
	if (r_ptr == NULL)
	    return 0;
	else
	    return -1;
    }
    else // l_ptr != NULL)
    {
	if (r_ptr == NULL)
	    return  1;
	// else r_ptr != NULL - fall to strcmp
    }

    // l_ptr != NULL && r_ptr != NULL
    return strcmp(l_ptr, r_ptr);
}
