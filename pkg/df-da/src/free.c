#include <int/da.h>
#include <df/log.h>
#include <sys/types.h> // ssize_t
#include <string.h>    // memset


// destruct all items.
void df_da_del_items(df_da_t *da)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    void *base = df_da_base_nocheck(da);

    if (base != NULL)
    {
	if (da->item_dtor != NULL)
	{
	    for (ssize_t i = da->length - 1; i >= 0; --i)
	    {
		void *item_p = df_da_item_nocheck(da, i);
		if (item_p != NULL)
		    da->item_dtor(item_p);
	    }
	}
	memset(base, 0, df_mem_size(da->m));
    }

    da->sorted = false;
}

// destruct all items and free all memory.
void df_da_free(df_da_t *da)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    df_da_del_items(da);
    df_mem_free(da->m);
    da->length = 0;
    da->sorted = false;
}
