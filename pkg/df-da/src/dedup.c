#include <int/da.h>
#include <df/error.h>

// sort array, find and destroy dup entry pointers.
// returns number of destroyed dups.
size_t df_da_dedup(df_da_t* da)
{
    DF_BT;

    void *base = df_da_check_base_NULL(da, __func__, DF_WARN);
    if (base == NULL)
	return 0;

    DF_CHECK_SIGNATURE(df_da, da);

    size_t length = da->length;
    if (length <= 1)
	return 0;

    DF_CHECK_NULL("item_cmp()", da->item_cmp);

    if (!da->sorted)
	df_da_qsort(da);

    size_t deduped = 0;
    for (size_t i = 0; i < length-1; ++i)
    {
	void *item_p  = df_da_item_nocheck(da, i);
	void *item1_p = df_da_item_nocheck(da, i+1);

	if (item_p == NULL || item1_p == NULL)
	    continue;

	if ( da->item_cmp(item_p, item1_p) == 0) // dup found
	{
	    df_da_del_item_nocheck(da, i);
	    ++deduped;
	    continue;
	}
    }

    return deduped;
}
