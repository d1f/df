#include <int/da.h>
#include <df/log.h>
#include <stdlib.h> // exit


// returns base, NULL if base == NULL and DF_WARN,
// exit with failure  if base == NULL and DF_ERR.
void *df_da_check_base_NULL(const df_da_t *da, const char *func, err_warn_t err_warn)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    void *base = df_da_base_nocheck(da);
    if (base == NULL)
    {
	dflog(err_warn == DF_ERR ? LOG_ERR : LOG_WARNING, "%s(): Base is NULL", func);
	if (err_warn == DF_ERR)
	    df_bt_exit(EXIT_FAILURE);
    }

    return base;
}

// returns true if check failed and DF_WARN,
// exit with failure if check failed and DF_ERR.
// do call after other checks.
bool df_da_check_index(const df_da_t *da, size_t index, const char *func, err_warn_t err_warn)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    if (index >= da->length)
    {
	dflog(err_warn == DF_ERR ? LOG_ERR : LOG_WARNING, "%s(): %s(): index (%zu) >= length (%zu)",
	      func, __func__, index, da->length);
	if (err_warn == DF_ERR)
	    df_bt_exit(EXIT_FAILURE);

	return true;
    }

    return false;
}
