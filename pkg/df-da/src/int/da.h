#ifndef  DF_DYN_ARRAY_INT_H
# define DF_DYN_ARRAY_INT_H

# include <df/da.h>
# include <df/mem.h>
# include <df/bt.h>
# include <df/check.h>
# include <df-da-signatures.h>


struct df_da
{
    df_signature_t signature;

    size_t         item_size;
    df_item_dtor_f item_dtor;
    df_item_cmp_f  item_cmp;
    size_t length;
    df_mem_t *m;
    bool sorted;
};


typedef enum DF_DA_ERR_WARN { DF_WARN, DF_ERR } err_warn_t;

#define DF_DA_CHECK_NULL(da) DF_CHECK_NULL("df_da_t pointer", da)

// returns base, NULL if base == NULL and DF_WARN,
// exit with failure  if base == NULL and DF_ERR.
void *df_da_check_base_NULL(const df_da_t *da, const char *func, err_warn_t err_warn) ATTR(nonnull);

// returns true if check failed and DF_WARN,
// exit with failure if check failed and DF_ERR.
bool df_da_check_index(const df_da_t *da, size_t index, const char *func, err_warn_t err_warn) ATTR(nonnull);


static inline ATTR(nonnull)
void *df_da_base_nocheck(const df_da_t *da)
{
    return df_mem_data(da->m);
}

static inline ATTR(nonnull)
void *df_da_item_nocheck(df_da_t* da, size_t index)
{
    return (char*)df_da_base_nocheck(da) + index * da->item_size;
}

void df_da_del_item_nocheck(df_da_t *da, size_t index) ATTR(nonnull);


#endif //DF_DYN_ARRAY_INT_H
