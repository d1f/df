#include <int/da.h>
#include <df/error.h>
#include <string.h> // memcpy

// destructs old item
void df_da_set_item(df_da_t* da, size_t index, const void *item_p)
{
    DF_BT;

    df_da_del_item(da, index); // all checks performed
    void *item_ptr = df_da_item_nocheck(da, index);
    memcpy(item_ptr, item_p, da->item_size);
    da->sorted = false;
}
