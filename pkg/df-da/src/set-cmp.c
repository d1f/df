#include <int/da.h>
#include <df/error.h>

void df_da_set_cmp(df_da_t* da, df_item_cmp_f item_cmp)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    da->item_cmp = item_cmp;
}
