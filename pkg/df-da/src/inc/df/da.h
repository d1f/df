#ifndef  DF_DYN_ARRAY_H
# define DF_DYN_ARRAY_H

# include <df/types.h>  // df_vptr_t df_cvptr_t df_item_dtor_f df_item_cmp_f
# include <df/defs.h>   // ATTR
# include <sys/types.h> // ssize_t
# include <stdbool.h>


        struct df_da;
typedef struct df_da df_da_t;


// ptr args may be NULL.
df_da_t   *df_da_create    (size_t item_size, df_item_dtor_f item_dtor, df_item_cmp_f item_cmp)
                                ATTR(returns_nonnull);
void       df_da_delete    (df_da_t **dap);

void       df_da_del_items (df_da_t  *da)      ATTR(nonnull); // destruct all items.
void       df_da_free      (df_da_t  *da)      ATTR(nonnull); // destruct all items and free all memory.

size_t     df_da_item_size (df_da_t const *da) ATTR(nonnull);
size_t     df_da_length    (df_da_t const *da) ATTR(nonnull);
void      *df_da_base      (df_da_t const *da) ATTR(nonnull);

void      *df_da_get_item  (df_da_t* da, size_t index)                       ATTR(nonnull); // returns item pointer
void       df_da_set_item  (df_da_t* da, size_t index, const void *item_p)   ATTR(nonnull); // destructs old item
void       df_da_del_item  (df_da_t *da, size_t index)                       ATTR(nonnull); // destructs item

void       df_da_append_item (df_da_t* da, const void *item_p)               ATTR(nonnull);
void       df_da_insert_item (df_da_t* da, size_t index, const void *item_p) ATTR(nonnull);
void       df_da_cut_item    (df_da_t* da, size_t index)                     ATTR(nonnull);

void       df_da_set_cmp   (df_da_t* da, df_item_cmp_f item_cmp) ATTR(nonnull(1));

// comparator for zero-terminated string, compatible with with all of qsort, bsearch, ...
int        df_da_strcmp(const void *l_ptr_ptr, const void *r_ptr_ptr) ATTR(nonnull);

// returns found index or <0
ssize_t    df_da_lsearch(df_da_t* da, const void *search_item_p) ATTR(nonnull);
ssize_t    df_da_bsearch(df_da_t* da, const void *search_item_p) ATTR(nonnull);

// returns found item pointer or NULL
static inline ATTR(nonnull)
void      *df_da_lsearch_item(df_da_t* da, const void *search_item_p)
{
    ssize_t idx = df_da_lsearch(da, search_item_p);
    return idx >= 0 ? df_da_get_item(da, idx) : NULL;
}

static inline ATTR(nonnull)
void      *df_da_bsearch_item(df_da_t* da, const void *search_item_p)
{
    ssize_t idx = df_da_bsearch(da, search_item_p);
    return idx >= 0 ? df_da_get_item(da, idx) : NULL;
}

// sets sorted flag
void       df_da_qsort(df_da_t* da) ATTR(nonnull);


// sort array, find and destroy dup entry pointers.
// returns number of destroyed dups.
size_t     df_da_dedup(df_da_t* da) ATTR(nonnull);


// usage: DF_DA_FOREACH(da, item_type, item_p, idx) { statements; }
// warning: item_p is local auto variable in current scope
#define DF_DA_FOREACH(da, item_type, item_p, idx) \
    item_type *item_p = NULL; \
    item_type *da_base = df_da_base(da); \
    for (size_t da_len = df_da_length(da), idx = 0; \
    (idx < da_len) ? (item_p = & da_base[idx], true) : false; \
        ++idx)


// usage: DF_DA_FOREACH_BACK(da, item_type, item_p, idx) { statements; }
// warning: item is local auto variable in current scope
#define DF_DA_FOREACH_BACK(da, item, idx) \
    item_type *item_p = NULL; \
    item_type *da_base = df_da_base(da); \
    for (ssize_t da_len = df_da_length(da), idx = da_len - 1; \
    (idx >= 0) ? (item_p = & da_base[idx], true) : false; \
        --idx)


#endif //DF_DYN_ARRAY_H
