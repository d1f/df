#include <int/da.h>
#include <df/x.h>
#include <df/log.h>


// ptr args may be NULL.
df_da_t *df_da_create(size_t item_size, df_item_dtor_f item_dtor, df_item_cmp_f item_cmp)
{
    DF_BT;

    if (item_size == 0)
	DFLOGEXBT("item_size == 0");

    df_da_t *da   = df_xmalloc(sizeof(*da));

    da->signature = df_da_SIGNATURE;;
    da->item_size = item_size;
    da->item_dtor = item_dtor;
    da->item_cmp  = item_cmp;
    da->length    = 0;
    da->m         = df_mem_create(0, NULL);
    da->sorted    = false;

    return da;
}
