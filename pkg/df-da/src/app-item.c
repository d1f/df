#include <int/da.h>
#include <df/error.h>

void df_da_append_item(df_da_t* da, const void *item_p)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    size_t newsize = df_mem_append_data(da->m, item_p, da->item_size);

    da->length = newsize / da->item_size;
    da->sorted = false;
}
