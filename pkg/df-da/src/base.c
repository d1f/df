#include <int/da.h>
#include <df/log.h>

void *df_da_base(df_da_t const *da)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    return df_da_base_nocheck(da);
}
