#include <int/da.h>
#include <df/error.h>
#include <search.h> // lfind


// returns found index or <0
ssize_t df_da_lsearch(df_da_t* da, const void *search_item_p)
{
    DF_BT;

    void *base = df_da_check_base_NULL(da, __func__, DF_WARN);
    if (base != NULL)
    {
	DF_CHECK_SIGNATURE(df_da, da);
	DF_CHECK_NULL("item_cmp()", da->item_cmp);

	size_t length = da->length;

	void *found = lfind(search_item_p, base, &length, da->item_size, da->item_cmp);
	if (found != NULL)
	    return ( (char*)found - (char*)base ) / da->item_size;
    }

    return -1;
}
