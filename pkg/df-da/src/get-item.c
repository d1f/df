#include <int/da.h>
#include <df/log.h>

// returns item pointer
void *df_da_get_item(df_da_t* da, size_t index)
{
    DF_BT;

    void *base = df_da_check_base_NULL(da, __func__, DF_ERR);

    DF_CHECK_SIGNATURE(df_da, da);

    df_da_check_index(da, index, __func__, DF_ERR);

    return (char*)base + index * da->item_size;
}
