#define _GNU_SOURCE
#include <stdlib.h> // qsort
#include <int/da.h>
#include <df/error.h>


// sets sorted flag
void df_da_qsort(df_da_t* da)
{
    DF_BT;

    void *base = df_da_check_base_NULL(da, __func__, DF_WARN);
    if (base == NULL)
	return;

    DF_CHECK_SIGNATURE(df_da, da);

    size_t length = da->length;
    if (length <= 1)
	return;

    DF_CHECK_NULL("item_cmp()", da->item_cmp);

    qsort(base, length, da->item_size, da->item_cmp);

    da->sorted = true;
}
