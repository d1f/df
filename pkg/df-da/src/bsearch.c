#include <int/da.h>
#include <df/error.h>

ssize_t df_da_bsearch(df_da_t* da, const void *search_item_p)
{
    DF_BT;

    void *base = df_da_check_base_NULL(da, __func__, DF_WARN);
    if (base == NULL)
	return -1;

    DF_CHECK_SIGNATURE(df_da, da);

    size_t length = da->length;

    if (length == 0)
	return -1;
    else if (length >= 1 && length <= 3) //FIXME: what is good upper limit for linear search?
	return df_da_lsearch(da, search_item_p);

    DF_CHECK_NULL("item_cmp()", da->item_cmp);

    if (!da->sorted)
	df_da_qsort(da);

    void *found = bsearch(search_item_p, base, length, da->item_size, da->item_cmp);
    if (found == NULL)
	return -1;

    return ( (char*)found - (char*)base ) / da->item_size;
}
