#include <int/da.h>

size_t df_da_length(df_da_t const *da)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_da, da);

    return da->length;
}
