#include <df/bt.h>
#include <pthread.h>

static pthread_key_t  key;
static pthread_once_t key_once = PTHREAD_ONCE_INIT;


static void df_bt_key_init(void)
{
    pthread_key_create(&key, NULL);
}

df_bt_t *df_bt_last_get(void)
{
    pthread_once(&key_once, df_bt_key_init);
    return pthread_getspecific(key);
}

void df_bt_last_set(df_bt_t *last)
{
    //pthread_once(&key_once, df_bt_key_init);
    pthread_setspecific(key, last);
}
