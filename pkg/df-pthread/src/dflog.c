#include <df/log-buf-get-set.h>


#include <pthread.h>

static pthread_key_t  key;
static pthread_once_t key_once = PTHREAD_ONCE_INIT;


static void dflog_key_destroy(void *p)
{
    dflog_buf_delete(p);
}

static void dflog_key_init(void)
{
    pthread_key_create(&key, dflog_key_destroy);
}


// init once, set thread-specific buffer
void dflog_buf_set_(dflog_buf_t *b)
{
    pthread_once(&key_once, dflog_key_init);
    pthread_setspecific(key, b);
}

// init once, return thread-specific buffer
dflog_buf_t *dflog_buf_get_(void)
{
    pthread_once(&key_once, dflog_key_init);
    return pthread_getspecific(key);
}
