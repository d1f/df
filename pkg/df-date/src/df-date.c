#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(void)
{
    tzset();
    const time_t time_curr = time(NULL);
    struct tm *tm = localtime(&time_curr);

    printf("%s/%s %04d.%02d.%02d %02d:%02d:%02d\n", tzname[0], tzname[1],
	   tm->tm_year + 1900, tm->tm_mon, tm->tm_mday,
	   tm->tm_hour, tm->tm_min, tm->tm_sec);

    return EXIT_SUCCESS;
}
