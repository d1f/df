#include <int/list.h>


void *df_list_node_data(df_list_node_t *node)
{
    df_list_node_check(DF_FLF, node);

    return node->data;
}
