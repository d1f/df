#include <int/list.h>


df_list_node_t *df_list_node_next(df_list_node_t *node)
{
    DF_BT;

    df_list_node_check(DF_FLF, node);

    return node->next;
}

df_list_node_t *df_list_node_prev(df_list_node_t *node)
{
    DF_BT;

    df_list_node_check(DF_FLF, node);

    return node->prev;
}
