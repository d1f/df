#include <int/list.h>


df_list_node_t *df_list_node_search(const void* key /* user data */,
				    df_list_node_t *from,
				    df_item_cmp_f cmp,
				    df_list_walk_mode_t walk_mode)
{
    DF_BT;

    df_list_node_check(DF_FLF, from);

    for (df_list_node_t *node = from; node != NULL;
	 node = (walk_mode == DF_LIST_WALK_FORWARD) ? node->next : node->prev)
    {
	df_list_node_check(DF_FLF, from);

	if (cmp(df_list_node_data(node), key) == 0)
	    return node;
    }

    return NULL;
}
