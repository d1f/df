#ifndef  DF_LIST_INT_H
# define DF_LIST_INT_H

# include <df/list.h>
# include <df/x.h>
# include <df/bt.h>
# include <df/error.h>
# include <df/log.h>
# include <stdint.h> // intptr_t

# include <df-list-signatures.h>
# include <df/check.h>


struct df_list_node
{
    df_signature_t signature; // df_list_node_SIGNATURE

    df_item_dtor_f dtor;

    df_list_node_t *next, *prev;

    size_t   data_size;
    intptr_t data[0];
};


void df_list_node_unlink_nocheck(df_list_node_t *node) ATTR(nonnull);
void df_list_node_link_nocheck  (df_list_node_t *target, df_list_node_t *source,
				 df_list_link_mode_t link_mode) ATTR(nonnull(1,2));


struct df_list
{
    df_signature_t signature; // df_list_SIGNATURE

    size_t nodes;
    df_item_cmp_f cmp;
    df_list_node_t *head, *tail;
};


void df_list_walk_nocheck(df_list_t *list, df_list_walk_action_f action,
			  df_list_walk_mode_t walk_mode, void *user_data) ATTR(nonnull(1,2));


#endif //DF_LIST_INT_H
