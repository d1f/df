#include <int/list.h>


void df_list_link(df_list_t *list, df_list_node_t  *target, df_list_node_t  *source,
		  df_list_link_mode_t link_mode)
{
    DF_BT;

    df_list_check(DF_FLF, list);

    if (target != NULL) // may be NULL if list is empty
	df_list_node_check(DF_FLF, target);

    df_list_node_check(DF_FLF, source);

    if (source->next != NULL || source->prev != NULL)
	dflog_xbt(DF_FLF, "New node is linked");

    if (list->nodes == 0 && list->tail == NULL && list->head == NULL)
    {
	list->tail  = source;
	list->head  = source;
	list->nodes = 1;
	return;
    }

    df_list_node_link_nocheck(target, source, link_mode);
    df_list_node_check(DF_FLF, source);
    df_list_node_check(DF_FLF, target);
    list->nodes++;

    // assert nodes >= 1 && tail != NULL && head != NULL
    if (link_mode == DF_LIST_LINK_AFTER)
    {
	if (list->tail == target)
	    list->tail =  source;
    }
    else if (link_mode == DF_LIST_LINK_BEFORE)
    {
	if (list->head == target)
	    list->head =  source;
    }
    else
    {
	dflog_xbt(DF_FLF, "Unknown link mode %d", link_mode);
    }

    df_list_check(DF_FLF, list);
}

df_list_node_t *df_list_unlink(df_list_t *list, df_list_node_t  *target)
{
    DF_BT;

    df_list_check(DF_FLF, list);

    df_list_node_check(DF_FLF, target);

    if (list->nodes == 0 && list->tail == NULL && list->head == NULL)
	return NULL;

    if (list->tail == target)
	list->tail =  target->prev;
    if (list->head == target)
	list->head =  target->next;

    df_list_node_unlink_nocheck(target);
    if (list->nodes == 0)
	dflog_xbt(DF_FLF, "nodes == 0 before decrement");
    list->nodes--;

    df_list_node_check(DF_FLF, target);
    df_list_check(DF_FLF, list);

    return target;
}
