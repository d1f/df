#include <int/list.h>


void df_list_node_link_nocheck(df_list_node_t *target, df_list_node_t *source,
			       df_list_link_mode_t link_mode)
{
    DF_BT;

    if (link_mode == DF_LIST_LINK_AFTER)
    {
	source->prev = target;
	source->next = target->next;
	target->next = source;
    }
    else if (link_mode == DF_LIST_LINK_BEFORE)
    {
	source->next = target;
	source->prev = target->prev;
	target->prev = source;
    }
    else
    {
	DFLOGEXBT("Unknown link mode %d", link_mode);
    }
}

void df_list_node_unlink_nocheck(df_list_node_t *node)
{
    DF_BT;

    if (node->prev != NULL)
	if (node->prev->next == node)
	    node->prev->next = node->next;

    if (node->next != NULL)
	if (node->next->prev == node)
	    node->next->prev = node->prev;

    node->next = node->prev = NULL;
}
