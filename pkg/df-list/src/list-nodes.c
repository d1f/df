#include <int/list.h>


size_t df_list_nodes(df_list_t const *list)
{
    DF_BT;

    df_list_check(DF_FLF, list);

    return list->nodes;
}
