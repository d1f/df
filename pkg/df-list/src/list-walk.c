#include <int/list.h>


void df_list_walk_nocheck(df_list_t *list, df_list_walk_action_f action,
			  df_list_walk_mode_t walk_mode, void *user_data)
{
    DF_BT;

    if (walk_mode == DF_LIST_WALK_FORWARD)
    {
	for (df_list_node_t *node = list->head, *node_next = NULL;
	     node != NULL;
	     node = node_next)
	{
	    node_next = node->next;

	    if (action(list, node, user_data))
		break;
	}
    }
    else if (walk_mode == DF_LIST_WALK_BACKWARD)
    {
	for (df_list_node_t *node = list->tail, *node_prev = NULL;
	     node != NULL;
	     node = node_prev)
	{
	    node_prev = node->prev;

	    if (action(list, node, user_data))
		break;
	}
    }
    else
    {
	DFLOGEXBT("Unknown walk mode %d", walk_mode);
    }
};

void df_list_walk(df_list_t *list, df_list_walk_action_f action,
		  df_list_walk_mode_t walk_mode, void *user_data)
{
    DF_BT;

    df_list_check(DF_FLF, list);

    df_list_walk_nocheck(list, action, walk_mode, user_data);
}
