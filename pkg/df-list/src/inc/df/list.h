#ifndef  DF_LIST_H
# define DF_LIST_H

# include <df/types.h>  // df_vptr_t df_cvptr_t df_item_dtor_f df_item_cmp_f df_item_dtor_f
# include <df/defs.h>   // returns_nonnull, ATTR
# include <sys/types.h> // ssize_t
# include <stdbool.h>


         struct df_list_node;
typedef  struct df_list_node df_list_node_t;


df_list_node_t *df_list_node_create(const void* data, size_t data_size,
				    df_item_dtor_f dtor)           ATTR(nonnull(1), returns_nonnull);
void            df_list_node_delete(df_list_node_t      **node_p)  ATTR(nonnull);
void            df_list_node_check (const df_flf_t *flf,
				    df_list_node_t const *node)    ATTR(nonnull); // returns error flag
df_list_node_t *df_list_node_next  (df_list_node_t       *node)    ATTR(nonnull);
df_list_node_t *df_list_node_prev  (df_list_node_t       *node)    ATTR(nonnull);
void           *df_list_node_data  (df_list_node_t       *node)    ATTR(nonnull); // returns user data pointer


         struct df_list;
typedef  struct df_list df_list_t;

typedef    enum DF_LIST_LINK_MODE { DF_LIST_LINK_BEFORE , DF_LIST_LINK_AFTER    } df_list_link_mode_t;
typedef    enum DF_LIST_WALK_MODE { DF_LIST_WALK_FORWARD, DF_LIST_WALK_BACKWARD } df_list_walk_mode_t;

// returns stop flag
typedef  bool (*df_list_walk_action_f)(df_list_t *list, df_list_node_t *node,
				       void *user_data)            ATTR(nonnull);


df_list_t      *df_list_create   (df_item_cmp_f cmp)               ATTR(returns_nonnull);
void            df_list_delete   (df_list_t      **list_p)         ATTR(nonnull);

void            df_list_check    (const df_flf_t *flf,
				  df_list_t const *list)           ATTR(nonnull); // returns error flag
void            df_list_check_all(const df_flf_t *flf,
				  df_list_t const *list)           ATTR(nonnull); // returns error flag

size_t          df_list_nodes    (df_list_t const *list)           ATTR(nonnull);

df_list_node_t *df_list_tail     (df_list_t       *list)           ATTR(nonnull);
df_list_node_t *df_list_head     (df_list_t       *list)           ATTR(nonnull);

void            df_list_link     (df_list_t       *list,
				  df_list_node_t  *target, // may be NULL if list is empty
				  df_list_node_t  *source,
				  df_list_link_mode_t link_mode)   ATTR(nonnull(1,3));

// returns unlinked target
df_list_node_t *df_list_unlink   (df_list_t *list,
				  df_list_node_t  *target)         ATTR(nonnull);


void            df_list_walk     (df_list_t *list,
				  df_list_walk_action_f action,
				  df_list_walk_mode_t walk_mode,
				  void *user_data)                 ATTR(nonnull(1,2));

#define DF_LIST_FOREACH_FORWARD(list, node) \
    for (df_list_node_t *node = df_list_head(list), *node##_next = NULL; \
         node != NULL ? node##_next = df_list_node_next(node) : NULL, node != NULL; \
         node = node##_next)

#define DF_LIST_FOREACH_BACKWARD(list, node) \
    for (df_list_node_t *node = df_list_tail(list), *node##_prev = NULL; \
         node != NULL ? node##_prev = df_list_node_prev(node) : NULL, node != NULL; \
         node = node##_prev)


// returns found node or NULL.
df_list_node_t *df_list_node_search(const void* key /* user data */,
				    df_list_node_t *from,
				    df_item_cmp_f cmp,
				    df_list_walk_mode_t walk_mode) ATTR(nonnull(1,2,3));


#endif //DF_LIST_H
