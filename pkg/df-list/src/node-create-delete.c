#include <int/list.h>


df_list_node_t *df_list_node_create(const void* data, size_t data_size, df_item_dtor_f dtor)
{
    DF_BT;

    if (data_size == 0)
	DFLOGEXBT("user size is 0");

    df_list_node_t *node = df_xzmalloc(sizeof(*node) + data_size);
    node->signature      = df_list_node_SIGNATURE;
    node->dtor           = dtor;
    node->data_size      = data_size;
    memcpy(node->data, data, data_size);
    return node;
}

void df_list_node_delete(df_list_node_t **node_p)
{
    DF_BT;

    df_list_node_t *node = *node_p;

    df_list_node_check(DF_FLF, node);

    if (node->dtor != NULL)
	node->dtor(node->data);

    df_list_node_unlink_nocheck(node);

    memset(node, 0, sizeof(*node) + node->data_size);
    df_freep(node_p);
}
