#include <int/list.h>


df_list_t *df_list_create(df_item_cmp_f cmp)
{
    DF_BT;

    df_list_t *list = df_xzmalloc(sizeof(*list));
    list->cmp       = cmp;
    list->signature = df_list_SIGNATURE;
    return list;
}

static bool df_list_node_delete_action(df_list_t *list, df_list_node_t *node, void *user_data)
{
    DF_BT;

    (void)user_data;

    if (node->dtor != NULL)
	node->dtor(node->data);

    df_list_node_unlink_nocheck(node);

    free(node);

    if (list->nodes > 0)
	list->nodes--;
    else
	DFLOGE("nodes == 0");


    return false;
}

void df_list_delete(df_list_t **list_p)
{
    DF_BT;

    df_list_t *list = *list_p;

    df_list_check(DF_FLF, list);

    df_list_walk_nocheck(list, df_list_node_delete_action, DF_LIST_WALK_BACKWARD, NULL);

    list->nodes = 0;
    list->head = list->tail = NULL;

    memset(list, 0, sizeof(*list));
    df_freep(list_p);
}
