#include <int/list.h>


void df_list_check(const df_flf_t *flf, df_list_t const *list)
{
    DF_BT;

    df_check_null     (flf, "list pointer", list);
    df_check_signature(flf, "df_list", list->signature, df_list_SIGNATURE);

    if (list->nodes == 0)
    {
	if (list->tail != NULL)
	    dflog_xbt(flf, "nodes == 0 && tail != NULL");
	if (list->head != NULL)
	    dflog_xbt(flf, "nodes == 0 && head != NULL");
    }
    else // nodes > 0
    {
	if (list->tail == NULL)
	    dflog_xbt(flf, "nodes > 0 && tail == NULL");
	if (list->head == NULL)
	    dflog_xbt(flf, "nodes > 0 && head == NULL");

	if (list->nodes == 1 && list->tail != NULL && list->head != NULL)
	{
	    if (list->tail != list->head)
		dflog_xbt(flf, "nodes == 1 && head != tail");
	}
    }
}

void df_list_check_all(const df_flf_t *flf, df_list_t const *list) // returns error flag
{
    df_list_check(flf, list);

    size_t nodes = df_list_nodes(list);
    size_t count = 0;

    for (df_list_node_t *node = list->head; node != NULL; node = node->next, ++count)
	df_list_node_check(flf, node);

    if (count != nodes)
	dflog_xbt(flf, "nodes %zu != counted nodes %zu", nodes, count);
}
