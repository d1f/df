#include <int/list.h>


df_list_node_t *df_list_tail(df_list_t *list)
{
    DF_BT;

    df_list_check(DF_FLF, list);

    return list->tail;
}

df_list_node_t *df_list_head(df_list_t *list)
{
    DF_BT;

    df_list_check(DF_FLF, list);

    return list->head;
}
