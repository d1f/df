#include <int/list.h>


void df_list_node_check(const df_flf_t *flf, df_list_node_t const *node)
{
    DF_BT;

    df_check_null(flf, "node pointer", node);

    df_check_signature(flf, "df_list_node", node->signature, df_list_node_SIGNATURE);

    if (node->next != NULL)
    {
	df_check_signature(flf, "df_list_node", node->next->signature, df_list_node_SIGNATURE);

	if (node->next->prev == NULL)
	    dflog_xbt(flf, "node->next->prev is NULL");
	else if (node->next->prev != node)
	    dflog_xbt(flf, "node->next->prev does not points to this node");
    }

    if (node->prev != NULL)
    {
	df_check_signature(flf, "df_list_node", node->prev->signature, df_list_node_SIGNATURE);

	if (node->prev->next == NULL)
	    dflog_xbt(flf, "node->prev->next is NULL");
	else if (node->prev->next != node)
	    dflog_xbt(flf, "node prev->next does not points to this node");
    }
}
