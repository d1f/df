#include <df/list.h>
#include <df/error.h>
#include <df/log.h>
#include <df/check.h>
#include <df/bt.h>
#include <stdlib.h> // EXIT_SUCCESS EXIT_FAILURE
#include <limits.h> // INT_MAX


static int cmp(const void *left, const void *right)
{
    DF_BT;

    return *(const int *)left - *(const int *)right;
}

static void data_dtor(void *data)
{
    DF_BT;

    DF_CHECK_NULL("data pointer", data);
    int *count_p = data;
    *count_p = INT_MAX;
}

int main(int ac, char *av[])
{
    DF_BT;

    dflog_open("", DFLOG_STDERR | DFLOG_BASENAME);

    if (ac != 2)
	dferror(EXIT_FAILURE, 0, "Usage: %s count>=0", df_basename(av[0]));

    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    const int count_limit = atoi(av[1]);
    if (count_limit < 0)
	dferror(EXIT_FAILURE, 0, "count %d < 0", count_limit);


    df_list_t *list = df_list_create(cmp);
    df_list_check_all(DF_FLF, list);

    int count = 0;

    for (count = 0; count < count_limit; ++count)
    {
	df_list_node_t *node = df_list_node_create(&count, sizeof(count), data_dtor);
	df_list_node_check(DF_FLF, node);

	df_list_link(list, df_list_tail(list), node, DF_LIST_LINK_AFTER);
	df_list_check_all(DF_FLF, list);
    }

    df_list_check_all(DF_FLF, list);

    count = 0;
    DF_LIST_FOREACH_FORWARD(list, node)
    {
	int *ret = df_list_node_data(node);
	if (ret == NULL)
	    return EXIT_FAILURE;
	if (*ret != count)
	    dferror(EXIT_FAILURE, 0, "payload: %d != %d", *ret, count);

	++count;
    }

    df_list_delete(&list);

    return EXIT_SUCCESS;
}
