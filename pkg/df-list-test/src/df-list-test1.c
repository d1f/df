#include <df/list.h>
#include <df/error.h>
#include <df/log.h>
#include <df/bt.h>
#include <stdlib.h> // EXIT_SUCCESS EXIT_FAILURE


int main(int ac, char *av[])
{
    DF_BT;

    (void)ac;

    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    static const int signature = 0xdeadbeef;

    int payload = signature;
    df_list_node_t *node = df_list_node_create(&payload, sizeof(payload), NULL);
    df_list_node_check(DF_FLF, node);

    int *ret = df_list_node_data(node);
    if (ret == NULL)
	return EXIT_FAILURE;
    if (*ret != signature)
	dferror(EXIT_FAILURE, 0, "payload: 0x%x != 0x%x", *ret, signature);


    df_list_t *list = df_list_create(NULL);
    df_list_check_all(DF_FLF, list);

    size_t n = df_list_nodes(list);
    if (n != 0)
	dferror(EXIT_FAILURE, 0, "nodes: %zu != 0", n);

    df_list_link(list, NULL, node, DF_LIST_LINK_AFTER);
    df_list_check_all(DF_FLF, list);

    n = df_list_nodes(list);
    if (n != 1)
	dferror(EXIT_FAILURE, 0, "nodes: %zu != 1", n);


    df_list_node_t *node_r = df_list_unlink(list, node);
    df_list_node_check(DF_FLF, node_r);

    n = df_list_nodes(list);
    if (n != 0)
	dferror(EXIT_FAILURE, 0, "nodes: %zu != 0", n);

    df_list_check_all(DF_FLF, list);

    ret = df_list_node_data(node_r);
    if (ret == NULL)
	return EXIT_FAILURE;
    if (*ret != signature)
	dferror(EXIT_FAILURE, 0, "payload: 0x%x != 0x%x", *ret, signature);

    df_list_delete(&list);

    return EXIT_SUCCESS;
}
