#include <int/ht.h>


void *df_ht_entry_data(df_ht_entry_t *hte)
{
    DF_BT;

    df_ht_entry_check(DF_FLF, hte);

    return hte->data;
}
