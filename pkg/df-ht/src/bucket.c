#include <int/ht.h>


void df_ht_bucket_init(df_ht_bucket_t *b)
{
    DF_BT;

    b->signature  = df_ht_bucket_SIGNATURE;
    b->entries    = 0;
    b->entry_list = NULL;
}

void df_ht_bucket_destroy(df_ht_bucket_t *b)
{
    DF_BT;

    df_ht_bucket_check(DF_FLF, b);

    for (df_ht_entry_t *hte = b->entry_list, *hte_next = NULL;
	 hte != NULL; hte = hte_next)
    {
	df_ht_entry_check(DF_FLF, hte);

	hte_next = hte->next;
	df_ht_entry_delete(&hte);
    }
    b->entries    = 0;
    b->signature  = 0;
}
