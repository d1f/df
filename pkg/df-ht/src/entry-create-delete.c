#include <int/ht.h>


// key and data copied.
df_ht_entry_t *df_ht_entry_create(const char  *key,
				  const void *data, size_t data_size,
				  df_item_dtor_f data_dtor,
				  const df_hash_alg_t *hash_alg)
{
    DF_BT;

    df_hash_alg_check(DF_FLF, hash_alg);

    if (data_size == 0)
	dflog_xbt(DF_FLF, "data size is 0");

    df_ht_entry_t *hte = df_xzmalloc(sizeof(*hte) + data_size);
    hte->signature     = df_ht_entry_SIGNATURE;
    hte->key_length    = strlen(key);
    hte->key           = df_xmemdup(key, hte->key_length + 1);
    hash_alg->hash_len(&hte->key_hash, key, hte->key_length);

    hte->data_size     = data_size;
    hte->data_dtor     = data_dtor;
    memcpy(hte->data, data, data_size);

    return hte;
}

void df_ht_entry_delete(df_ht_entry_t **hte_p)
{
    DF_BT;

    df_ht_entry_t *hte = *hte_p;
    df_ht_entry_check(DF_FLF, hte);

    if (hte->data_dtor != NULL)
	hte->data_dtor(hte->data);

    df_freep(&hte->key);

    memset(hte, 0, sizeof(*hte) + hte->data_size);
    df_freep(hte_p);
}
