#include <int/ht.h>


static df_ht_entry_t *df_ht_search(df_ht_t *ht, const char *key, uint32_t key_hash)
{
    DF_BT;

    df_ht_check(DF_FLF, ht);

    DF_CHECK_NULL("key pointer", key);

    size_t ht_idx = key_hash % ht->buckets_num;

    df_ht_bucket_t *b = &ht->buckets[ht_idx];
    df_ht_bucket_check(DF_FLF, b);

    for (df_ht_entry_t *hte = b->entry_list; hte != NULL; hte = hte->next)
    {
	    df_ht_entry_check(DF_FLF, hte);

	    if (key_hash != hte->key_hash)
		continue;

	    if (strcmp(key, hte->key) == 0)
		return hte;
    }

    return NULL;
}

// returns found entry or NULL if entry with the key was not found.
df_ht_entry_t *df_ht_search_key(df_ht_t *ht, const char *key)
{
    DF_BT;

    uint32_t hash;
    ht->hash_alg->hash_str(&hash, key);
    return df_ht_search(ht, key, hash);
}

// returns found or added entry.
// *search_p = NULL if not found and added.
df_ht_entry_t *df_ht_search_add(df_ht_t *ht, df_ht_entry_t **search_p)
{
    DF_BT;

    df_ht_entry_t *search = *search_p;
    df_ht_entry_check(DF_FLF, search);

    ht->keys_tried_to_add++;

    df_ht_entry_t *found = df_ht_search(ht, search->key, search->key_hash);
    if (found != NULL)
    {
	ht->keys_rejected++;
	return found;
    }

    df_ht_bucket_t *b = &ht->buckets[ search->key_hash % ht->buckets_num ];

    // entry not found, link in the search entry
    search->next  = b->entry_list;
    b->entry_list = search;
    *search_p     = NULL;

    // stat
    if (b->entries == 0)
	ht->buckets_used++;
    b->entries++;
    if (ht->bucket_max_entries < b->entries)
	ht->bucket_max_entries = b->entries;
    ht->keys_added++;

    return search;
}
