#ifndef  DF_HT_INT_H
# define DF_HT_INT_H

# include <df/ht.h>
# include <df/bt.h>
# include <df/x.h>
# include <df/hash.h>
# include <df/error.h>
# include <df/log.h>
# include <df-ht-signatures.h>
# include <df/check.h>


struct df_ht_entry
{
    df_signature_t signature;
    df_ht_entry_t *next;

    char          *key;
    size_t         key_length; // strlen(key)
    uint32_t       key_hash;

    size_t         data_size;
    df_item_dtor_f data_dtor;
    intptr_t       data[0];
};

typedef struct df_ht_bucket
{
    df_signature_t signature;

    size_t         entries;
    df_ht_entry_t *entry_list;
} df_ht_bucket_t;

struct df_ht
{
    df_signature_t signature;

    const df_hash_alg_t *hash_alg;

    size_t keys_tried_to_add;
    size_t keys_added;
    size_t keys_rejected;
    size_t buckets_used;
    size_t bucket_max_entries;

    size_t         buckets_num;
    df_ht_bucket_t buckets[];
};


void df_ht_bucket_init   (df_ht_bucket_t       *b) ATTR(nonnull);
void df_ht_bucket_destroy(df_ht_bucket_t       *b) ATTR(nonnull);
void df_ht_bucket_check  (const df_flf_t *flf,
			  df_ht_bucket_t const *b) ATTR(nonnull);


#endif //DF_HT_INT_H
