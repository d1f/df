#include <int/ht.h>


df_ht_t *df_ht_create(size_t buckets, const df_hash_alg_t *hash_alg)
{
    DF_BT;

    df_hash_alg_check(DF_FLF, hash_alg);

    if (buckets < 2)
	dflog_xbt(DF_FLF, "table length < 2");

    df_ht_t *ht       = df_xzmalloc(sizeof(*ht) + buckets * sizeof(df_ht_bucket_t));
    ht->signature     = df_ht_SIGNATURE;
    ht->buckets_num   = buckets;
    ht->hash_alg      = hash_alg;

    for (size_t i = 0; i < buckets; ++i)
	df_ht_bucket_init(&ht->buckets[i]);

    return ht;
}

void df_ht_delete(df_ht_t **ht_p)
{
    DF_BT;

    df_ht_t *ht = *ht_p;
    df_ht_check(DF_FLF, ht);

    for (size_t i = 0; i < ht->buckets_num; ++i)
	df_ht_bucket_destroy(&ht->buckets[i]);

    memset(ht, 0, sizeof(*ht) + ht->buckets_num * sizeof(df_ht_bucket_t));
    df_freep(ht_p);
}
