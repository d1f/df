#include <int/ht.h>


df_ht_stat_t df_ht_stat(df_ht_t const *ht)
{
    DF_BT;

    df_ht_check(DF_FLF, ht);

    df_ht_stat_t stat;
    memset(&stat, 0, sizeof(stat));

    stat.buckets_all        = ht->buckets_num;
    stat.buckets_used       = ht->buckets_used;
    stat.keys_tried_to_add  = ht->keys_tried_to_add;
    stat.keys_added         = ht->keys_added;
    stat.keys_rejected      = ht->keys_rejected;
    stat.bucket_max_entries = ht->bucket_max_entries;
    stat.bucket_avg_entries = (float)ht->keys_added / ht->buckets_used;

    return stat;
}
