#include <int/ht.h>


void df_ht_entry_check(const df_flf_t *flf, df_ht_entry_t const *hte)
{
    DF_BT;

    df_check_null     (flf, "hash table entry pointer", hte);
    df_check_signature(flf, "df_ht_entry", hte->signature, df_ht_entry_SIGNATURE);
    df_check_null     (flf, "key pointer", hte->key);
}

void df_ht_bucket_check(const df_flf_t *flf, df_ht_bucket_t const *b)
{
    DF_BT;

    df_check_null     (flf, "hash table bucket pointer", b);
    df_check_signature(flf, "df_ht_bucket", b->signature, df_ht_bucket_SIGNATURE);

    if (b->entries > 0 && b->entry_list == NULL)
    {
	dflog_xbt(flf, "bucket entrie > 0 and entry_list == NULL");
    }
    if (b->entries == 0 && b->entry_list != NULL)
    {
	dflog_xbt(flf, "bucket entrie == 0 and entry_list != NULL");
    }
}

void df_ht_check(const df_flf_t *flf, df_ht_t const *ht)
{
    DF_BT;

    df_check_null     (flf, "hash table pointer", ht);
    df_check_signature(flf, "df_ht", ht->signature, df_ht_SIGNATURE);
    df_hash_alg_check (flf, ht->hash_alg);
}

void df_ht_check_all(const df_flf_t *flf, df_ht_t const *ht)
{
    DF_BT;

    df_ht_check(flf, ht);

    for (size_t bi = 0; bi < ht->buckets_num; ++bi)
    {
	const df_ht_bucket_t *b = &ht->buckets[bi];
	df_ht_bucket_check(flf, b);

	size_t entries = 0;
	for (df_ht_entry_t *hte = b->entry_list; hte != NULL; hte = hte->next)
	{
	    entries++;
	    df_ht_entry_check(flf, hte);

	    uint32_t key_hash;
	    ht->hash_alg->hash_str(&key_hash, hte->key);
	    if (key_hash != hte->key_hash)
	    {
		dflog_xbt(flf, "key_hash 0x%08x is not matched to key |%s|",
			  hte->key_hash, hte->key);
	    }
	}

	if (entries != b->entries)
	{
	    dflog_xbt(flf, "bucket entries %zu does not matched counted %zu",
		      b->entries, entries);
	}
    }
}
