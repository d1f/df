#ifndef  DF_HT_H
# define DF_HT_H

# include <df/defs.h>  // ATTR
# include <df/types.h> // df_item_dtor_f
# include <df/hash.h>
# include <stdbool.h>


        struct df_ht_entry;
typedef struct df_ht_entry df_ht_entry_t;

// key and data are copied.
df_ht_entry_t *df_ht_entry_create(const char *key,
				  const void *data, size_t data_size,
				  df_item_dtor_f data_dtor,
				  const df_hash_alg_t *hash_alg)
                                                            ATTR(nonnull(1,2,5), returns_nonnull);

void           df_ht_entry_delete(df_ht_entry_t **hte_p)    ATTR(nonnull);

void           df_ht_entry_check(const df_flf_t *flf,
				df_ht_entry_t const *hte)   ATTR(nonnull);

void          *df_ht_entry_data  (df_ht_entry_t  *hte)      ATTR(nonnull, returns_nonnull);


        struct df_ht;
typedef struct df_ht df_ht_t;

df_ht_t       *df_ht_create      (size_t buckets,
				  const df_hash_alg_t *hash_alg)
				                            ATTR(nonnull(2), returns_nonnull);
void           df_ht_delete      (df_ht_t **ht_p)           ATTR(nonnull);
void           df_ht_check       (const df_flf_t *flf,
				  df_ht_t const *ht)        ATTR(nonnull);
void           df_ht_check_all   (const df_flf_t *flf,
				  df_ht_t const *ht)        ATTR(nonnull);

// returns found entry or NULL if entry with the key was not found.
df_ht_entry_t *df_ht_search_key  (df_ht_t *ht,
				  const char *key)          ATTR(nonnull);

// returns found or added entry.
// *search_p = NULL if not found and added.
df_ht_entry_t *df_ht_search_add  (df_ht_t *ht,
				  df_ht_entry_t **search_p) ATTR(nonnull, returns_nonnull);

typedef struct
{
    size_t buckets_all;
    size_t buckets_used;

    size_t keys_tried_to_add;
    size_t keys_added;
    size_t keys_rejected;

    size_t bucket_max_entries;
    float  bucket_avg_entries;
} df_ht_stat_t;

df_ht_stat_t   df_ht_stat        (df_ht_t const *ht)        ATTR(nonnull);


#endif //DF_HT_H
