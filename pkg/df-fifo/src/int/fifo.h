#ifndef  DF_FIFO_INT_H
# define DF_FIFO_INT_H

# include <df/fifo.h>
# include <df/bt.h>
# include <df/defs.h>   // ATTR


struct df_fifo
{
    df_fifo_slot_dtor_f dtor;

    size_t slot_size;
    size_t slots;
    size_t used_slots;	// amount of messages in the queue

    char*  base;	// start of array
    char*  end;		// end of array (above of the last element)
    char*  head;	// free or filled slot at head of queue
    char*  tail;	// next empty slot at tail of queue
};

#define DF_FIFO_BASE(m)       ((      char*)((f) + 1))
#define DF_FIFO_BASE_CONST(m) ((const char*)((f) + 1))


static inline ATTR(nonnull)
bool is_empty(const df_fifo_t* f)
{
    return f->used_slots == 0;
}

static inline ATTR(nonnull)
bool is_full(const df_fifo_t* f)
{
    return f->used_slots >= f->slots;
}

static inline ATTR(nonnull)
size_t free_slots(const df_fifo_t* f)
{
    return f->slots - f->used_slots;
}


static inline ATTR(nonnull)
void next(df_fifo_t* f, char** slotp)
{
    if ( ( (*slotp) += f->slot_size ) >= f->end )
	*slotp = f->base;
}


#endif //DF_FIFO_INT_H
