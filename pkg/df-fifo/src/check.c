#include <int/fifo.h>

//FIXME: return malloced message string on error

static bool check_range(const df_fifo_t* f, const void* _slot)
{
    DF_BT;

    if (_slot == NULL)
	return true;

    const char *slot = _slot;

    if (slot < f->base)
	return true;

    if (slot >= f->end)
	return true;

    if ( ((slot - f->base) % f->slot_size) != 0)
	return true;

    return false;
}

// returns error flag. slot may be NULL
bool df_fifo_check(const df_fifo_t* f, const void* slot)
{
    DF_BT;

    if (f->slot_size == 0)
	return true;

    if (f->base != DF_FIFO_BASE_CONST(f))
	return true;

    if (f->end != f->base + f->slots * f->slot_size)
	return true;

    if (check_range(f, f->head))
	return true;

    if (check_range(f, f->tail))
	return true;

    if (f->head != f->tail)
	if (is_empty(f) || is_full(f))
	    return true;

    if (slot != NULL)
    {
	if (check_range(f, slot))
	    return true;
    }

    return false;
}
