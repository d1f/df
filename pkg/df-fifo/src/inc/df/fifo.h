#ifndef  DF_FIFO_H
# define DF_FIFO_H

# include <stddef.h> // size_t NULL
# include <stdbool.h>
# include <df/defs.h> // ATTR


struct df_fifo;
typedef struct df_fifo df_fifo_t;

// fifo slot destructor; arg is pointer to slot data in fifo.
typedef void (*df_fifo_slot_dtor_f)(void* slot) ATTR(nonnull);

// slot_size > 0
// slots may be 0
// dtor may be NULL
df_fifo_t* df_fifo_create    (size_t slot_size, size_t slots, df_fifo_slot_dtor_f dtor);

// desctructor called on each of unreleased slot
void       df_fifo_delete    (df_fifo_t **fp);

bool       df_fifo_is_empty  (const df_fifo_t* f) ATTR(nonnull);
bool       df_fifo_is_full   (const df_fifo_t* f) ATTR(nonnull);

size_t     df_fifo_slots_all (const df_fifo_t* f) ATTR(nonnull);
size_t     df_fifo_slots_used(const df_fifo_t* f) ATTR(nonnull);
size_t     df_fifo_slots_free(const df_fifo_t* f) ATTR(nonnull);
size_t     df_fifo_slot_size (const df_fifo_t* f) ATTR(nonnull);


// claim free tail slot and advance FIFO tail.
// returns claimed tail slot pointer or NULL if no free slots available.
void* df_fifo_put_tail   (df_fifo_t* f) ATTR(nonnull);

// returns pointer to filled head slot
// or NULL if there is no filled slots.
void* df_fifo_get_head   (df_fifo_t* f) ATTR(nonnull);

// release filled head slot and advance FIFO head
// returns true if there was head to release.
// head slot destructor called if supplied to df_fifo_create.
bool df_fifo_release_head(df_fifo_t* f) ATTR(nonnull);

// claim free tail slot and advance FIFO tail.
// release head slot if no space.
// head slot destructor called if supplied to df_fifo_create
// and head slot released.
// returns claimed tail slot pointer or NULL.
void* df_fifo_put_tail_release_head(df_fifo_t* f) ATTR(nonnull);


// returns next slot from supplied one in head to tail direction,
// NULL if supplied slot is tail.
// if _slot is NULL, return head.
void *df_fifo_next(df_fifo_t* f, void *slot) ATTR(nonnull(1));


// returns error flag. slot may be NULL
bool df_fifo_check(const df_fifo_t* f, const void* slot) ATTR(nonnull(1));


#endif //DF_FIFO_H
