#include <int/fifo.h>

bool df_fifo_is_empty(const df_fifo_t* f)
{
    DF_BT;

    return is_empty(f);
}

bool df_fifo_is_full(const df_fifo_t* f)
{
    DF_BT;

    return is_full(f);
}

size_t df_fifo_slots_all(const df_fifo_t* f)
{
    DF_BT;

    return f->slots;
}

size_t df_fifo_slots_used(const df_fifo_t* f)
{
    DF_BT;

    return f->used_slots;
}

size_t df_fifo_slots_free(const df_fifo_t* f)
{
    DF_BT;

    return free_slots(f);
}

size_t df_fifo_slot_size(const df_fifo_t* f)
{
    DF_BT;

    return f->slot_size;
}
