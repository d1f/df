#include <int/fifo.h>


static char * put_tail(df_fifo_t* f)
{
    DF_BT;

    char* slot = f->tail;
    next(f, &f->tail);
    ++f->used_slots;
    return slot;
}

// claim free tail slot and advance FIFO tail.
// returns claimed tail slot pointer or NULL if no free slots available.
void* df_fifo_put_tail(df_fifo_t* f)
{
    DF_BT;

    return !is_full(f) ? put_tail(f) : NULL;
}

// returns pointer to filled head slot
// or NULL if there is no filled slots.
void* df_fifo_get_head(df_fifo_t* f)
{
    DF_BT;

    return f->used_slots > 0 ? f->head : NULL;
}

static void release_head(df_fifo_t* f)
{
    DF_BT;

    if (f->dtor != NULL)
	f->dtor(f->head);

    next(f, &f->head);
    f->used_slots--;
}

// release filled head slot and advance FIFO head
// returns true if there was head to release.
// head slot destructor called if supplied to df_fifo_create.
bool df_fifo_release_head(df_fifo_t* f)
{
    DF_BT;

    if ( !df_fifo_is_empty(f) )
    {
	release_head(f);
	return true;
    }
    else
	return false;
}

// claim free tail slot and advance FIFO tail.
// release head slot if no space.
// head slot destructor called if supplied to df_fifo_create
// and head slot released.
// returns claimed tail slot pointer or NULL.
void* df_fifo_put_tail_release_head(df_fifo_t* f)
{
    DF_BT;

    if (f->slots > 0)
    {
	if (is_full(f))
	    release_head(f);

	return put_tail(f);
    }
    else
	return NULL;
}
