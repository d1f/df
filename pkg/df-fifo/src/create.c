#include <int/fifo.h>
#include <stdlib.h> // malloc NULL
#include <string.h> // memset

// slot_size > 0
// slots may be 0
// dtor may be NULL
df_fifo_t *df_fifo_create(size_t slot_size, size_t slots, df_fifo_slot_dtor_f dtor)
{
    DF_BT;

    if (slot_size == 0)
	return NULL;

    df_fifo_t* f = malloc(sizeof(df_fifo_t) + slot_size * slots);
    if (f != NULL)
    {
	memset(f, 0, sizeof(df_fifo_t));
	f->dtor      = dtor;
	f->slots     = slots;
	f->slot_size = slot_size;
	f->base      = DF_FIFO_BASE(f);
	f->end       = f->base + slots * f->slot_size;
	f->head      = f->tail = f->base;
    }

    return f;
}
