#include <int/fifo.h>

// returns next slot from supplied one in head to tail direction,
// NULL if supplied slot is tail.
// if _slot is NULL, return head.
void *df_fifo_next(df_fifo_t* f, void *_slot)
{
    DF_BT;

    if (_slot == NULL)
	return df_fifo_get_head(f);

    char *slot = _slot;

    char *slot_next = slot;
    next(f, &slot_next);

    if (f->tail > f->head)
    {
	if (slot >= f->head)
	{
	    if (slot < f->tail)
	    {
		if (slot_next == f->tail)
		    return NULL;

		return slot_next;
	    }
	    else
		return NULL;
	}
	else
	{
	    //FIXME: error message
	    return NULL;
	}
    }
    else if (f->tail == f->head)
    {
	if (is_empty(f))
	{
	    return NULL;
	}
	else // is_full
	{
	    if (slot == f->head) // first call after get_head
		return slot_next;

	    if (slot_next == f->head) // slot == pre-head
		return NULL;

	    return slot_next;
	}
    }
    else // tail < head, slots buffer wrapped
    {
	if (slot_next >= f->head)
	    return slot_next;

	if (slot < f->tail)
	{
	    if (slot_next == f->tail)
		return NULL;

	    return slot_next;
	}

	return NULL; // slot >= tail
    }

    return NULL;
}
