#include <int/fifo.h>
#include <stdlib.h> // free NULL
#include <string.h> // memset
#include <df/x.h>   // df_freep

// desctructor called on each of unreleased slot
void df_fifo_delete(df_fifo_t **fp)
{
    DF_BT;

    if (fp == NULL)
	return;

    df_fifo_t *f = *fp;
    if (f == NULL)
	return;

    if (f->dtor != NULL)
    {
	while (df_fifo_release_head(f))
	    ;
    }

    memset(f, 0, sizeof(df_fifo_t) + f->slots * f->slot_size);

    df_freep(fp);
}
