#define _GNU_SOURCE

#include <df/hex.h>
#include <df/log.h>
#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename strcmp

int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "\nUsage: %s 'string'\n\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    const char *src = av[1];
    //printf("src: |%s|\n", src);

    char hex[strlen(src) * 2 + 1];
    df_str2hex(hex, src);
    //printf("hex: |%s|\n", hex);

    char asc[sizeof(hex)];
    if (df_hex2str(asc, hex))
	return EXIT_FAILURE;
    //printf("asc: |%s|\n", asc);

    if (strcmp(src, asc) != 0)
	dflog(LOG_ERR, "Test failed");

    return EXIT_SUCCESS;
}
