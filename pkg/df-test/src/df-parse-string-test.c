#define _GNU_SOURCE

#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename
#include <df/parse-string.h>


// returns error flag 'key not recognized'
static bool cb(char *out, size_t out_lomit, void *user_data, const char *key)
{
    (void)out_lomit;
    (void)user_data;

#if 0
    (void)out;
    printf("key: |%s|\n", key);
#else
    strcat(out, "[");
    strcat(out, key);
    strcat(out, "]");
#endif

    return false;
}

int main(int ac, char *av[])
{
    if (ac != 4)
    {
	printf("\nUsage: %s 'left_delim' 'right_delim' 'string'\n\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    char  left_delim = av[1][0];
    char right_delim = av[2][0];
    char        *src = av[3];

    char out[strlen(src)*2]; out[0] = '\0';

    bool rc = df_parse_string(out, sizeof(out), cb, NULL,
			      src, left_delim, right_delim);

    printf("out: |%s|\n", out);
    return rc ? EXIT_FAILURE : EXIT_SUCCESS;
}
