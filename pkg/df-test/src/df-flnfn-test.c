#include <df/log.h>
#include <df/types.h>


static void pr(const df_flf_t *flf)
{
    dflog(LOG_ERR, "flf: %p, file: %s, line: %zu, func: %s", flf, flf->file, flf->line, flf->func);
}

int main(int ac, char *av[])
{
    (void)ac;
    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    pr(DF_FLF);
    //
    pr(DF_FLF);
    return 0;
}
