#include <df/log.h>
#include <df/error.h>
#include <stdlib.h> // EXIT_*
#include <stdio.h>
#include <unistd.h> // daemon


int main(int ac, char *av[])
{
    (void)ac;
    dflog_open(av[0], DFLOG_PID | DFLOG_SYS | DFLOG_BASENAME);
    dflog(LOG_INFO, "start");
    dflog_stdio_hook();

    fprintf(stdout, "stdout before daemon\n");
    fprintf(stderr, "stderr before daemon\n");

    if (daemon(/*nochdir*/0, /*noclose*/1) < 0)
	dferror(EXIT_FAILURE, errno, "Can't daemonize");

    dflog_open(av[0], DFLOG_PID | DFLOG_SYS | DFLOG_BASENAME);

    fprintf(stdout, "stdout after daemon\n");
    fprintf(stderr, "stderr after daemon\n");
    fprintf(stdout, "stdout ------------------------\nstdout -----------------------------------------\n");
    fprintf(stderr, "stderr ------------------------\nstderr -----------------------------------------\n");

#if 0
    dflog_stdio_unhook();

    fprintf(stdout, "unhooked stdout\n");
    fprintf(stderr, "unhooked stderr\n");
#endif

    dflog(LOG_INFO, "end");
    return EXIT_SUCCESS;
}
