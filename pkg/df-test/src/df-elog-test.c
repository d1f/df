#include <df/elog.h>
#include <stdio.h>
#include <stdlib.h> // EXIT_*
#include <errno.h>


extern const char *__progname;

int main(int ac, char *av[])
{
    if (ac < 2 || ac > 3)
    {
	fprintf(stderr, "Usage: %s \"message\" [ident]\n", __progname);
	return EXIT_FAILURE;
    }

    if (ac == 3)
	__progname = av[2];

    errno = ENOMEM;
    df_elog (av[1]);

    return EXIT_SUCCESS;
}
