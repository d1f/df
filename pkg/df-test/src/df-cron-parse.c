#define _GNU_SOURCE

#include <df/cron.h>
#include <df/error.h>
#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename strcmp

int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "\nUsage: %s 'string'\n\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    char *src = strdup(av[1]);
    //printf("src: |%s|\n", src);

    static const size_t out_len = 32;
    bool out[out_len];
    df_cron_parse_field(out, out_len, src, 0);

    for (size_t i = 0; i <= out_len-1; ++i)
	printf("%d ", out[i]);
    printf("\n");

    return EXIT_SUCCESS;
}
