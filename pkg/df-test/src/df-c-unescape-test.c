#define _GNU_SOURCE

#include <df/c-escape.h>

#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename

int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "\nUsage: %s 'string'\n\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    const char *src = av[1];

    char dst[strlen(src)+1];
    df_c_unescape(dst, src);

    fputs(dst, stdout);

    return EXIT_SUCCESS;
}
