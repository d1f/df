#include <stdio.h>  // printf
#include <stdlib.h> // strtoull EXIT_SUCCESS EXIT_FAILURE
#include <df/basename.h>
#include <df/ull2str.h>


int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "\nUsage: %s ullnum\n\n", df_basename(av[0]));
	return EXIT_FAILURE;
    }

    ullong num = strtoull(av[1], NULL, 10);

    df_free_list_t *fl = NULL;

    printf("%15s\n%15s\n%15s\n%15s\n",
	   df_ull2str(num     , ',', &fl),
	   df_ull2str(num*10  , ',', &fl),
	   df_ull2str(num*100 , ',', &fl),
	   df_ull2str(num*1000, ',', &fl)
	  );

    df_free_list_free(&fl);

    return EXIT_SUCCESS;
}
