#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <unistd.h> // unlink
#include <df/whitespace.h>


int main(int ac, char *av[])
{
    if (ac != 3)
    {
	fprintf(stderr, "Usage: %s infile outfile\n", av[0]);
	return EXIT_FAILURE;
    }

    const char *fnin  = av[1];
    const char *fnout = av[2];

    if (strcmp(fnin, fnout) == 0)
	error(EXIT_FAILURE, errno, "Input and output file names are the same");

    FILE *in = fopen(fnin, "r");
    if (in == NULL)
	error(EXIT_FAILURE, errno, "Can't open input file %s", fnin);

    FILE *out = fopen(fnout, "w");
    if (out == NULL)
	error(EXIT_FAILURE, errno, "Can't open output file %s", fnout);

    while (1)
    {
	char *line = NULL;
	size_t n = 0;
	ssize_t rc = getline(&line, &n, in);
	if (rc < 0)
	{
	    if (ferror(in))
	    {
		fclose(out);
		unlink(fnout);
		error(EXIT_FAILURE, errno, "Error reading %s", fnin);
	    }
	    if (feof(in))
		break;
	}

	df_strip_tail_whitespace(line);

	if (fputs(line, out) < 0)
	{
	    fclose(out);
	    unlink(fnout);
	    error(EXIT_FAILURE, errno, "Error writing %s", fnout);
	}

	free(line); line = NULL;

	if (fputc('\n', out) < 0)
	{
	    fclose(out);
	    unlink(fnout);
	    error(EXIT_FAILURE, errno, "Error writing %s", fnout);
	}
    }

    return EXIT_SUCCESS;
}
