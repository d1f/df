#include <df/bt.h>
#include <df/log.h>
#include <df/error.h>
#include <df/x.h>
#include <stdlib.h> // EXIT_*
#include <string.h> // strcmp


enum TYPE { FAULT, FINISH };

static int f4(enum TYPE type)
{
    DF_BT;

    if (type == FINISH)
    {
	df_bt_exit(EXIT_FAILURE);
    }
    else
    {
	volatile int* ptr = NULL;
	*ptr = 0;
    }

    return type;
}

static int f3(enum TYPE type)
{
    DF_BT;

    int v = f4(type);

    return v;
}

static int f2(enum TYPE type)
{
    DF_BT;

    int v = f3(type);

    return v;
}

static int f1(enum TYPE type)
{
    DF_BT;

    int v = f2(type);
    dflog(LOG_INFO, "%s(): val: %d", __func__, v);

    return v;
}

static int f0(enum TYPE type)
{
    DF_BT;

    int v = f1(type);
    dflog(LOG_INFO, "%s(): val: %d", __func__, v);

    return v;
}

int main(int ac, char *av[])
{
    DF_BT;

    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    if (ac != 2)
    {
	dflog(LOG_ERR, "Usage: %s fault | finish", df_basename(av[0]));
	return EXIT_FAILURE;
    }

    enum TYPE type = FAULT;
    if (strcmp(av[1], "fault") == 0)
	type = FAULT;
    else if (strcmp(av[1], "finish") == 0)
	type = FINISH;
    else
	dferror(EXIT_FAILURE, 0, "Unknown terminate type: %s", av[1]);

    dflog(LOG_INFO, "%s(): start", __func__);

    df_bt_xset_signals(df_bt_siginfo_handler);

    int v = f0(type);
    dflog(LOG_INFO, "%s(): val: %d", __func__, v);

    dflog(LOG_INFO, "%s(): exit", __func__);
    return EXIT_SUCCESS;
}
