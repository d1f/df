#include <df/log.h>
#include <df/shcfg.h>
#include <stdlib.h> // EXIT_*
#include <stdio.h>


static void found(df_da_t *da, ssize_t idx)
{
    if (idx >= 0)
    {
	dflog(LOG_INFO, "found idx: %2zd, key: %s, val: %s", idx,
	       ((df_kv_t*)df_da_get_item(da, idx))->key.ccPtr,
	       ((df_kv_t*)df_da_get_item(da, idx))->val.ccPtr
	      );
	    //idx = df_da_bsearch(da, &kv);
    }
    else
    {
	dflog(LOG_INFO, "Not found");
    }
}

int main(int ac, char *av[])
{
    if (ac < 3 || ac > 4)
    {
	fprintf(stderr, "\nUsage: %s infile|- outfile|- [search-key]\n\n", av[0]);
	return EXIT_FAILURE;
    }

    char *key = ac == 4 ? av[3] : NULL;

    df_shcfg_t *cfg = df_shcfg_create(av[1], '=');
    if (cfg == NULL)
	return EXIT_FAILURE;

    if (key != NULL)
    {
	dflog(LOG_INFO, "\n");

	// returns array of df_kv_t
	df_da_t *da = df_shcfg_da(cfg);

	dflog_("lsearch %s: ", key);
	ssize_t idx = df_da_lsearch(da, &((df_kv_t){ .key.ccPtr = key }) ); // returns found index or <0
	found(da, idx);

	dflog_("bsearch %s: ", key);
	idx = df_da_bsearch(da, &((df_kv_t){ .key.ccPtr = key }));
	found(da, idx);
    }

    dflog(LOG_INFO, "\n");
    df_shcfg_write(cfg, av[2], '=', '"');
    dflog(LOG_INFO, "\n");


    df_shcfg_delete(&cfg);

    return EXIT_SUCCESS;
}
