#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h> // atoi, EXIT_*
#include <stdint.h> // uint64_t
#include <inttypes.h> // PRIu64
#include <string.h> // basename

#include <df/fifo.h>

static size_t slot_size;

static void dtor(void *slot)
{
    uint64_t data = 0LLU;
    memcpy(&data, slot, slot_size);

    printf("dtor: slot address: %p, data: %" PRIu64  "\n", slot, data);
}

static const char *boolstr(bool value)
{
    return value ? "True" : "False";
}

static void fifo_stat(df_fifo_t* f)
{
    printf("\nslots: all: %zu, used: %zu, free: %zu, empty: %s, full: %s\n",
	   df_fifo_slots_all(f), df_fifo_slots_used(f), df_fifo_slots_free(f),
	   boolstr(df_fifo_is_empty(f)), boolstr(df_fifo_is_full(f))
	  );
}

static void foreach(df_fifo_t* f)
{
    for (void *slot = NULL; (slot = df_fifo_next(f, slot)) != NULL; )
    {
	uint64_t data = 0LLU;
	memcpy(&data, slot, df_fifo_slot_size(f));
	printf("\tslot: %p, data: %" PRIu64 "\n", slot, data);
    }
}

void fifo_check(df_fifo_t* f)
{
    printf("Checking FIFO: ");
    if (df_fifo_check(f, NULL))
	printf("Failed!\n");
    else
	printf("OK\n");
}

int main(int ac, char *av[])
{
    if (ac != 3)
    {
	fprintf(stderr, "\nUsage: %s fifo-size<1..%zu> fifo-slots\n\n",
		basename(av[0]), sizeof(uint64_t));
	return EXIT_FAILURE;
    }

    int size  = atoi(av[1]);
    int slots = atoi(av[2]);

    if (size < 1 || size > (ssize_t)sizeof(uint64_t))
    {
	fprintf(stderr, "%s: size %d is out of range\n",
		basename(av[0]), size);
	return EXIT_FAILURE;
    }

    slot_size = size;

    if (slots < 0)
    {
	fprintf(stderr, "%s: slots %d < 0\n", basename(av[0]), slots);
	return EXIT_FAILURE;
    }


    df_fifo_t* f = df_fifo_create(size, slots, dtor);
    printf("fifo created at %p\n", f);

    fifo_check(f);
    fifo_stat(f);

    printf("\nFor each slots from head to tail:\n");
    foreach(f);

    printf("\nfilling slots with values %d..%d\n", 0, (slots/2)-1);
    size_t i;
    uint64_t data;
    for (i = 0; i < (size_t)slots/2; ++i)
    {
	data = i;

	// claim free tail slot and advance FIFO tail
	// returns claimed tail slot pointer or NULL
	void* slot = df_fifo_put_tail(f);
	memcpy(slot, &data, size);
    }

    fifo_check(f);
    fifo_stat(f);
    printf("\nFor each slots from head to tail:\n");
    foreach(f);

    printf("\nfilling slots with values %zu..%d\n", i, slots-1);
    for (; i < (size_t)slots; ++i)
    {
	data = i;

	// claim free tail slot and advance FIFO tail
	// returns claimed tail slot pointer or NULL
	void* slot = df_fifo_put_tail(f);
	memcpy(slot, &data, size);
    }

    fifo_check(f);
    fifo_stat(f);
    printf("\nFor each slots from head to tail:\n");
    foreach(f);

    void* slot = df_fifo_put_tail(f);
    printf("\nGetting yet another slot: %p\n", slot);

    printf("\nReleasing head:\n");
    bool released = df_fifo_release_head(f);
    printf("Released: %s\n", boolstr(released));

    fifo_check(f);
    fifo_stat(f);

    printf("\nFor each slots from head to tail:\n");
    foreach(f);

    slot = df_fifo_put_tail(f);
    printf("\nGetting yet another slot: %p\n", slot);
    printf("slot: %p\n", slot);
    if (slot != NULL)
    {
	data = i;
	memcpy(slot, &data, size);
	printf("putted %"PRIu64"\n", data);
    }

    fifo_check(f);
    fifo_stat(f);

    printf("\nFor each slots from head to tail:\n");
    foreach(f);

    data = ++i;
    printf("\nPutting data %"PRIu64" with release head if no space:\n", data);
    slot = df_fifo_put_tail_release_head(f);
    printf("slot: %p, %s\n", slot, (slot!=NULL) ? "OK" : "Fail");
    if (slot != NULL)
	memcpy(slot, &data, size);

    fifo_check(f);
    fifo_stat(f);

    printf("\nFor each slots from head to tail:\n");
    foreach(f);

    printf("\ndeleting fifo:\n");
    df_fifo_delete(&f);
    return EXIT_SUCCESS;
}
