#define _GNU_SOURCE

#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename
#include <df/filename-template.h>
#include <df/error.h>


int main(int ac, char *av[])
{
    if (ac != 8)
    {
	printf("\nUsage: %s prefix suffix seqnum left-delim right-delim template ru|en\n\n",
	       basename(av[0]));
	return EXIT_FAILURE;
    }

    const char *prefix     =      av[1];
    const char *suffix     =      av[2];
    size_t      seqnum     = atoi(av[3]);
    const char  left_delim =      av[4][0];
    const char right_delim =      av[5][0];
    const char *template   =      av[6];
    const char *lang_str   =      av[7];

    df_locale_lang_t lang ;
    if (df_locale_lang_parse(&lang, lang_str))
	return EXIT_FAILURE;

    char out[NAME_MAX]; out[0] = '\0';

    df_filename_template(out, prefix, suffix, &seqnum,
			 left_delim, right_delim, template, lang, NULL);

    printf("out: |%s|\n", out);
    return EXIT_SUCCESS;
}
