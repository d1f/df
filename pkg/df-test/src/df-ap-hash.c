#include <df/ap-hash.h>
#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*


int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "Usage: %s string\n", av[0]);
	return EXIT_FAILURE;
    }

    uint32_t hash;
    df_ap_hash_str(&hash, av[1]);
    printf("%08x\n", hash);
    return EXIT_SUCCESS;
}
