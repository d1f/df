#include <df/date-format.h> // df_date_format_list
#include <stdlib.h> // EXIT_SUCCESS

int main(void)
{
    // output format description list to stdout
    df_date_format_list("<", ">", " : ", ",\n", DF_LOCALE_ENG);

    return EXIT_SUCCESS;
}
