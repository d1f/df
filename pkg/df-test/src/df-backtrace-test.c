#include <stdlib.h> // EXIT_*
#include <df/backtrace.h>

void f1(void) { df_backtrace(); }
void f2(void) { f1(); }
void f3(void) { f2(); }
void f4(void) { f3(); }
void f5(void) { f4(); }

int main(int ac, char *av[])
{
    (void)ac; (void)av;

    f5();

    return EXIT_SUCCESS;
}
