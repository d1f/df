#define _GNU_SOURCE

#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename
#include <df/date-format.h>
#include <df/error.h>


int main(int ac, char *av[])
{
    if (ac != 3)
    {
	printf("\nUsage: %s ru|en 'format-string'\n\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    const char  left_delim = '<';
    const char right_delim = '>';

    const char *lang_str = av[1];
    char            *fmt = av[2];

    df_locale_lang_t lang;
    if (df_locale_lang_parse(&lang, lang_str))
	return EXIT_FAILURE;

    char out[strlen(fmt)*3]; out[0] = '\0';


    // returns error flag
    bool rc = df_date_format(out, sizeof(out),
			     fmt, left_delim, right_delim, lang, NULL);

    printf("%s\n", out);
    return rc ? EXIT_FAILURE : EXIT_SUCCESS;
}
