PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


 STD_CFLAGS += -O0
 STD_CFLAGS += -pthread
STD_LDFLAGS += -pthread
STD_LDFLAGS += $(tc_SYSROOT)/lib/libpthread.a

dflibs = df-pthread-hooks df df-pthread

BUILD_TYPE     = MULTI_SRC_BIN
BIN_NAME       = $(PKG)
LIBS           = $(dflibs)
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call tstamp_f,install,$(dflibs))

#MK_VARS += Q=
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(INSTALL_ROOT_CMD_DEFAULT)


include $(RULES)
