#include <pthread.h>
#include <stdlib.h> // EXIT_*


int main(int ac, char *av[])
{
    (void)ac; (void)av;

    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);

    pthread_mutex_lock(&mutex);
    pthread_mutex_lock(&mutex);
    pthread_mutex_unlock(&mutex);
    pthread_mutex_unlock(&mutex);

    return EXIT_SUCCESS;
}
