#include <df/ht.h>
#include <df/ap-hash.h>
#include <df/hash-rand8.h>
#include <df/error.h>
#include <df/log.h>
#include <df/check.h>
#include <df/bt.h>
#include <df/x.h>   // df_freep
#include <stdlib.h> // EXIT_SUCCESS EXIT_FAILURE
#include <stdio.h>  // getline
#include <string.h> // strrchr


int main(int ac, char *av[])
{
    DF_BT;

    dflog_open(NULL, DFLOG_STDERR | DFLOG_BASENAME);

    if (ac != 3)
	dferror(EXIT_FAILURE, 0, "Usage: %s hash-table-buckets hash-alg(ap|rand8) < stdin-lines",
		df_basename(av[0]));

    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    ssize_t buckets = atoi(av[1]);
    if (buckets < 2)
	dferror(EXIT_FAILURE, 0, "buckets %zd < 2", buckets);

    const df_hash_alg_t *hash_alg = NULL;
    if (strcmp(av[2], "ap") == 0)
	hash_alg = &df_ap_hash_alg;
    else if (strcmp(av[2], "rand8") == 0)
	hash_alg = &df_hash_rand8_alg;
    else
	dferror(EXIT_FAILURE, 0, "Unknown hash properties supplied: %s", av[2]);

    if (hash_alg->buckets_max != 0 &&
	hash_alg->buckets_max < (size_t)buckets)
	buckets = hash_alg->buckets_max;

    df_ht_t *ht = df_ht_create(buckets, hash_alg);
    df_ht_check_all(DF_FLF, ht);

    size_t data = 0; // lines counter

    while (1)
    {
	char *lineptr = NULL;
	size_t n = 0;
	ssize_t rc = getline(&lineptr, &n, stdin);
	if (rc < 0)
	{
	    if (ferror(stdin))
		dferror(EXIT_FAILURE, errno, "Error on stdin");
	    if (feof(stdin))
		break;
	}

	data++; // lines counter

	char *nl = strrchr(lineptr, '\n');
	if (nl != NULL)
	    *nl = 0; // strip new line
	df_ht_entry_t *new_hte = df_ht_entry_create(lineptr, &data, sizeof(data), NULL, hash_alg);

	// returns found or added entry.
	// *search_p = NULL if not found and added.
	df_ht_search_add(ht, &new_hte);
	if (new_hte != NULL)
	{
	    dflog(LOG_INFO, "Line %zu |%s| already exist", data, lineptr);
	    df_ht_entry_delete(&new_hte);
	}

	df_freep(&lineptr);
    }

    df_ht_stat_t stat = df_ht_stat(ht);
    dflog(LOG_INFO,
	  "Lines: %zu, Buckets all/used: %zu/%zu, "
	  "Keys: tried/added/rejected: %zu/%zu/%zu, "
	  "Max/Avg entries in a used bucket: %zu/%f",
	  data, stat.buckets_all, stat.buckets_used,
	  stat.keys_tried_to_add, stat.keys_added, stat.keys_rejected,
	  stat.bucket_max_entries, stat.bucket_avg_entries);

    df_ht_check_all(DF_FLF, ht);
    df_ht_delete(&ht);

    return EXIT_SUCCESS;
}
