#include <int/mem.h>

void df_mem_realloc_nocheck(df_mem_t *m, size_t size)
{
    DF_BT;

    void *data = df_xrealloc(m->data, size);
    m->data = data;
    m->size = size;
}
