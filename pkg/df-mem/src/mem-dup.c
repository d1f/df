#include <int/mem.h>
#include <string.h> // memcpy

// return xmalloc'ed data copy
void *df_mem_dup(const df_mem_t *m)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    DF_CHECK_NULL("data pointer", m->data);

    return df_xmemdup(m->data, m->size);
}
