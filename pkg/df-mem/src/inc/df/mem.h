#ifndef  DF_MEM_H
# define DF_MEM_H

# include <stddef.h>  // size_t NULL
# include <df/defs.h> // ATTR, returns_nonnull


        struct df_mem;
typedef struct df_mem df_mem_t;

df_mem_t *df_mem_create     (size_t size, void const *src)  /* args maybe 0 */         ATTR(returns_nonnull);
df_mem_t *df_mem_create_take(size_t size, void      **srcp) /* args maybe 0 */         ATTR(returns_nonnull);

void      df_mem_delete     (df_mem_t **mp);     /* mp, *mp may be NULL */
void      df_mem_free       (df_mem_t  *m)       /* free data */                       ATTR(nonnull);

size_t    df_mem_size       (df_mem_t const *m)                                        ATTR(nonnull);
void     *df_mem_data       (df_mem_t       *m)                                        ATTR(nonnull);

void      df_mem_realloc    (df_mem_t *m, size_t size) /* size may be 0 */             ATTR(nonnull);
void      df_mem_chop_nul   (df_mem_t *m)                                              ATTR(nonnull);

// returns new size
size_t    df_mem_append_char(df_mem_t *m, char c)                                      ATTR(nonnull);
size_t    df_mem_append_data(df_mem_t *m,                const void *src, size_t size) ATTR(nonnull);
size_t    df_mem_insert_at  (df_mem_t *m, size_t offset, const void *src, size_t size) ATTR(nonnull);
size_t    df_mem_cut_at     (df_mem_t *m, size_t offset,                  size_t size) ATTR(nonnull);

void     *df_mem_dup        (df_mem_t const *m)  /* return xmalloc'ed data copy */     ATTR(nonnull,returns_nonnull);
void     *df_mem_take       (df_mem_t      **mp) /* return xmalloc'ed data, *mp deleted */ ATTR(nonnull,returns_nonnull);


#endif // DF_MEM_H
