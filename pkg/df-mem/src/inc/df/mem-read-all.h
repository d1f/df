#ifndef  DF_MEM_READ_ALL_H
# define DF_MEM_READ_ALL_H

# include <df/mem.h>
# include <df/defs.h>


df_mem_t *df_mem_read_all(int fd) ATTR(returns_nonnull);


#endif	//DF_MEM_READ_ALL_H
