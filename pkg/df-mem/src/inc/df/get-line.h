/*
 read line from FILE* to dynamically malloc'ed buffer
 */

#ifndef  DF_GET_LINE_H
# define DF_GET_LINE_H

#include <stdio.h>
#include <df/defs.h>


/*
 Reads line from FILE* to dynamically malloc'ed buffer.
 Returns: 0 on success, >0 - EOF, <0 - -errno.
*/
int df_get_line(FILE* f, char* *lineptr) ATTR(nonnull);


#endif	/* DF_GETLINE_H */
