#include <int/mem.h>

df_mem_t *df_mem_create(size_t size, const void *src) /* args maybe 0 */
{
    DF_BT;

    df_mem_t *m  = df_xmalloc(sizeof(df_mem_t));
    m->signature = df_mem_SIGNATURE;
    m->data      = df_xmalloc(size);
    m->size      = size;
    if (src != NULL)
	memcpy(m->data, src, size);
    else
	memset(m->data, 0, size);

    return m;
}

df_mem_t *df_mem_create_take(size_t size, void **srcp) /* args maybe 0 */
{
    DF_BT;

    df_mem_t *m  = df_xmalloc(sizeof(df_mem_t));
    m->signature = df_mem_SIGNATURE;
    m->size      = size;

    if (srcp != NULL)
    {
	if (*srcp != NULL)
	{
	    m->data      = *srcp;
	    *srcp        = NULL;
	}
	else
	{
	    m->data      = df_xzmalloc(size);
	}
    }
    else
    {
	m->data      = df_xzmalloc(size);
    }

    return m;
}
