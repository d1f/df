#include <df/mem-read-all.h>
#include <df/error.h>
#include <df/bt.h>
#include <unistd.h> // read

#define BUFSIZE (4096 - sizeof(void*))

df_mem_t *df_mem_read_all(int fd)
{
    DF_BT;

    df_mem_t *m = df_mem_create(0, NULL);

    for (;;)
    {
	size_t old_size = df_mem_size(m);
	df_mem_realloc(m, old_size + BUFSIZE);
	ssize_t rc = read(fd, (char*)df_mem_data(m) + old_size, BUFSIZE);
	if (rc > 0)
	{
	    if ((size_t)rc < BUFSIZE)
		df_mem_realloc(m, old_size + rc); // shrink to usable size
	}
	else if (rc == 0) // EOF
	{
	    df_mem_realloc(m, old_size); // shrink to usable size
	    break;
	}
	else // rc < 0
	    dferror_bt(EXIT_FAILURE, errno, "Cant read %d", fd);
    }

    return m;
}
