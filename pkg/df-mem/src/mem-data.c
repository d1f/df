#include <int/mem.h>

void *df_mem_data(df_mem_t *m)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    return m->data;
}
