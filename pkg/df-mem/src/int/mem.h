#ifndef  DF_MEM_INT_H
# define DF_MEM_INT_H

# include <df/mem.h>
# include <df/x.h>
# include <df/bt.h>
# include <df/check.h>
# include <df-mem-signatures.h>

struct df_mem
{
    df_signature_t signature;

    size_t size;
    void  *data;
};


void df_mem_realloc_nocheck(df_mem_t *m, size_t size) /* size mey be 0 */ ATTR(nonnull);

#define CHECK_NULL(m) DF_CHECK_NULL("df_mem_t pointer", m)
#define CHECK_SIGNATURE(m) DF_CHECK_SIGNATURE(df_mem, m)


#endif //DF_MEM_INT_H
