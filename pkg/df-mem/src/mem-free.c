#include <int/mem.h>


// free data
void df_mem_free(df_mem_t *m)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    memset(m->data, ~0, m->size);
    df_freep(&m->data);
    m->size = 0;
}
