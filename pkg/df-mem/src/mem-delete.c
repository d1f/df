#include <int/mem.h>

// mp, *mp may be NULL.
void df_mem_delete(df_mem_t **mp)
{
    DF_BT;

    if (mp == NULL)
	return;

    df_mem_t *m = *mp;
    if (m == NULL)
	return;

    CHECK_SIGNATURE(m);

    memset(m->data, ~0, m->size);
    df_freep(&m->data);

    memset(m, ~0, sizeof(*m));
    df_freep(mp);
}
