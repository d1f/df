#include <int/mem.h>

size_t df_mem_size(const df_mem_t *m)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    return m->size;
}
