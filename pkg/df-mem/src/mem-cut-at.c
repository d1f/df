#include <int/mem.h>
#include <string.h> // memmove


// returns new size
size_t df_mem_cut_at(df_mem_t *m, size_t offset, size_t size)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    size_t old_size = m->size;

    if (offset < old_size)
    {
	DF_CHECK_NULL("data pointer", m->data);

	if ((offset + size) > old_size)
	    size = old_size - offset; // clip cutted size

	// cut hole
	memmove((char*)m->data + offset, (char*)m->data + offset + size, old_size - size - offset);

	// shrink
	df_mem_realloc_nocheck(m, old_size - size);
    }
    else // offset >= old_size
    {
	// nothing to do
    }

    return m->size;
}
