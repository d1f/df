#include <int/mem.h>
#include <string.h> // memmove


// returns new size
size_t df_mem_insert_at(df_mem_t *m, size_t offset, const void *src, size_t size)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    size_t old_size = m->size;

    size_t new_size = (offset <= old_size) ? old_size + size : offset + size;

    df_mem_realloc_nocheck(m, new_size);

    if (offset <= old_size)
    {
	// make hole
	memmove((char*)m->data + offset + size, (char*)m->data + offset, old_size - offset);
    }
    else // offset > old_size
    {
	// fill hole with zeroes
	memset((char*)m->data + old_size, 0, offset - old_size);
    }

    // copy to hole / new area
    memmove((char*)m->data + offset, src, size);

    return m->size;
}
