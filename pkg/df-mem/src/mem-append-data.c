#include <int/mem.h>
#include <string.h> // memmove


// returns new size
size_t df_mem_append_data(df_mem_t *m, const void *src, size_t size)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    size_t old_size = m->size;

    df_mem_realloc_nocheck(m, old_size + size);

    memmove((char*)m->data + old_size, src, size);

    return m->size;
}
