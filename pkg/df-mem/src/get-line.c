#include <df/get-line.h>
#include <df/mem.h>
#include <df/bt.h>

#include <stdlib.h>
#include <errno.h>


/*
 Reads line from FILE* to dynamically malloc'ed buffer.
 Returns: 0 on success, >0 - EOF, <0 - -errno.
*/
int df_get_line(FILE* f, char* *line)
{
    DF_BT;

    df_mem_t *m = df_mem_create(0, NULL);

    int c, rc;

    while ( (c = fgetc(f)) != EOF )
    {
	if (c == 0)
	    continue;

	df_mem_append_char(m, c);

	if (c == '\n')
	    goto out;
    }

    if (ferror(f))
    {
        rc = -errno;
	goto fail;
    }

    // EOF and no data
    if (df_mem_size(m) == 0)
    {
	*line = NULL;
	return 1; // EOF
    }

    // EOF and data

out:
    df_mem_append_char(m, '\0');
    *line = df_mem_take(&m);
    return 0;

fail:
    df_mem_delete(&m);
    *line = NULL;
    return rc;
}
