#include <int/mem.h>

// returns new size
size_t df_mem_append_char(df_mem_t *m, char c)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    df_mem_realloc_nocheck(m, m->size + 1);

    ((char*)m->data)[m->size - 1] = c;
    return m->size;
}
