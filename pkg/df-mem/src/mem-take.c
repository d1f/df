#include <int/mem.h>

// return xmalloc'ed data, *mp deleted
void *df_mem_take(df_mem_t **mp)
{
    DF_BT;

    df_mem_t *m = *mp;
    CHECK_NULL(m);
    CHECK_SIGNATURE(m);

    void *data = m->data;
    m->data = NULL;
    m->size = 0;

    memset(m, ~0, sizeof(*m));
    df_freep(mp);

    return data;
}
