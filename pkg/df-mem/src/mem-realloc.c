#include <int/mem.h>

void df_mem_realloc(df_mem_t *m, size_t size)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    df_mem_realloc_nocheck(m, size);
}
