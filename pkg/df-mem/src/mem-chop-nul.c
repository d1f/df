#include <int/mem.h>
#include <string.h> // strlen

void df_mem_chop_nul(df_mem_t *m)
{
    DF_BT;

    CHECK_SIGNATURE(m);

    if (m->size > 0)
    {
	if (((char*)m->data)[m->size - 1] == '\0') // last char is NUL
	    m->size--;
    }
}
