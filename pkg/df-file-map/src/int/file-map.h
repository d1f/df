#ifndef DF_FILE_MAP_INT_H
#define DF_FILE_MAP_INT_H

#include <df/file-map.h>


struct df_file_map
{
    df_signature_t signature;

    char *filename;
    int fd;
    size_t size;
    void *ptr;
};


void df_file_map_check(df_flf_t *flf, df_file_map_t *fmap);


#endif //DF_FILE_MAP_INT_H
