#ifndef DF_FILE_MAP_H
#define DF_FILE_MAP_H

#include <df/defs.h>
#include <df/types.h>
#include <sys/mman.h> // madvise flags


        struct df_file_map;
typedef struct df_file_map df_file_map_t;


// returns NULL if open() failed
df_file_map_t *df_file_map_create(const char *filename, int madv_flag) ATTR(nonnull);
void           df_file_map_delete(df_file_map_t **fmap_p);

const char    *df_file_map_name  (df_file_map_t  *fmap) ATTR(nonnull, returns_nonnull);
size_t         df_file_map_size  (df_file_map_t  *fmap) ATTR(nonnull);
void          *df_file_map_ptr   (df_file_map_t  *fmap) ATTR(nonnull);


#endif //DF_FILE_MAP_H
