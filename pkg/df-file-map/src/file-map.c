#include <int/file-map.h>
#include <df/x.h>
#include <df/bt.h>
#include <df/error.h>
#include <df/check.h>
#include <df-file-map-signatures.h>
#include <sys/stat.h> // fstat
#include <fcntl.h>    // open
#include <unistd.h>   // close


df_file_map_t *df_file_map_create(const char *filename, int madv_flag)
{
    DF_BT;

    df_file_map_t *fmap = df_xzmalloc(sizeof(df_file_map_t));
    fmap->signature     = df_file_map_SIGNATURE;
    fmap->fd            = -1;
    fmap->filename      = df_xstrdup(filename);

    fmap->fd = open(filename, O_RDONLY | O_LARGEFILE | O_NOATIME);
    if (fmap->fd < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't open %s", __func__, filename);
	goto err;
    }

    struct stat stat;
    int rc = fstat(fmap->fd, &stat);
    if (rc < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't stat %s", __func__, filename);
	goto err;
    }
    fmap->size = stat.st_size;

    fmap->ptr = mmap(NULL, stat.st_size, PROT_READ,
		     MAP_PRIVATE | MAP_NORESERVE,
		     fmap->fd, 0);
    if (fmap->ptr == MAP_FAILED)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't mmap %s", __func__, filename);
	goto err;
    }

    if (madvise(fmap->ptr, stat.st_size, MADV_DONTDUMP) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't madvise DONTDUMP on %s", __func__, filename);
	goto err;
    }

    if (madvise(fmap->ptr, stat.st_size, madv_flag) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't madvise on %s", __func__, filename);
	goto err;
    }

    return fmap;

err:
    df_file_map_delete(&fmap);
    return NULL;
}

void df_file_map_delete(df_file_map_t **fmap_p)
{
    DF_BT;

    if (fmap_p == NULL)
	return;

    df_file_map_t *fmap = *fmap_p;
    if (fmap == NULL)
	return;

    DF_CHECK_SIGNATURE(df_file_map, fmap);

    if (fmap->ptr != NULL && fmap->ptr != MAP_FAILED)
    {
	munmap(fmap->ptr, fmap->size);
	fmap->ptr = NULL;
    }

    if (fmap->fd >= 0)
    {
	close(fmap->fd);
	fmap->fd = -1;
    }

    df_freep(&fmap->filename);
    fmap->signature = 0;

    df_freep(fmap_p);
}


const char *df_file_map_name(df_file_map_t *fmap)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_file_map, fmap);

    return fmap->filename;
}

size_t df_file_map_size(df_file_map_t *fmap)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_file_map, fmap);

    return fmap->size;
}

void *df_file_map_ptr(df_file_map_t *fmap)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_file_map, fmap);

    return fmap->ptr;
}
