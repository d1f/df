#include <df/sm.h>
#include <df/bt.h>
#include <df/log.h>


static void df_sm_process_(df_sm_ctx_t *ctx, df_sm_event_t event, void *proc_data,
			   size_t events, size_t states,
			   const df_sm_switch_item_t items[events][states])
{
    DF_BT;

    const df_sm_switch_item_t *item = &items[event][ctx->state];

    df_sm_state_t new_state = DF_SM_NO_STATE;
    if (item->func != NULL)
	new_state = item->func(ctx, event, item->data, proc_data);

    if (IS_DF_SM_NO_STATE(new_state))
	new_state = item->next;

    if (! IS_DF_SM_NO_STATE(new_state))
	ctx->state = new_state;
}

void df_sm_process(df_sm_ctx_t *ctx, df_sm_event_t event, void *proc_data)
{
    DF_BT;

    if (ctx->events == 0)
    {
	DFLOGW("Context events == 0");
	return;
    }

    if (ctx->states == 0)
    {
	DFLOGW("Context states == 0");
	return;
    }

    if (IS_DF_SM_NO_STATE(ctx->state))
    {
	DFLOGW("No context state");
	return;
    }

    if ((size_t)ctx->state >= ctx->states)
    {
	DFLOGW("state %zd exceed states limit %zu", ctx->state, ctx->states);
	return;
    }

    if (IS_DF_SM_NO_EVENT(event))
    {
	DFLOGW("No event supplied");
	return;
    }

    if ((size_t)event >= ctx->events)
    {
	DFLOGW("event %zd exceed events limit %zu", event, ctx->events);
	return;
    }

    df_sm_process_(ctx, event, proc_data,
		   ctx->events, ctx->states, ctx->items);
}
