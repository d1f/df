#ifndef  DF_SM_H
# define DF_SM_H

# include <stddef.h>    // size_t NULL
# include <sys/types.h> // ssize_t
# include <df/defs.h>   // ATTR


typedef ssize_t df_sm_state_t;
typedef ssize_t df_sm_event_t;

#define DF_SM_NO_STATE (-1)
#define DF_SM_NO_EVENT (-1)

#define IS_DF_SM_NO_STATE(state) ((state) < 0)
#define IS_DF_SM_NO_EVENT(event) ((event) < 0)


// state machinte context with state
        struct df_sm_ctx_s;
typedef struct df_sm_ctx_s df_sm_ctx_t;


// state/event process function
// returns next state
typedef df_sm_state_t (*df_sm_func_f)(df_sm_ctx_t *ctx, df_sm_event_t event,
				      void *item_data, void *proc_data) ATTR(nonnull);


typedef struct df_sm_switch_item_s
{
    df_sm_func_f func;
    void *data; // user-defined per-switch-item data
    df_sm_state_t next; // default next state
} df_sm_switch_item_t;


struct df_sm_ctx_s
{
    df_sm_state_t state;
    void *data;  // user-defined per-context data
    const void *params; // user-defined per-context parameters

    size_t events, states;
    const df_sm_switch_item_t *items; // 2d array, items[events][states]
};



void df_sm_process(df_sm_ctx_t *ctx, df_sm_event_t event, void *proc_data) ATTR(nonnull);


#endif //DF_SM_H
