#include <int/shcfg.h>

void df_shcfg_add(df_shcfg_t *cfg, const char *key, const char *val)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    df_da_append_item(cfg->da, df_kv_str_create(key, val));
}
