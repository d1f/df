#include <int/shcfg.h>
#include <df/error.h>
#include <df/get-line.h>
#include <df/whitespace.h>
#include <df/x.h>


// returns malloced line or NULL on EOF or error
static char *df_shcfg_get_line(df_shcfg_t *cfg)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    if (cfg->f == NULL)
	return NULL;

    char *line = NULL;

    // Returns: 0 on success, >0 - EOF, <0 - -errno.
    int rc = df_get_line(cfg->f, &line);

    if (rc == 0)
	++cfg->line_num;
    else if (rc < 0)
    {
	dferror(EXIT_SUCCESS, -rc, "Can't read from %s", cfg->fname);
	line = NULL;
    }
    else // rc > 0 - EOF
	line = NULL;

    return line;
}

// returns NULL if no key=value pair found, empty line
static df_kv_t *df_shcfg_parse_line(char *line, char separator)
{
    DF_BT;

    df_strip_whitespace(line);

    char *h = strchr(line, '#'); // find shell comment
    if (h != NULL)
	*h = '\0'; // strip shell comment

    df_strip_whitespace(line);

    if (strlen(line) == 0)
	return NULL;

    char *ep = strchr(line, separator);
    if (ep != NULL)
    {
	*ep++ = '\0';

	df_strip_whitespace(line);
	df_strip_whitespace(ep);

	size_t eplen = strlen(ep);

	if ( (strlen(line) + eplen) == 0 )
	    return NULL;

	// strip double quotes
	if (eplen > 1 && ep[0] == '"' && ep[eplen-1] == '"')
	{
	    ep[eplen-1] = '\0'; eplen--;
	    memmove(ep, ep+1, eplen+1);
	}
    }

    return df_kv_str_create(line, ep);
}

// returns error flag
bool df_shcfg_parse(df_shcfg_t *cfg, const char *fname, char separator)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    bool rc = df_shcfg_fopen(cfg, fname, DF_SHCFG_OPEN_READ);
    if (rc) return rc;

    for (char *line; ((line = df_shcfg_get_line(cfg)) != NULL); )
    {
	df_kv_t *kv = df_shcfg_parse_line(line, separator);
	if (kv != NULL)
	    df_da_append_item(cfg->da, kv);

	free(line);
    }

    return false;
}
