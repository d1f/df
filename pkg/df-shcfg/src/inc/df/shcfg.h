#ifndef  DF_SHCFG_H
# define DF_SHCFG_H

# include <df/da.h>
# include <df/kv.h>
# include <df/defs.h> // ATTR

        struct df_shcfg;
typedef struct df_shcfg df_shcfg_t;


// returns NULL on error.
// fname maybe "-" for stdin/stdout or NULL.
df_shcfg_t *df_shcfg_create     (const char *fname, char separator);

void        df_shcfg_delete     (df_shcfg_t **cfgp);

// returns error flag. fname maybe "-" for stdin/stdout.
bool        df_shcfg_parse      (df_shcfg_t *cfg, const char *fname, char separator)             ATTR(nonnull);
bool        df_shcfg_write      (df_shcfg_t *cfg, const char *fname, char separator, char quote) ATTR(nonnull);

size_t      df_shcfg_length     (df_shcfg_t *cfg) ATTR(nonnull);

// returns found df_kv_t* or NULL
df_kv_t    *df_shcfg_lsearch    (df_shcfg_t *cfg, const char *key)   ATTR(nonnull);
df_kv_t    *df_shcfg_bsearch    (df_shcfg_t *cfg, const char *key)   ATTR(nonnull);

// returns found val string or NULL
const char *df_shcfg_lsearch_val(df_shcfg_t *cfg, const char *key)   ATTR(nonnull);
const char *df_shcfg_bsearch_val(df_shcfg_t *cfg, const char *key)   ATTR(nonnull);

void        df_shcfg_del        (df_shcfg_t *cfg, const char *key)                  ATTR(nonnull);
void        df_shcfg_add        (df_shcfg_t *cfg, const char *key, const char *val) ATTR(nonnull(1,2));
void        df_shcfg_set        (df_shcfg_t *cfg, const char *key, const char *val) ATTR(nonnull(1,2));


// returns array of df_kv_t
df_da_t    *df_shcfg_da         (df_shcfg_t *cfg) ATTR(nonnull);

# define DF_SHCFG_FOREACH(cfg, kvp, idx) DF_DA_FOREACH(df_shcfg_da(cfg), df_kv_t, kvp, idx)


// usage: df_shcfg_t DF_SHCFG_CLEANUP *cfg = df_shcfg_create(fname, separator);
# define DF_SHCFG_CLEANUP ATTR( __cleanup__(df_shcfg_delete) )


#endif //DF_SHCFG_H
