#include <int/shcfg.h>

df_kv_t *df_shcfg_lsearch(df_shcfg_t *cfg, const char *key)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    return df_da_lsearch_item(cfg->da, &((df_kv_t){ .key.ccPtr = key }) );
}

df_kv_t *df_shcfg_bsearch(df_shcfg_t *cfg, const char *key)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    return df_da_bsearch_item(cfg->da, &((df_kv_t){ .key.ccPtr = key }) );
}


const char *df_shcfg_lsearch_val(df_shcfg_t *cfg, const char *key)
{
    DF_BT;

    df_kv_t *found = df_shcfg_lsearch(cfg, key);
    return found == NULL ? NULL : found->val.ccPtr;
}

const char *df_shcfg_bsearch_val(df_shcfg_t *cfg, const char *key)
{
    DF_BT;

    df_kv_t *found = df_shcfg_bsearch(cfg, key);
    return found == NULL ? NULL : found->val.ccPtr;
}
