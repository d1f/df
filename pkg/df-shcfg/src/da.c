#include <int/shcfg.h>
#include <df/error.h>

df_da_t *df_shcfg_da(df_shcfg_t *cfg)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    return cfg->da;
}
