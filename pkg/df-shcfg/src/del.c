#include <int/shcfg.h>

void df_shcfg_del(df_shcfg_t *cfg, const char *key)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    ssize_t found = df_da_bsearch(cfg->da, &((df_kv_t){ .key.ccPtr = key }) );
    if (found >= 0)
	df_da_cut_item(cfg->da, found);
}
