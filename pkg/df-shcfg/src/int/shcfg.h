#ifndef  DF_SHCFG_INT_H
# define DF_SHCFG_INT_H

# include <df/shcfg.h>
# include <df/bt.h>
# include <df/check.h>
# include <df-shcfg-signatures.h>
# include <stdio.h>  // FILE
# include <stddef.h> // size_t


struct df_shcfg
{
    df_signature_t signature;

    char *fname; // strdup'ed
    FILE *f;
    size_t line_num;
    df_da_t *da;
};


#define DF_SHCFG_CHECK_NULL(cfg) DF_CHECK_NULL("df_shcfg_t pointer", cfg)


void df_shcfg_fclose(df_shcfg_t *cfg) ATTR(nonnull);


typedef enum DF_SHCFG_OPEN { DF_SHCFG_OPEN_READ, DF_SHCFG_OPEN_WRITE } df_shcfg_open_mode_t;

// returns error flag. fname maybe "-" for stdin/stdout.
bool df_shcfg_fopen(df_shcfg_t *cfg, const char *fname, df_shcfg_open_mode_t mode) ATTR(nonnull);


#endif //DF_SHCFG_INT_H
