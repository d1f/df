#include <int/shcfg.h>

void df_shcfg_fclose(df_shcfg_t *cfg)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    if (cfg->f != NULL)
    {
	fclose(cfg->f);
	cfg->f = NULL;
    }
}
