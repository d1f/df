#include <int/shcfg.h>
#include <df/error.h>

bool df_shcfg_write(df_shcfg_t *cfg, const char *fname, char separator, char quote)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    bool rc = df_shcfg_fopen(cfg, fname, DF_SHCFG_OPEN_WRITE);
    if (rc) return rc;

    DF_SHCFG_FOREACH(cfg, kv, i)
    {
	fputs(kv->key.ccPtr, cfg->f);
	fputc(separator, cfg->f);

	if (kv->val.vPtr != NULL)
	{
	    if (quote != 0)
		fputc(quote, cfg->f);

	    fputs(kv->val.ccPtr, cfg->f);

	    if (quote != 0)
		fputc(quote, cfg->f);
	}

	fputc('\n', cfg->f);
    }

    fflush(cfg->f);

    if (ferror(cfg->f))
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't write file \"%s\"", __func__, fname);
	rc = true;
    }

    df_shcfg_fclose(cfg);
    return rc;
}
