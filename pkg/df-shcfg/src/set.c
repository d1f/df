#include <int/shcfg.h>
#include <df/x.h> // df_freep df_xstrdup

void df_shcfg_set(df_shcfg_t *cfg, const char *key, const char *val)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    df_kv_t *found = df_da_bsearch_item(cfg->da, &((df_kv_t){ .key.ccPtr = key }) );
    if (found != NULL)
    {
	df_freep(&found->val.cPtr);
	if (val != NULL)
	    found->val.cPtr = df_xstrdup(val);
    }
}
