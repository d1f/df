#include <int/shcfg.h>
#include <df/error.h>
#include <df/x.h> // df_freep

// returns error flag. fname maybe "-" for stdin/stdout.
bool df_shcfg_fopen(df_shcfg_t *cfg, const char *fname, df_shcfg_open_mode_t mode)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    df_shcfg_fclose(cfg);

    df_freep(&cfg->fname);

    bool std = false;
    if (strcmp(fname, "-") == 0)
    {
	std = true;
	fname = mode == DF_SHCFG_OPEN_WRITE ? "stdout" : "stdin";
    }

    cfg->fname = df_xstrdup(fname);

    if (std)
    {
	cfg->f = mode == DF_SHCFG_OPEN_WRITE ? stdout : stdin;
    }
    else
    {
	cfg->f = fopen(fname, mode == DF_SHCFG_OPEN_WRITE ? "w" : "r");
	if (cfg->f == NULL)
	{
	    dferror(EXIT_SUCCESS, errno, "Can't open file \"%s\" for %s", fname,
		    mode == DF_SHCFG_OPEN_WRITE ? "write" : "read");
	    return true;
	}
    }

    return false;
}
