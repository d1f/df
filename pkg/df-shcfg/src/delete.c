#include <int/shcfg.h>
#include <df/x.h>

void df_shcfg_delete(df_shcfg_t **cfgp)
{
    DF_BT;

    if (cfgp == NULL)
	return;

    df_shcfg_t *cfg = *cfgp;
    if (cfg == NULL)
	return;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    df_shcfg_fclose(cfg);

    df_freep(&cfg->fname);

    cfg->line_num = 0;

    df_da_delete(&cfg->da);

    df_freep(cfgp);
}
