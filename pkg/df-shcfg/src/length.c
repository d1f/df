#include <int/shcfg.h>

size_t df_shcfg_length(df_shcfg_t *cfg)
{
    DF_BT;

    DF_CHECK_SIGNATURE(df_shcfg, cfg);

    return df_da_length(cfg->da);
}
