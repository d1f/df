#include <int/shcfg.h>
#include <df/x.h>

static void kv_dtor(void *item)
{
    DF_BT;

    df_kv_str_dtor(item);
}

// returns NULL on error.
// fname maybe "-" for stdin/stdout or NULL.
df_shcfg_t *df_shcfg_create(const char *fname, char separator)
{
    DF_BT;

    df_shcfg_t *cfg = df_xzmalloc(sizeof(df_shcfg_t));
    cfg->signature  = df_shcfg_SIGNATURE;
    cfg->da = df_da_create(sizeof(df_kv_t), kv_dtor, df_kv_key_strcmp);

    if (fname != NULL)
	if (df_shcfg_parse(cfg, fname, separator))
	    df_shcfg_delete(&cfg);

    return cfg;
}
