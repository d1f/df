#define _GNU_SOURCE

#include <df/date-format.h> // df_date_format_list
#include <df/error.h>
#include <df/log.h>
#include <df/c-escape.h>

#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h>  // printf
#include <string.h> // basename


int main(int ac, char *av[])
{
    dflog_open(NULL, DFLOG_STDERR | DFLOG_BASENAME);

    if (ac != 6)
    {
	dflog(LOG_ERR, "Wrong number of parameters supplied!");
	dflog(LOG_INFO, "\nUsage: %s <left delimiter string> <right delimiter string> <column separator> <line end> en|ru\n\n",
	       df_basename(av[0]));
	return EXIT_FAILURE;
    }

    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    const char * left_delim = av[1];
    const char *right_delim = av[2];
    const char *    col_sep = av[3];
    const char *   line_end = av[4];
    const char *   lang_str = av[5];

    df_locale_lang_t lang;
    if (df_locale_lang_parse(&lang, lang_str))
	return EXIT_FAILURE;

    char  col_sep_unescaped[strlen(col_sep) +1];
    char line_end_unescaped[strlen(line_end)+1];

    df_c_unescape( col_sep_unescaped,  col_sep);
    df_c_unescape(line_end_unescaped, line_end);

    // output format description list to stdout
    df_date_format_list(left_delim, right_delim, col_sep_unescaped, line_end_unescaped, lang);

    return EXIT_SUCCESS;
}
