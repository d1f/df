#define _GNU_SOURCE

#include <stdio.h>  // fputs
#include <stdlib.h> // EXIT_*
#include <string.h> // basename
#include <df/filename-template.h>
#include <df/hex.h>
#include <df/error.h>


int main(int ac, char *av[])
{
    if (ac != 5)
    {
	printf("\nUsage: %s left-delim right-delim hex-template ru|en\n\n",
	       basename(av[0]));
	return EXIT_FAILURE;
    }

    const char  left_delim   = av[1][0];
    const char right_delim   = av[2][0];
    const char *template_hex = av[3];
    const char *lang_str     = av[4];

    df_locale_lang_t lang;
    if (df_locale_lang_parse(&lang, lang_str))
	return EXIT_FAILURE;

    char template[NAME_MAX];
    if (df_hex2str(template, template_hex))
	return EXIT_FAILURE;

    char out[NAME_MAX]; out[0] = '\0';

    df_filename_template(out, NULL, NULL, NULL,
			 left_delim, right_delim, template, lang, NULL);

    char hex[NAME_MAX * 2 + 1];
    df_str2hex(hex, out);

    fputs(hex, stdout);
    return EXIT_SUCCESS;
}
