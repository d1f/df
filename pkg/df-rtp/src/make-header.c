#include <df/rtp.h>
#include <df/log.h>
#include <arpa/inet.h> // htonl


void df_rtp_make_header(uint32_t rtphdr[RTP_MIN_HEADER_LEN],
			uint16_t seqnum, uint8_t pt, uint32_t ts, uint32_t ssrc)
{
    uint32_t h = RTP_V_VAL << RTP_V_SHIFT;
    h         |= (pt & RTP_PT_MASK) << RTP_PT_SHIFT;
    h         |= seqnum & RTP_SEQ_MASK;
    rtphdr[0]  = htonl(h);
    rtphdr[1]  = htonl(ts);
    rtphdr[2]  = htonl(ssrc);
}

