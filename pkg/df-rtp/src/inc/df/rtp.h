#ifndef DF_RTP_H
#define DF_RTP_H

// rfc 3550

#define RTP_V_SHIFT       30
#define RTP_V_MASK        0x3
#define RTP_V_VAL         0x2

/* Padding bit
 the packet contains one or more additional padding octets
 at the end which are not part of the payload.
 The last octet of the padding contains a count of how
 many padding octets should be ignored, including itself.
 */
#define RTP_P_SHIFT       29 
#define RTP_P_MASK        1

/* eXtension bit
 fixed header MUST be followed by exactly one header extension,
 with a format defined in Section 5.3.1.
 */
#define RTP_X_SHIFT       28
#define RTP_X_MASK        1

/* CSRC count
 the number of CSRC identifiers that follow the fixed header.
 */
#define RTP_CC_SHIFT      24
#define RTP_CC_MASK       0xf

/* marker bit
 The interpretation of the marker is defined by a profile.
 It is intended to allow significant events such as frame boundaries
 to be marked in the packet stream.
 */
#define RTP_M_SHIFT       23
#define RTP_M_MASK        1

/* payload type
 */
#define RTP_PT_SHIFT      16
#define RTP_PT_MASK       0x7f

/* sequence number
 increments by one for each RTP data packet sent.
 The initial value of the sequence number SHOULD be random (unpredictable).
 */
#define RTP_SEQ_SHIFT     0
#define RTP_SEQ_MASK      0xffff


#define RTP_MIN_HEADER_LEN (3)
#define RTP_MIN_HEADER_SIZE (RTP_MIN_HEADER_LEN * sizeof(uint32_t))


#include <df/types.h>

typedef struct
{
    uint32_t ssrc;
    uint32_t csrc[RTP_CC_MASK + 1]; // 16
    uint32_t timestamp;
    ushort   seq_num;
    uchar    payload_type;
    uchar    csrc_count;
    int      padding;
    int    extension;
    int       marker;
    int        error; // rest is not valid
} df_rtp_ret_t;

// returns pointer past RTP header or NULL if error
const uint32_t *df_rtp_parse_header(df_rtp_ret_t *rtpret, const uint32_t *rtpsrc);

void df_rtp_make_header(uint32_t rtphdr[RTP_MIN_HEADER_LEN],
			uint16_t seqnum, uint8_t pt, uint32_t ts, uint32_t ssrc);


#endif //DF_RTP_H

