#include <df/rtp.h>
#include <df/log.h>
#include <arpa/inet.h> // ntohl


// returns pointer past RTP header or NULL if error
const uint32_t *df_rtp_parse_header(df_rtp_ret_t *rtpret, const uint32_t *rtpsrc)
{
    const uint32_t *ret = NULL;

    if (rtpsrc == NULL)
    {
	dflog(LOG_ERR, "%s(): rtpsrc is NULL", __func__);
	return NULL;
    }
    if (rtpret == NULL)
    {
	dflog(LOG_ERR, "%s(): rtpret is NULL", __func__);
	return NULL;
    }

    memset(rtpret, 0, sizeof(*rtpret));
    rtpret->error = 1;

    uint32_t h = ntohl(rtpsrc[0]);

    uint32_t version  = (h >> RTP_V_SHIFT) & RTP_V_MASK;
    if (version != RTP_V_VAL)
    {
	dflog(LOG_ERR, "%s(): Unknown version 0%02x, only 2 supported", __func__, version);
	return NULL;
    }

    rtpret->padding  = (h >> RTP_P_SHIFT) & RTP_P_MASK;
    if (rtpret->padding != 0) //FIXME
    {
	dflog(LOG_ERR, "%s(): Padding bit yet not supported, sorry", __func__);
	return NULL;
    }

    rtpret->extension = (h >> RTP_X_SHIFT) & RTP_X_MASK;
    if (rtpret->extension != 0) //FIXME
    {
	dflog(LOG_ERR, "%s(): Extension bit yet not supported, sorry", __func__);
	return NULL;
    }

    ret = &rtpsrc[RTP_MIN_HEADER_LEN]; // past fixed header

    rtpret->csrc_count   = (h >> RTP_CC_SHIFT) & RTP_CC_MASK;
    for (size_t i = 0; i < rtpret->csrc_count; ++i)
	rtpret->csrc[i] = ntohl(rtpsrc[2+i]);
    ret += rtpret->csrc_count; // past CSRC

    rtpret->marker = (h >> RTP_M_SHIFT) & RTP_M_MASK;

    rtpret->payload_type = (h >> RTP_PT_SHIFT) & RTP_PT_MASK; // 8 - PCMA

    rtpret->seq_num = (h >> RTP_SEQ_SHIFT) & RTP_SEQ_MASK;

    rtpret->timestamp = ntohl(rtpsrc[1]);
    rtpret->ssrc      = ntohl(rtpsrc[2]);

    rtpret->error = 0;
    return ret;
}

