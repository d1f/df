#include <df/pda.h>
#include <df/defs.h> // DF_MEMZERO
#include <df/bt.h>
#include <df/check.h>
#include <df/x.h>


#define DF_PDA_CHECK_NULL(pda) DF_CHECK_NULL("df_pda_t*", pda)


void df_pda_init(df_pda_t *pda)
{
    DF_BT;

    DF_MEMZERO(*pda);
}

void df_pda_free(df_pda_t *pda)
{
    DF_BT;

    df_freep(&pda->pa);
    pda->offset = 0;
    pda->length = 0;
    pda->childs = 0;
}

df_pda_t *df_pda_create(void)
{
    DF_BT;

    return df_xzmalloc(sizeof(df_pda_t));
}

void df_pda_delete(df_pda_t **pdap)
{
    DF_BT;

    df_pda_t *pda = *pdap;

    df_pda_free(pda);

    df_freep(pdap);
}

df_vptr_t df_pda_get(df_pda_t const *pda, size_t idx)
{
    DF_BT;

    if (idx < pda->offset || idx >= (pda->offset + pda->length))
	return NULL;

    return pda->pa[idx - pda->offset];
}

static size_t ATTR(nonnull(1)) df_pda_realloc(df_pda_t *pda, size_t idx)
{
    DF_BT;

    size_t size_inc = 0;

    if (pda->length == 0)
    {
	pda->pa = df_xmalloc(sizeof(df_vptr_t));
	pda->length = 1;
	pda->offset = idx;
	size_inc = sizeof(df_vptr_t);
    }
    else if (idx >= (pda->offset + pda->length))
    {
	size_t  new_length = idx + 1 - pda->offset;
	size_t hole_length = new_length - pda->length;
	df_vptr_t *new_pa    = df_xrealloc(pda->pa, sizeof(df_vptr_t) * new_length);
	memset(&new_pa[pda->length], 0, sizeof(df_vptr_t) * hole_length); // zero hole
	pda->pa     = new_pa;
	pda->length = new_length;

	size_inc = sizeof(df_vptr_t) * hole_length;
    }
    else if (idx < pda->offset)
    {
	size_t  new_offset = idx;
	size_t  new_length = (pda->length + pda->offset) - new_offset;
	size_t hole_length =   new_length - pda->length;
	df_vptr_t *new_pa  = df_xrealloc(pda->pa, sizeof(df_vptr_t) * new_length);
	memmove(&new_pa[hole_length], &new_pa[0],   pda->length); // make hole
	memset (&new_pa[0], 0, sizeof(df_vptr_t) * hole_length); // zero hole
	pda->pa     = new_pa;
	pda->length = new_length;
	pda->offset = new_offset;

	size_inc = sizeof(df_vptr_t) * hole_length;
    }

    return size_inc;
}

// returns size increment
size_t df_pda_set(df_pda_t *pda, size_t idx, df_vptr_t ptrval)
{
    DF_BT;

    size_t size_inc = df_pda_realloc(pda, idx);
    df_vptr_t *pp = & pda->pa[idx - pda->offset];

    if (*pp == NULL)
	pda->childs += (ptrval != NULL);
    else // *pp != NULL
    {
	if (ptrval == NULL)
	    df_dec(DF_FLF, "df_pda_t childs count", &pda->childs);
    }

    *pp = ptrval;
    return size_inc;
}

size_t df_pda_size(df_pda_t const *pda)
{
    DF_BT;

    return sizeof(*pda) + pda->length * sizeof(df_vptr_t);
}

size_t df_pda_childs(df_pda_t const *pda)
{
    DF_BT;

    return pda->childs;
}

void df_pda_move(df_pda_t *dst, df_pda_t *src)
{
    DF_BT;

    *dst = *src;
    DF_MEMZERO(*src);
}

