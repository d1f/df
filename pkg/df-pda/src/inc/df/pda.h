#ifndef DF_PDA_H
#define DF_PDA_H

#include <df/types.h> // df_vptr_t


typedef struct
{
    size_t offset, length, childs;
    df_vptr_t *pa; // pointers array
} df_pda_t;

void      df_pda_init(df_pda_t *pda)                     ATTR(nonnull);
void      df_pda_free(df_pda_t *pda)                     ATTR(nonnull);

df_pda_t *df_pda_create(void)                            ATTR(returns_nonnull);
void      df_pda_delete(df_pda_t **pdap)                 ATTR(nonnull);

df_vptr_t df_pda_get   (df_pda_t const *pda, size_t idx) ATTR(nonnull);

// returns size increment
size_t    df_pda_set   (df_pda_t       *pda, size_t idx,
			df_vptr_t ptrval)                ATTR(nonnull(1));

size_t    df_pda_size  (df_pda_t const *pda)             ATTR(nonnull);
size_t    df_pda_childs(df_pda_t const *pda)             ATTR(nonnull);

void      df_pda_move(df_pda_t *dst, df_pda_t *src)      ATTR(nonnull);


// warning: item_p has (df_vptr_t *) type.
// usage: DF_PDA_FOREACH(pda, item_p) { statements; }
#define   DF_PDA_FOREACH(pda, item_p) \
    for (df_vptr_t *(item_p) = (pda)->pa; (item_p) < (pda)->pa + (pda)->length; ++(item_p))


#endif //DF_PDA_H

