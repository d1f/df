#include <df/opt.h>
#include <df/log.h>
#include <df/bt.h>


static df_opt_cb_res_t cb(const df_opt_t *opt, void *cb_data)
{
    DF_BT;

    (void)cb_data;

    if (opt->ccp_ptr != NULL && !opt->has_value)
    {
	dflog(LOG_INFO, "-%s: ccp_ptr != NULL requires has_value", opt->key);
	return DF_OPT_CB_STOP;
    }

    if (opt->scan_fmt != NULL && !opt->has_value)
    {
	dflog(LOG_INFO, "-%s: scan_fmt != NULL requires has_value", opt->key);
	return DF_OPT_CB_STOP;
    }

    if (opt->scan_ptr != NULL && !opt->has_value)
    {
	dflog(LOG_INFO, "-%s: scan_ptr != NULL requires has_value", opt->key);
	return DF_OPT_CB_STOP;
    }

    if (opt->scan_fmt == NULL && opt->scan_ptr != NULL)
    {
	dflog(LOG_INFO, "-%s: scan_fmt == NULL and scan_ptr != NULL", opt->key);
	return DF_OPT_CB_STOP;
    }

    if (opt->scan_fmt != NULL && opt->scan_ptr == NULL)
    {
	dflog(LOG_INFO, "-%s: scan_fmt == NULL and scan_ptr != NULL", opt->key);
	return DF_OPT_CB_STOP;
    }

    return DF_OPT_CB_CONT;
}


// returns error flag
bool df_opt_check(const df_opt_t opts[])
{
    DF_BT;

    return df_opt_foreach(opts, cb, NULL) != NULL;
}

// exit on failure with static backtrace
void df_opt_check_exit(const df_opt_t opts[])
{
    DF_BT;

    if (df_opt_check(opts))
	df_bt_exit(EXIT_FAILURE);
}
