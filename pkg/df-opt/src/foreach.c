#include <df/opt.h>
#include <df/bt.h>


// returns opt on which stopped or NULL
const df_opt_t *df_opt_foreach(const df_opt_t opts[], df_opt_cb_f cb, void *cb_data)
{
    DF_BT;

    df_opt_cb_res_t res = DF_OPT_CB_CONT;

    for (const df_opt_t *opt = &opts[0]; opt->key != NULL; ++opt)
	if ((res = cb(opt, cb_data)) == DF_OPT_CB_STOP)
	    return opt;

    return NULL;
}
