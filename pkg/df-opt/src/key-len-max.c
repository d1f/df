#include <df/opt.h>
#include <df/bt.h>
#include <string.h>


static df_opt_cb_res_t cb(const df_opt_t *opt, void *cb_data)
{
    DF_BT;

    size_t *keylenmax = cb_data;

    size_t keylen = strlen(opt->key);
    if (keylen > *keylenmax)
	*keylenmax = keylen;

    return DF_OPT_CB_CONT;
}

size_t df_opt_key_len_max(const df_opt_t opts[])
{
    DF_BT;

    size_t keylenmax = 0;
    df_opt_foreach(opts, cb, &keylenmax);

    return keylenmax;
}
