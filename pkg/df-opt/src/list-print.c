#include <df/opt.h>
#include <df/bt.h>
#include <string.h> // strlen


typedef struct list_print_prm_s
{
    size_t keylenmax;
    FILE *out;
} list_print_prm_t;


static df_opt_cb_res_t cb(const df_opt_t *opt, void *cb_data)
{
    DF_BT;

    const list_print_prm_t *prm = cb_data;

    fprintf(prm->out, "\t-%s", opt->key);

    size_t padlen = prm->keylenmax - strlen(opt->key);
    while (padlen--)
	fputc(' ', prm->out);

    if (opt->desc != NULL)
	fprintf(prm->out, " %s", opt->desc);

    fputc('\n', prm->out);

    return DF_OPT_CB_CONT;
}

// prints options list with description
void df_opt_list_print(const df_opt_t opts[], FILE *out)
{
    DF_BT;

    list_print_prm_t prm = { .keylenmax = df_opt_key_len_max(opts), .out = out };
    df_opt_foreach(opts, cb, &prm);
}
