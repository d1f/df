#ifndef  DF_OPT_H
# define DF_OPT_H

# include <stddef.h> // size_t
# include <stdio.h>  // FILE
# include <stdbool.h>
# include <df/defs.h> // ATTR


        struct df_opt_s;
typedef struct df_opt_s df_opt_t;

typedef int (*df_opt_f)(const df_opt_t *opt, const char *value) ATTR(nonnull);

struct df_opt_s
{
    // mandatory members:

    const char *key;      // option name string
    const char *desc;     // option description
    bool has_value;       // option has mandatory value


    // optional members:

    bool *bool_ptr;       // *bool_ptr assigned to bool_val if bool_ptr != NULL
    bool  bool_val;

    int  * int_ptr;       // * int_ptr assigned to  int_val if  int_ptr != NULL
    int    int_val;

    const char **ccp_ptr; // *ccp_ptr assigned to value if ccp_ptr != NULL

    const char *scan_fmt; // sscanf parameter if != NULL
    void       *scan_ptr; //

    df_opt_f func;        // function for complex option processing with and without value
    void    *data;        // function context data
};


typedef enum DF_OPT_CB
{
    DF_OPT_CB_CONT,
    DF_OPT_CB_STOP
} df_opt_cb_res_t;

typedef df_opt_cb_res_t (*df_opt_cb_f)(const df_opt_t *opt, void *cb_data)               ATTR(nonnull);

// returns opt on which stopped or NULL
const df_opt_t *df_opt_foreach    (const df_opt_t opts[], df_opt_cb_f cb, void *cb_data) ATTR(nonnull(1,2));


size_t          df_opt_key_len_max(const df_opt_t opts[])                                ATTR(nonnull);

// returns found option pointer or NULL
const df_opt_t *df_opt_lookup_key (const df_opt_t opts[], const char *key)               ATTR(nonnull);

// returns error flag
bool            df_opt_check      (const df_opt_t opts[])                                ATTR(nonnull);

// exit on failure with static backtrace
void            df_opt_check_exit (const df_opt_t opts[])                                ATTR(nonnull);


// returns argv index after options
size_t          df_opt_parse      (const df_opt_t opts[], size_t ac, char *const av[])   ATTR(nonnull);

// prints options list with description
void            df_opt_list_print (const df_opt_t opts[], FILE *out)                     ATTR(nonnull);

// df_log() options list with description
void            df_opt_list_log   (const df_opt_t opts[])                                ATTR(nonnull);


#endif //DF_OPT_H
