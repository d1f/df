#include <df/opt.h>
#include <df/bt.h>
#include <string.h> // strcasecmp


static df_opt_cb_res_t cb(const df_opt_t *opt, void *cb_data)
{
    DF_BT;

    return strcasecmp(opt->key, cb_data) == 0 ? DF_OPT_CB_STOP : DF_OPT_CB_CONT;
}

// returns found option pointer or NULL
const df_opt_t *df_opt_lookup_key(const df_opt_t opts[], const char *key)
{
    DF_BT;

    union { void *data; const char *key; } un = { .key = key };
    return df_opt_foreach(opts, cb, un.data);
}
