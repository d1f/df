#include <df/opt.h>
#include <df/log.h>
#include <df/bt.h>
#include <string.h> // strlen


static df_opt_cb_res_t cb(const df_opt_t *opt, void *cb_data)
{
    DF_BT;

    size_t *keylenmax = cb_data;

    dflog_("\t-%s", opt->key);

    size_t padlen = *keylenmax - strlen(opt->key);
    while (padlen--)
	dflog_(" ");

    if (opt->desc != NULL)
	dflog_(" %s", opt->desc);

    dflog_flush(LOG_INFO);

    return DF_OPT_CB_CONT;
}

// prints options list with description
void df_opt_list_log(const df_opt_t opts[])
{
    DF_BT;

    size_t keylenmax = df_opt_key_len_max(opts);
    df_opt_foreach(opts, cb, &keylenmax);
}
