#include "./local.h"


PROTO(pthread_rwlock_init, (rwlock, attr),
      pthread_rwlock_t *restrict rwlock, const pthread_rwlockattr_t *restrict attr)
    ERR();
    return err;
}

PROTO(pthread_rwlock_destroy, (rwlock), pthread_rwlock_t *rwlock)
    ERR();
    return err;
}

PROTO(pthread_rwlock_wrlock, (rwlock), pthread_rwlock_t *rwlock)
    FATAL();
    return err;
}

PROTO(pthread_rwlock_trywrlock, (rwlock), pthread_rwlock_t *rwlock)
    ERR();
    return err;
}

PROTO(pthread_rwlock_rdlock, (rwlock), pthread_rwlock_t *rwlock)
    FATAL();
    return err;
}

PROTO(pthread_rwlock_tryrdlock, (rwlock), pthread_rwlock_t *rwlock)
    ERR();
    return err;
}

PROTO(pthread_rwlock_unlock, (rwlock), pthread_rwlock_t *rwlock)
    ERR();
    return err;
}
