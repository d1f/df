#include "./local.h"


PROTO(pthread_cond_init, (cond, cond_attr),
      pthread_cond_t *cond, const pthread_condattr_t *cond_attr)
    ERR();
    return err;
}

PROTO(pthread_cond_signal, (cond), pthread_cond_t *cond)
    ERR();
    return err;
}

PROTO(pthread_cond_broadcast, (cond), pthread_cond_t *cond)
    ERR();
    return err;
}

PROTO(pthread_cond_wait, (cond, mutex),
      pthread_cond_t *cond, pthread_mutex_t *mutex)
    FATAL();
    return err;
}

PROTO(pthread_cond_timedwait, (cond, mutex, abstime),
      pthread_cond_t *cond, pthread_mutex_t *mutex, const struct timespec *abstime)
    ERR();
    return err;
}

PROTO(pthread_cond_destroy, (cond), pthread_cond_t *cond)
    ERR();
    return err;
}
