#ifndef  DF_PTHREAD_LOCAL_H
# define DF_PTHREAD_LOCAL_H

# define _GNU_SOURCE
# include <pthread.h>
# include <df/pthread.h>
# include <df/error.h>
# include <df/log.h>
# include <df/bt.h>


extern bool __df_pthread_trace_flag;

# define TRACE() \
    if (__df_pthread_trace_flag) dflog(LOG_INFO, "df-pthread-hooks: %s()", __func__)

# define PROTO(NAME, ARGS, ...) \
         int     NAME(__VA_ARGS__) { \
  DF_BT; \
  extern int __##NAME(__VA_ARGS__); \
  TRACE(); \
  int err =  __##NAME ARGS;

# define ERR() \
  do { if (err) { \
          dferror_bt(EXIT_SUCCESS, err, "%s(): __%s() failed", __func__, __func__); \
       } \
  } while (0)


# define FATAL() \
  do { if (err) { \
          dferror_bt(EXIT_FAILURE, err, "%s(): __%s() failed", __func__, __func__); \
       } \
  } while (0)


#endif //DF_PTHREAD_LOCAL_H
