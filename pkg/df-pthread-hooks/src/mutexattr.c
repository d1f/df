#include "./local.h"


PROTO(pthread_mutexattr_init, (attr), pthread_mutexattr_t *attr)
    ERR();
    return err;
}

PROTO(pthread_mutexattr_destroy, (attr), pthread_mutexattr_t *attr)
    ERR();
    return err;
}

PROTO(pthread_mutexattr_settype, (attr, kind),
      pthread_mutexattr_t *attr, int kind)
    ERR();
    return err;
}
