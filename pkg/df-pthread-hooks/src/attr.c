#include "./local.h"


int pthread_attr_init(pthread_attr_t *attr)
{
    DF_BT;
    TRACE();

    extern int __pthread_attr_init_2_1(pthread_attr_t *attr);
    int err =  __pthread_attr_init_2_1(attr);

    ERR();
    return err;
}

PROTO(pthread_attr_destroy, (attr), pthread_attr_t *attr)
    ERR();
    return err;
}

PROTO(pthread_attr_setscope, (attr, scope),
      pthread_attr_t *attr, int scope)
    ERR();
    return err;
}

PROTO(pthread_attr_getscope, (attr, scope),
      const pthread_attr_t *attr, int *scope)
    ERR();
    return err;
}

PROTO(pthread_attr_setschedparam, (attr, param),
      pthread_attr_t *attr, const struct sched_param *param)
    ERR();
    return err;
}

PROTO(pthread_attr_getschedparam, (attr, param),
      const pthread_attr_t *attr, struct sched_param *param)
    ERR();
    return err;
}

PROTO(pthread_attr_setinheritsched, (attr, inheritsched),
      pthread_attr_t *attr, int inheritsched)
    ERR();
    return err;
}

PROTO(pthread_attr_getinheritsched, (attr, inheritsched),
      const pthread_attr_t *attr, int *inheritsched)
    ERR();
    return err;
}

PROTO(pthread_attr_setschedpolicy, (attr, policy),
      pthread_attr_t *attr, int policy)
    ERR();
    return err;
}

PROTO(pthread_attr_getschedpolicy, (attr, policy),
      const pthread_attr_t *attr, int *policy)
    ERR();
    return err;
}
