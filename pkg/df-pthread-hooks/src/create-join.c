#include "./local.h"


#if 0
int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg)
{
    DF_BT;
    TRACE();

    extern int __pthread_create_2_1(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
    int err =  __pthread_create_2_1(thread, attr, start_routine, arg);

    ERR();
    return err;
}
#endif
