#include "./local.h"


int pthread_mutex_init(pthread_mutex_t *mutex,
		       const pthread_mutexattr_t *mutexattr)
{
    DF_BT;
    TRACE();

    extern int __pthread_mutex_init(pthread_mutex_t *mutex,
				    const pthread_mutexattr_t *mutexattr);
    int err =  __pthread_mutex_init(mutex, mutexattr);

    ERR();
    return err;
}

PROTO(pthread_mutex_destroy, (mutex), pthread_mutex_t *mutex)
    ERR();
    return err;
}

int pthread_mutex_lock(pthread_mutex_t *mutex)
{
    DF_BT;
    extern int __pthread_mutex_lock(pthread_mutex_t *mutex);
    int err  = __pthread_mutex_lock(mutex);

    FATAL();
    return err;
}

int pthread_mutex_unlock(pthread_mutex_t *mutex)
{
    DF_BT;
    extern int __pthread_mutex_unlock(pthread_mutex_t *mutex);
    int err  = __pthread_mutex_unlock(mutex);

    ERR();
    return err;
}

int pthread_mutex_trylock(pthread_mutex_t *mutex)
{
    DF_BT;
    extern int __pthread_mutex_trylock(pthread_mutex_t *mutex);
    int err  = __pthread_mutex_trylock(mutex);

    if (err != 0 && err != EBUSY)
	FATAL();

    return err;
}
