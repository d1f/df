#include <df/log.h>
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h>


int main(int ac, char *av[])
{
    (void)ac;

    extern const char *__progname; // glibc global variable

    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);
    //fprintf(stderr, "%s: %2d: av[0]: %p %s, __progname: %p %s\n", __progname, __LINE__, av[0], av[0], __progname, __progname);
    dflog(LOG_INFO, "%2d: av[0]: %s, __progname: %s", __LINE__, av[0], __progname);
    dflog(LOG_INFO, "%2d: av[0]: %s, __progname: %s", __LINE__, av[0], __progname);
    //fprintf(stderr, "%s: %2d: av[0]: %p %s, __progname: %p %s\n", __progname, __LINE__, av[0], av[0], __progname, __progname);

    //dflog_close();
    return EXIT_SUCCESS;
}
